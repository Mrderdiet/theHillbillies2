package hillbillies.exceptions;

public class BreakStatementException extends Exception{

	private static final long serialVersionUID = 3849486624251286873L;

	public BreakStatementException() {
	}

	public BreakStatementException(String message){
		super(message);
	}
}
