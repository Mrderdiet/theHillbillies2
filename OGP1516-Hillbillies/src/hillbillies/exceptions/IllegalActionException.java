package hillbillies.exceptions;

public class IllegalActionException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public IllegalActionException(String message) {
		super(message);
	}

	public IllegalActionException() {

	}

}
