package hillbillies.exceptions;

public class IllegalArgumentExceptions extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalArgumentExceptions(String message) {
		super(message);
	}

	public IllegalArgumentExceptions() {

	}

}
