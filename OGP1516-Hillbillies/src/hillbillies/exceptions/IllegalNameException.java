package hillbillies.exceptions;

public class IllegalNameException extends Exception {

	public IllegalNameException(String message) {
		super(message);
	}

	public IllegalNameException() {

	}

	private static final long serialVersionUID = 1L;

}
