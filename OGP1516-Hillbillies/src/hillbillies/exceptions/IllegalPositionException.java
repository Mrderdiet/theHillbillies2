package hillbillies.exceptions;

public class IllegalPositionException extends Exception {

	public IllegalPositionException(String message) {
		super(message);
	}

	public IllegalPositionException() {
	}

	private static final long serialVersionUID = 1L;

}
