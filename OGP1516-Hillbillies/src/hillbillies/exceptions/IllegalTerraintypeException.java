package hillbillies.exceptions;

public class IllegalTerraintypeException extends Exception {
	public IllegalTerraintypeException(String message) {
		
		super(message);
	}

	public IllegalTerraintypeException() {
	}

	private static final long serialVersionUID = 1L;
}
