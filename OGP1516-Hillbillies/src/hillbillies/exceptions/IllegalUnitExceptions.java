package hillbillies.exceptions;

public class IllegalUnitExceptions extends Exception {
	public IllegalUnitExceptions(String message) {
		super(message);
	}

	public IllegalUnitExceptions() {

	}

	private static final long serialVersionUID = 1L;
}
