package hillbillies.exceptions;

public class NoMoreExpressionException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NoMoreExpressionException(String message) {
		super(message);
	}

	public NoMoreExpressionException() {

	}
}
