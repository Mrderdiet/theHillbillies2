package hillbillies.helpers;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
/**
 * Class containing the attributes of a unit
 * 
 * @author dieter
 * 
 * @Invar The unit must have a valid attributes (weight, agility, strength, toughness) with values between 0 and 200
 * 			|0<unit.getWeight()<200
 * 			|0<unit.getAgility()<200
 * 			|0<unit.getStrength()<200
 * 		  	|0<unit.getTougness()<200
 * @Invar The unit must have a valid weight with value between (strength+ Agility)/2 and 200
 * 			|(unit.getStrength() + unit.getAgility())/2<unit.getWeight()<200
 * @Invar The unit must have valid hitpoints and valid stamina points (values between 0 and (2 * unit.getWeight() * unit.getToughness() / 100))
 * 			|0<unit.getCurrentHitPoints()<(2*unit.getWeight() * unit.getToughness() / 100)
 * 			|0<unit.getCurrentStaminaPoints()<(2*unit.getWeight() * unit.getToughness() / 100)
 */
public class Attributes {

	/**
	 * 
	 * @param weight
	 * @param agility
	 * @param strength
	 * @param toughness
	 */
	@Raw
	public Attributes(int weight, int agility, int strength, int toughness) {
		this.setAgility(makeNumberFitAtStart(agility));
		this.setStrength(makeNumberFitAtStart(strength));
		this.setToughness(makeNumberFitAtStart(toughness));
		this.setWeight(makeNumberFitAtStart(weight));

	}

	/**
	 * returns the minimum value of an attribute.
	 * 
	 * @return this.MIN_VALUE
	 */
	@Basic
	@Raw
	@Immutable
	private int getValueMin() {
		return this.MIN_VALUE;
	}

	/**
	 * returns the maximum value of an attribute.
	 * 
	 * @return this.MAX_VALUE
	 */
	@Basic
	@Raw
	@Immutable
	private int getValueMax() {
		return this.MAX_VALUE;
	}

	/**
	 * returns the maximum value of an attribute at the start.
	 * 
	 * @return this.START_MAX_VALUE
	 */
	@Basic
	@Raw
	@Immutable
	private int getValueStartMax() {
		return this.START_MAX_VALUE;
	}

	/**
	 * returns the minimum value of an attribute at the start.
	 * 
	 * @return this.START_MIN_VALUE
	 */
	@Basic
	@Raw
	@Immutable
	private int getValueStartMin() {
		return this.START_MIN_VALUE;
	}

	/**
	 * variables storing the minimum and maximum values for an attribute. And a unit.
	 */
	private final int MAX_VALUE = 200;
	private final int MIN_VALUE = 1;
	private final int START_MAX_VALUE = 100;
	private final int START_MIN_VALUE = 25;
	/**
	 * Return the strength of the unit
	 * 
	 * @return this.strength
	 */
	@Basic
	@Raw
	public int getStrength() {
		return this.strength;
	}

	/**
	 * sets this.strength to given strength, it adjust the value if needed
	 * 
	 * @param strength
	 *            the strength of the unit
	 * @post if the unit is carrying a object he can exceed the given MAX_VALUE.
	 * @post the new strength lies between 1 and 200. 
	 * 			|if strength <this.getValueMin() 
	 * 			|then new.getStrength() == this.getValueMin() 
	 * 			|if strength > this.getValueMax() 
	 * 			|then new.getStrength() == this.getValueMax() 	
	 *          |else new.getStrength() ==strength
	 */
	@Raw
	public void setStrength(int strength) {
		this.strength = makeNumberFit(strength);
		this.updateWeight();
	}

	/**
	 * variable storing the strength of the unit
	 */
	private int strength = 50;

	/**
	 * Return the agility of the unit
	 * 
	 * @return this.agility
	 */
	@Basic
	@Raw
	public int getAgility() {
		return this.agility;
	}

	/**
	 * sets this.agility to given agility, it adjust the value if needed
	 * 
	 * @param agility
	 *            the agility of the unit
	 * @post the new agility lies between 1 and 200. |if agility <
	 *       this.getValueMin() |then new.getAgility() == this.getValueMin() |if
	 *       agility > this.getValueMax() |then new.getAgility() ==
	 *       this.getValueMax() |else new.getAgility() == agility
	 */
	@Raw
	public void setAgility(int agility) {
		this.agility = makeNumberFit(agility);
		this.updateWeight();
	}

	/**
	 * variable storing the agility of the unit
	 */
	private int agility = 50;

	/**
	 * Return the toughness of the unit
	 * 
	 * @return this.toughness
	 */
	@Basic
	@Raw
	public int getToughness() {
		return this.toughness;
	}

	/**
	 * sets this.toughness to given toughness, it adjust the value if needed
	 * 
	 * @param toughness
	 *            the toughness of the unit
	 * @post the new toughness lies between 1 and 200. |if toughness <
	 *       this.getValueMin() |then new.getToughness() == this.getValueMin()
	 *       |if toughness > this.getValueMax() |then new.getToughness() ==
	 *       this.getValueMax() |else new.getToughness() == toughness
	 */
	@Raw
	public void setToughness(int toughness) {
		this.toughness = makeNumberFit(toughness);
	}

	/**
	 * variable storing the toughness of the unit
	 */
	private int toughness = 50;

	/**
	 * Return the toughness of the unit
	 * 
	 * @return this.weight
	 */
	@Basic
	@Raw
	public int getWeight() {
		return this.weight;
	}

	/**
	 * sets this.weight to given weight, it adjust the value if needed
	 * 
	 * @param weight
	 *            the weight of the unit
	 * @post If the given weight is below the minimum value for this attribute ,
	 *       the given weight is set on | if (weight < (this.strength +
	 *       this.agility) / 2 | then new.getWeight() == (this.strength +
	 *       this.agility) / 2 | else if (weight > this.getValueMax) | then
	 *       new.getWeight() == this.getValueMax
	 */

	public void setWeight(int weight) {
		if (makeNumberFit(weight) < (this.strength + this.agility) / 2)
			this.weight = (this.getStrength() + this.getAgility()) / 2;
		else
			this.weight = makeNumberFit(weight);
	}
	
	
	
	/**
	 * A method that updates (checks) the weight if some other attributes
	 * changes. carrying
	 */

	private void updateWeight() {
		this.setWeight(this.weight);
	}

	/**
	 * variable storing the weight of the unit
	 */
	private int weight = 50;

	/**
	 * A method that checks and adjust the value for the attributes/
	 * 
	 * @param x
	 *            the number we have to check
	 * @return returns the minimum value if x is less than the minimum value
	 *         returns the maximum value if x is less than the maximum value
	 *         returns x if the value is between the minimum and the maximum
	 *         value | if (x less than this.getValueMin()) | then return
	 *         this.getValueMin(); | if (x greater than this.getValueMax()) |
	 *         then return this.getValueMax(); | else return x
	 */
	@Raw
	private int makeNumberFit(int x) {
		if (x < this.getValueMin())
			return this.getValueMin();
		else if (x > this.getValueMax())
			return this.getValueMax();
		return x;
	}

	/**
	 * A method that checks and adjust the value for the attributes when a new
	 * unit is created
	 * 
	 * @param x
	 *            the number we have to check
	 * @return returns the minimum value if x is less than the minimum value
	 *         returns the maximum value if x is less than the maximum value
	 *         returns x if the value is between the minimum and the maximum
	 *         value | if (x less than this.getStartValueMin()) | then return
	 *         this.getValueMin(); | if (x greater than this.getValueStartMax())
	 *         | then return this.getStartValueStartMax(); | else return x
	 */
	@Raw
	private int makeNumberFitAtStart(int x) {
		if (x < this.getValueStartMin())
			return this.getValueStartMin();
		else if (x > this.getValueStartMax())
			return this.getValueStartMax();
		return x;
	}

	/* Secondary Attributes (nominal) */
	/**
	 * Return the MaxHitPoints of the unit
	 * 
	 */
	@Basic
	public int getMaxHitPoints() {
		return (int) Math.ceil(2.0 * this.getWeight() * this.getToughness() / 100);
	}

	/**
	 * Return the current Hit Points of the unit
	 * 
	 */
	@Basic
	public int getCurrentHitPoints() {
		return this.hitpoints;
	}

	/**
	 * Set the amount of hitpoints to the given amount.
	 * 
	 * @param hitpoints
	 *            The new hitpoints for the current hitpoints
	 * @pre The given amount of hitpoints must be a valid amount for an unit, in
	 *      view of the MaxHitpoints of this unit. |
	 *      isValidHitPoint(hitpoints,this.getMaxHitPoints())
	 * @post The amount of hitpoints of this unit is equal to the given amount
	 *       of hitpoints. | new.getCurrentHitPoints() == hitpoints
	 */

	public void setCurrentHitPoints(int hitpoints) {
		if(isValidHitPoint(hitpoints, this.getMaxHitPoints())){
			this.hitpoints = hitpoints;
		}
		else{
			this.hitpoints = this.getMaxHitPoints();
		}
	}

	/**
	 * variable storing the current hit points of the unit
	 */
	private int hitpoints = 0;

	/**
	 * Check whether the given Hitpoints are a valid value for the given
	 * MaxHitPoints.
	 * 
	 * @param hitpoint
	 *            The value hitpoint to check.
	 * @return True if and only if the given contents is less or equal than the
	 *         maximum amount of hitpoints, but greater or equal than 0. |
	 *         result == | (hitpoints<= this.getMaxHitPoints())&&(hitpoints>=
	 *         0);
	 */

	public static boolean isValidHitPoint(int hitpoints, int maxHitpoints) {
		return (hitpoints <= maxHitpoints) && (hitpoints >= 0);
	}

	/**
	 * Return the MaxStaminaPoints of the unit
	 * 
	 */
	@Basic
	public int getMaxStaminaPoints() {
		return (int) Math.ceil(2.0 * this.getWeight() * this.getToughness() / 100);
	}

	/**
	 * Return the current stamina Points of the unit
	 * 
	 */
	@Basic
	public int getCurrentStaminaPoints() {
		return this.stamina;
	}

	/**
	 * Set the amount of staminapoints to the given amount.
	 * 
	 * @param hitpoints
	 *            The new stamina for the current stamina
	 * @pre The given amount of stamina must be a valid amount for an unit, in
	 *      view of the MaxHitpoints of this unit. |
	 *      isValidStaminaPoint(stamina,this.getMaxStaminaPoints())
	 * @post The amount of stamina of this unit is equal to the given amount of
	 *       stamina. | new.getCurrentStaminaPoints() == stamina
	 */

	public void setCurrentStaminaPoints(int stamina) {
		 if (isValidStaminaPoint(stamina, this.getMaxStaminaPoints())){
			 this.stamina = stamina;
		 }
		 else{
			 this.stamina=this.getMaxStaminaPoints();
		 }
	}

	/**
	 * variable storing the current stamina points of the unit
	 */
	private int stamina = 0;

	/**
	 * Check whether the given staminapoints are a valid value for the given
	 * MaxStaminaPoints.
	 * 
	 * @param stamina
	 *            The value stamina to check.
	 * @return True if and only if the given contents is less or equal than the
	 *         maximum amount of stamina, but greater or equal than 0. | result
	 *         == | (stamina<= this.getMaxStaminaPoints())&&(stamina>= 0);
	 */
	@Raw
	private static boolean isValidStaminaPoint(int stamina, int maxStaminapoints) {
		return (stamina <= maxStaminapoints) && (stamina >= 0);
	}

	/* Consuming and regaining of stamina and hitpoints (nominal) */

	/**
	 * Adds a given amount of hitpoints to the current hitpoints
	 * 
	 * @param amount
	 *            The amount to be added to current hitpoints of this unit.
	 * @pre The given amount must be positive and less or equal than the maximum
	 *      amount of hitpoints (to prevent overflowing) . |
	 *      this.getMaxHitPoints()>amount > 0
	 * @effect The current amount of hitpoints of this unit is set to its
	 *         current amount incremented with the given amount of points. |
	 *         setCurrentHitPoints(getCurrentHitPoints() + amount)
	 */
	public void regainHitpoints(int amount) {
		assert (amount > 0) && (this.getMaxHitPoints() >= amount);
		this.setCurrentHitPoints(this.getCurrentHitPoints() + amount);
	}

	/**
	 * Adds a given amount of staminapoints to the current staminapoints
	 * 
	 * @param amount
	 *            The amount to be added to current stamina points of this unit.
	 * @pre The given amount must be positive and less or equal than the maximum
	 *      amount of stamina points (to prevent overflowing) . |
	 *      this.getMaxStaminaPoints()>amount > 0
	 * @effect The current amount of stamina points of this unit is set to its
	 *         current amount incremented with the given amount of points. |
	 *         setCurrentStaminaPoints(getCurrentStaminaPoints() + amount)
	 */
	public void regainStamina(int amount) {
		assert (amount > 0) && (this.getMaxStaminaPoints() >= amount);
		this.setCurrentStaminaPoints(this.getCurrentStaminaPoints() + amount);
	}

	/**
	 * Takes a given amount of staminapoints to the current staminapoints
	 * 
	 * @param amount
	 *            The amount to be token from current stamina points of this
	 *            unit.
	 * @pre The given amount must be positive and less or equal than the current
	 *      amount of stamina points. | this.getCurrentStaminaPoints()>amount >
	 *      0
	 * @effect The current amount of stamina points of this unit is set to its
	 *         current amount decremented with the given amount of points. |
	 *         setCurrentStaminaPoints(getCurrentStaminaPoints() - amount)
	 */
	public void consumeStamina(int amount) {
		assert (amount > 0) && (this.getMaxStaminaPoints() >= amount);
		this.setCurrentStaminaPoints(this.getCurrentStaminaPoints() - amount);
	}

}
