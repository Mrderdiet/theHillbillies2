package hillbillies.helpers;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.*;
/**
 * Class storing the terrain type of a cube and the boulders and logs occupying the terrain
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 *
 * @Invar The Type of a cube must be a valid Terraintype
 
 */
public class Cube {
	

	/**
	 * Constructor of this class
	 * @param type
	 * 			Value of the according terrain type
	 * @throws IllegalTerraintypeException 
	 * 			if type is not a valid type from TerrainTypes throw IllegalTerraintypeException 
	 */
	public Cube(int type) throws IllegalTerraintypeException{
		this.type= TerrainType.convertType(type);
	}
	
	/**
	 * Return the cube's type of terrain.
	 * @return
	 * 		type of cube.
	 */
	@Raw @Basic
	public int getType(){
		return this.type.getNumber();
	}
	
	
	/**
	 * Changes the cube's type of terrain
	 * @param type
	 * 			the new type of terrain
	 * @throws IllegalTerraintypeException 
	 * 			if type is not a valid type from TerrainTypes throw IllegalTerraintypeException 
	 * @post the cube type is the given type
	 * 			|new.getType()==type
	 * 
	 */
	@Raw
	public void setType(int type) throws IllegalTerraintypeException{
		this.type= TerrainType.convertType(type);
	}
	
	
	/**
	 * Variable storing the type of the cube
	 */
	private TerrainType type = TerrainType.AIR;
	
	
	/**
	 * Returns whether or not the cube is solid
	 * @return
	 * 		whether or not the cube is solid
	 * 		
	 */
	public boolean isSolid(){
		return type.isSolid();
	}
	
	
	@Override
	public String toString(){
		return this.type.toString();
	}

}
