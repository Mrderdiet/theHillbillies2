package hillbillies.helpers;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.IllegalNameException;

/**
 * A class to give objects names
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 *
 * @Invar The Name of the object must only contain valid characters
 * 			| canHaveAsName(new name)
 */
public class Name {

	/**
	 * Constructs a name tag of an object.
	 * 
	 * @param name
	 */
	public Name(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param initialPosition
	 * @throws ModelException
	 */

	/**
	 * returns the characters the name of the object may take
	 * 
	 * @return this.validCharacters
	 */
	@Basic
	@Raw
	@Immutable
	private String getValidCaracters() {
		return this.validCharacters;
	}

	/**
	 * variable storing the valid characters
	 */
	private final String validCharacters = "[a-zA-Z_\'_\\s_\"]+";

	/**
	 * returns the name of the object
	 * 
	 * @return this.name
	 */
	@Basic
	@Raw
	public String getName() {
		return this.name;
	}

	/**
	 * Sets this.name to the given value newName
	 * 
	 * @param newName
	 *            the new Value for this.name
	 * @throws IllegalNameException
	 *             if the name is not valid
	 * @post the name will be the new name if it the newName is valid |if
	 *       (this.canHaveAsName(newName) | then new.getName() == newName
	 * 
	 *
	 */
	@Raw
	public void setName(String newName) throws IllegalNameException {
		if (!this.canHaveAsName(newName))
			throw new IllegalNameException("Not a valid Name");
		else
			this.name = newName;
	}

	/**
	 * variable storing the name of the unit
	 */
	private String name;

	/**
	 * Check whether the given name is a valid name for the unit.
	 * 
	 * @param name
	 *            the string that has to be checked
	 * @return returns true if the string name contains only legal
	 *         characters(alphabetical, " " and ' and ") and the first character
	 *         is a capital and the length of the name is at least 2 characters
	 *         | if (name contains only characters, capitals letters at the
	 *         begin of each word, | length >= 2 letters and quotes) | then
	 *         return true
	 * 
	 */
	@Raw
	private boolean canHaveAsName(String name) {
		if ((Character.isUpperCase(name.charAt(0))) && (name.length() >= 2) && (name.matches("[a-zA-Z_\'_\\s_\"]+")))
			return true;
		return false;
	}

}
