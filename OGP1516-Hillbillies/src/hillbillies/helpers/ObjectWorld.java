package hillbillies.helpers;

import java.util.Random;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.IllegalWorldException;
import hillbillies.model.World;

/**
 * Class to make new boulders and logs
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 *
 * @Invar The weight must be valid
 * 
 * @Invar The position must be valid (must lie on solid ground or on z = 0 
 * 
 * @Invar The world must be a valid World
 */
public abstract class ObjectWorld {
	
	/**
	 * Constructor of this class
	 * @param pos
	 * 			The initial position
	 * @param world
	 * 			The world the ObjectWorld lives in
	 * @throws IllegalPositionException
	 * 			If the position is not a legal position throw an  IllegalPositionException 
	 * @throws IllegalWorldException
	 * 			If the world is not legal throw an IllegalWorldException 
	 */
	protected ObjectWorld(int[] pos,World world) throws IllegalPositionException, IllegalWorldException{
		this.world = world;
		this.position = new Position(pos,this.getWorld().getWorldBoudaries());
		this.checkPosition();
			
	}
	
	/* World */
	/**
	 * return the world the object is exists in
	 * @return
	 * 			the world
	 */
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * sets the world of a log to a given World
	 * @param world
	 * 			the newWorld for the log
	 * @throws IllegalWorldException 
	 * 			If the world is not legal throw an IllegalWorldException 
	 * @post the new world of the log is the given value
	 * 			|new.getWorld==world
	 */
	@Raw
	private void setWorld(World world) throws IllegalWorldException {
		this.world = world;
		//world.addObject(this);
	}
	/**
	 * Variable storing the world of the object
	 */
	private World world ;
	
	/* Position */
	/**
	 * Returns the current position of the ObjectWorld
	 * @return
	 * 			the current position of the ObjectWorld
	 */
	@Basic @Raw
	public double[] getPosition() {
		return this.position.getPosition();
		
	}
	/**
	 * Returns the current cube position of the ObjectWorld
	 * @return
	 * 			the current cube position of the ObjectWorld
	 */
	@Basic @Raw
	public int[] getCubePosition(){
		return this.position.getRoundPosition();
	}
	
	/**
	 * Returns the position of the object as a position
	 * @return the Position of the object
	 */
	public Position getExactPosition(){
		return this.position;
	}
	

	/**
	 * Sets the position to the given position
	 * @param newPos
	 * 			the given position
	 * @throws IllegalPositionException
	 * 			if the new position isn't legal throw IllegalPositionException
	 * @post The new position of the ObjectWorld is equal to the given position
	 * 			|new.getPosition() == newPos 
	 */
	@Raw
	public void setPosition(double[] newPos) throws IllegalPositionException {
		this.position=new Position(newPos, this.getWorld().getWorldBoudaries());
	}
	
	/**
	 * Variable storing the position of a ObjectWorld
	 */
	private Position position;
	
	
	/* Weight */
	/**
	 * @return the weight of the  objectWorld
	 */
	@Basic @Raw
	public int getWeight() {
		return this.weight;
	}
	
	/**
	 * Random weight setter
	 */
	private final int weight = new Random().nextInt((40) + 1) + 10;
	
	
	/* Other */
	/**
	 * Check whether this ObjectWorld is already consumed.
	 */
	@Basic
	@Raw
	public boolean isConsumed() {
		return this.isConsumed;
	}

	/**
	 * Variable reflecting whether or not this ObjectWorld is consumed.
	 */
	private boolean isConsumed= false;
	
	/**
	 *Variable reflecting whether or not this ObjectWorld is falling.
	 */
	private boolean isFalling = true;	
	
	/* Actions */
	
	
	public boolean isPickedUp(){
		return this.isPickedUp;
	}
	
	private boolean isPickedUp = false;
	

	/**
	 * Picks up the boulder 
	 * 
	 */
	public void pickUp(){
		this.isPickedUp = true;	
	}
	/**
	 * Drops the boulder on the given location
	 * @param pos
	 * 			the position of the drop
	 * @throws IllegalWorldException
	 * 
	 * @post this.isFalling is true
	 * 			|new.isFalling == true
	 * @post the boulder is added to the world
	 * @post the boulder is added to the cube
	 */
	public void Drop(double[] pos) throws IllegalWorldException{
		this.isFalling=true;
		try {
			this.position = new Position(pos,this.getWorld().getWorldBoudaries());
		} catch (IllegalPositionException e) {
		}
		this.isPickedUp = false;
	
	}
	
	/**
	 * Consumes the Boulder
	 * @post the boulder is removed from the world
	 * @post the boulder is removed from the cube
	 * @post the position of the boulder is null
	 * @post this.isConsumed is true
	 * 			|new.isConsumed == true
	 */
	public void consume() {
		if (!isConsumed()) {			
			this.position = null;
			this.isConsumed = true;
			}
		
	}
	
	public void checkPosition(){
		if (this.getWorld().isSolid(this.getCubePosition()[0], this.getCubePosition()[1], this.getCubePosition()[2])){
			this.isFalling=false;
		}else{
			this.isFalling=true;
		}
		
	}
	
	/* AdvanceTime */
	/**
	 * Updates the object location when falling
	 * @param dt
	 * 			the given timestep
	 * @throws IllegalPositionException
	 */
	public void advanceTime(double dt) throws IllegalPositionException {
		if (this.isFalling){
			this.position= new Position(new double[] { (this.position.getPosX()),
					(this.position.getPosY()),
					(this.position.getPosZ() -3.0 * dt) },this.getWorld().getWorldBoudaries());
			if (this.getWorld().isSolid(this.getCubePosition()[0],this.getCubePosition()[1], this.getCubePosition()[2])){
				this.isFalling=false;
				this.position= new Position(new double[] { (this.position.getPosX()),
						(this.position.getPosY()),
						+this.getCubePosition()[2]},this.getWorld().getWorldBoudaries());
			}
		}
		
	}

	
	
	
	
}
