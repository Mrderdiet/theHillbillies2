package hillbillies.helpers;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.IllegalPositionException;

/**
 * A class involving all the coordinates of the world (X,Y,Z)
 * 
 * @version 2.0
 * @author Dieter Verbruggen & Paolo Léonard
 * 
 * @Invar The object must be in the given boundaries.
 * 			|  canHaveCoordinate(x,y,z)
 *
 */
@Value
public class Position {

	

	/**
	 * Constructor of the class Position
	 * 
	 * @post the position of the object will be in the center of the initial
	 *       cube. |new.getPosition()== {initialPosition[0] +
	 *       0.5,initialPosition[1] + 0.5,initialPosition[2] + 0.5}
	 * 
	 * @param initialPosition
	 *            the start position for the object
	 * @throws IllegalPositionException
	 *             If the given position is outside the boundaries, throw
	 *             IllegalPositionException
	 */
	public Position(int[] initialPosition,int[] boundaries) throws IllegalPositionException {
		this.boundaries = boundaries;
		this.setPosX((double) (initialPosition[0] + 0.5));
		this.setPosY((double) (initialPosition[1] + 0.5));
		this.setPosZ((double) (initialPosition[2] + 0.5));
	}
	
	
	
	/**
	 * Constructor off the class Position
	 * 
	 * @post the position of the object will be the same as the given
	 *       coordinates. |new.getPosition()== initialPosition
	 * @param initialPosition
	 *            the start position for the object
	 * @throws IllegalPositionException
	 *             If the given position is outside the boundaries, throw
	 *             IllegalPositionException
	 */
	public Position(double[] initialPosition, int[] boundaries) throws IllegalPositionException {
		this.boundaries = boundaries;
		this.setPosition(initialPosition);
	}

	/**
	 * Returns the exact value of the X-coordinate
	 * 
	 * @return this.posX
	 */
	@Basic
	@Raw
	@Immutable
	public double getPosX() {
		return this.posX;
	}

	/**
	 * sets this.Posx too the given value x
	 * 
	 * @post the x coordinate is the given x coordinate if it is in range
	 *       |if(this.canHaveCoordinate): |then new.getPosX() == x
	 * @param x
	 *            the new value for this.posX
	 * @throws IllegalPositionException
	 *             the given value is not in range | ! canHaveCoordinate(x)
	 */
	@Raw
	private void setPosX(double x) throws IllegalPositionException {
		if (!canHaveCoordinateX(x))
			throw new IllegalPositionException("invalid x-coordinate");
		else
			this.posX = x;
	}

	/**
	 * variable storing the X-coordinate
	 */
	private double posX;

	/**
	 * Returns the exact value of the Y-coordinate
	 * 
	 * @return this.posY
	 */
	@Basic
	@Raw
	@Immutable
	public double getPosY() {
		return this.posY;
	}

	/**
	 * sets this.Posx too the given value y
	 * 
	 * @post the y coordinate is the given y coordinate if it is in range
	 *       |if(this.canHaveCoordinate): |then new.getPosX() == y
	 * @param y
	 *            the new value for this.posY
	 * @throws IllegalPositionException
	 *             the given value is not in range | ! canHaveCoordinate(y)
	 */
	@Raw
	private void setPosY(double y) throws IllegalPositionException {
		if (!canHaveCoordinateY(y)){
			throw new IllegalPositionException("invalid y-coordinate");
		}
		else
			this.posY = y;
	}

	/**
	 * variable storing the Y-coordinate
	 */

	private double posY;

	/**
	 * Returns the exact value of the Z-coordinate
	 * 
	 * @return this.posZ
	 */
	@Basic
	@Raw
	@Immutable
	public double getPosZ() {
		return posZ;
	}

	/**
	 * sets this.PosZ too the given value z
	 * 
	 * @post the z coordinate is the given z coordinate if it is in range
	 *       |if(this.canHaveCoordinate): |then new.getPosX() == z
	 * @param z
	 *            the new value for this.posZ
	 * @throws IllegalPositionException
	 *             the given value is not in range | ! canHaveCoordinate(z)
	 */
	@Raw
	private void setPosZ(double z) throws IllegalPositionException {
		if (!canHaveCoordinateZ(z))
			throw new IllegalPositionException("invalid z-coordinate");
		else
			this.posZ = z;
	}

	/**
	 * variable storing the Z-coordinate
	 */
	private double posZ;

	/**
	 * Checks whether or not a X coordinate is valid
	 * 
	 * @param cor
	 *            the value of the new Coordinate
	 * @return if the coordinate is within the given range, return true | if
	 *         (this.getCoordinateMin() <= cor <= this.getCoordinateMax()) |
	 *         then return true | else return false
	 * 
	 */
	@Raw
	private boolean canHaveCoordinateX(double cor) {
		if ((cor <= this.boundaries[0]) && (cor >= 0))
			return true;
		return false;
	}
	/**
	 * Checks whether or not a Y coordinate is valid
	 * 
	 * @param cor
	 *            the value of the new Coordinate
	 * @return if the coordinate is within the given range, return true | if
	 *         (this.getCoordinateMin() <= cor <= this.getCoordinateMax()) |
	 *         then return true | else return false
	 * 
	 */
	@Raw
	private boolean canHaveCoordinateY(double cor) {
		if ((cor <= this.boundaries[1]) && (cor >= 0))
			return true;
		return false;
	}
	/**
	 * Checks whether or not a Z coordinate is valid
	 * 
	 * @param cor
	 *            the value of the new Coordinate
	 * @return if the coordinate is within the given range, return true | if
	 *         (this.getCoordinateMin() <= cor <= this.getCoordinateMax()) |
	 *         then return true | else return false
	 * 
	 */
	@Raw
	private boolean canHaveCoordinateZ(double cor) {
		if ((cor <= this.boundaries[2]) && (cor >= 0))
			return true;
		return false;
	}
	
	/**
	 * Variable storing the boundaries
	 */
	private int[] boundaries= {50,50,50};
	/**
	 * Sets the position of an object too the given position.
	 * 
	 * @post all the coordinates will be changed too the given one (if they are
	 *       in range) |if (they are all valid): |then new.getPosition ==
	 *       position
	 * @param position
	 *            the new position for the object
	 * @throws IllegalPositionException
	 *             if one of more coordinates aren't valid, throw new exception
	 */
	@Raw
	private void setPosition(double[] position) throws IllegalPositionException {
		this.setPosX(position[0]);
		this.setPosY(position[1]);
		this.setPosZ(position[2]);

	}

	/**
	 * returns the exact position of the object
	 * 
	 * @return
	 */
	@Basic
	@Raw
	@Immutable
	public double[] getPosition() {
		return new double[] { this.getPosX(), this.getPosY(), this.getPosZ() };
	}

	/**
	 * returns the rounded (down) position of the unit
	 * 
	 * @return
	 */
	@Basic
	@Raw
	@Immutable
	public int[] getRoundPosition() {
		return new int[] { (int) this.getPosX(), (int) this.getPosY(), (int) this.getPosZ() };

	}

	/**
	 * Checks if an target position is equal to this position
	 * @param target
	 * 			the position to compare with this position
	 * @return
	 *			whether or not the target position is equal to this 
	 */
	public boolean equals(Object target) {
		if (!(target instanceof Position))
			return false;
		if (equalsDouble(this.getPosX(), ((Position) target).getPosX(), 0.10) && equalsDouble(this.getPosY(), ((Position) target).getPosY(), 0.10)
				&& equalsDouble(this.getPosZ(), ((Position) target).getPosZ(), 0.10))
			return true;

		return false;
	}

	/**
	 * helper function to check if two doubles are equal
	 * 
	 * @return true if they are equal , false otherwise
	 */
	@Raw
	private boolean equalsDouble(double x, double y, double EPSILON) {
		if (Math.abs(x - y) < EPSILON)
			return true;
		return false;
	}
	
	/**
	 * Makes a string from the position
	 */
	@Override
	public String toString(){
		return (this.getRoundPosition()[0]+0.5)+" "+(this.getRoundPosition()[1]+0.5)+" "+this.getRoundPosition()[2];
	}
	
	/**
	 * Makes a copy of the position
	 */
	public Position copy(){
		try {
			return new Position(this.getPosition(),this.boundaries);
		} catch (IllegalPositionException e) {
			return null;
		}
	}
	
	
	

}
