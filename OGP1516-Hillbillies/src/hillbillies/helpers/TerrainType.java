package hillbillies.helpers;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.IllegalTerraintypeException;


/**
 * An enumeration introducing the different types that a cube can have.
 */
@Value
public enum TerrainType {
	AIR(false, 0, "Air"),
	ROCK(true, 1, "Rock"),
	TREE(true, 2,"Tree"),
	WORKSHOP(false, 3,"Workshop");
	
	/**
	 * Initialize this TerrainType with the given name, solid or not
	 * and number.
	 * @param solid
	 * 			Whether the new TerrainType is solid or not.
	 * @param number
	 * 			The number corresponding to the new TerrainType.
	 * @param name
	 * 			The name corresponding to the new TerrainType.
	 * @param air 
	 * @post whether it is solid or not is equal to the given boolean
	 * 			|new.isSolid() == solid
	 * @post The number of the Terraintype is equal to the given number
	 * 			|new.getNumber()==number
	 * @Post The Name of the Terraintype is equal to the given Name
	 * 			|new.name==name
	 */
	private TerrainType(boolean solid,int number,String name) {
		this.solid = solid;
		this.number = number;
		this.name = name;
	}
	
	
	/**
	 * Return the number corresponding to this TerrainType.
	 */
	@Basic @Immutable
	public int getNumber() {
		return this.number;
	}
	
	/**
	 * Variable storing the corresponding number to this TerrainType.
	 */
	private final int number;
	
	/**
	 * Return whether this TerrainType is solid or not.
	 */
	@Basic @Immutable
	public boolean isSolid() {
		return this.solid;
	}
	
	/**
	 * Variable storing whether this TerrainType is solid or not.
	 */
	private boolean solid;
	
	/**
	 * variable for the name
	 */
	private String name;
	/**
	 * Static function that converts a number to its corresponding TerrainType.
	 * @param 	type
	 * 			The number that must be converted.
	 * @return	If the type is 0, the result is air. If the type
	 * 			is 1, the result is rock. If the type is 2, the
	 * 			result is tree. If the type is 3, the result is
	 * 			workshop.
	 * 		|	if (type == 0) result == TerrainType.AIR
	 * 		|	if (type == 1) result == TerrainType.ROCK
	 * 		|	if (type == 2) result == TerrainType.TREE
	 * 		|	if (type == 3) result == TerrainType.WORKSHOP
	 * 		| 	else throw an IllegalTerraintypeException
	 */
	public static TerrainType convertType(int type) throws IllegalTerraintypeException{
		TerrainType result = null;
		switch (type) {
		case 0:
			result = TerrainType.AIR;
			break;
		case 1:
			result = TerrainType.ROCK;
			break;
		case 2:
			result = TerrainType.TREE;
			break;
		case 3:
			result = TerrainType.WORKSHOP;
			break;
		default:
			throw new IllegalTerraintypeException();
		}
		
		return result;
	}
	
	@Override
	public String toString(){
		return this.name;
	}

}
