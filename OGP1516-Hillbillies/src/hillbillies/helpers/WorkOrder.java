package hillbillies.helpers;
/**
 * All the different work orders a unit can get.
 *
 */
public enum WorkOrder {
	DROP,
	WORK_AT_WORKSHOP,
	PICK_UP_BOULDER,
	PICK_UP_LOG,
	WORK_WOOD,
	WORK_ROCK,
	DO_NOTHING
}
