package hillbillies.model;

import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.IllegalWorldException;
import hillbillies.helpers.ObjectWorld;
/**
 * Class to make and manipulate Boulders
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 *
 */
public class Boulder extends ObjectWorld {
	
	
	/**
	 * The constructor of the class Boulder. Initialize the new boulder with a weight and a position and a world
	 * @param pos
	 * 			The initial position
	 * @param world
	 * 			The world the ObjectWorld lives in
	 * @throws IllegalPositionException
	 * 			If the position is not a legal position throw an  IllegalPositionException 
	 * @throws IllegalWorldException
	 * 			If the world is not legal throw an IllegalWorldException 
	 */
	public Boulder(int[] pos,World world) throws IllegalPositionException, IllegalWorldException{
		super(pos,world);
		world.addBoulder(this);	
	}
	
	/**
	 * Returns string of the boulder
	 * @return "Boulder"
	 */
	@Override
	public String toString(){
			return "Boulder";
	}
}
