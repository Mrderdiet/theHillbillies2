package hillbillies.model;

import java.util.HashSet;
import java.util.Set;
import be.kuleuven.cs.som.annotate.*;

/**
 * An enumeration introducing the different factions that a unit can have
 * It also stores the units from a faction.
 */
public enum Faction {
	House_Baratheon(0,"House Baratheon"),
	House_Lannister(1, "House Lannister" ),
	House_Stark(2,"House Stark"),
	House_Targaryen(3,"House Targaryen"),
	House_Martell(4,"House Martell");
	
	

	/**
	 * Initialize this faction with the given name and number.
	 * @param value
	 * 			the number or Id of the faction
	 * @param name
	 * 			The name of the faction
	 */
	private Faction(int value, String name){
		this.number = value;
		this.name = name;
	}
	
	/**
	 * Returns the Id of the Faction
	 * @return
	 * 			the Id of the Faction
	 */
	@Basic @Raw
	public int getId(){
		return this.number;
	}
	
	/**
	 * Variabele storing the Id
	 */
	private int number;
	
	/**
	 * Adds a new unit to this faction
	 * @param unit
	 * 			the freshly recruited unit
	 */
	@Raw
	public void recruitUnit(Unit unit){
		this.units.add(unit);
	}
	
	/**
	 * Fires units if they are death or no needed anymore 
	 * @param unit
	 * 			The unit that we have to let go
	 */
	@Raw
	public void fireUnit(Unit unit){
		this.units.remove(unit);
	}
	
	/**
	 * Returns a set of all the units of a given faction
	 * 
	 * @return a set of all the units of a given faction
	 */
	@Raw @Basic
	public Set<Unit> getUnits(){
		return this.units;
	}
	
	/**
	 * Variable storing all the units of a faction
	 */
	private Set<Unit> units = new HashSet<Unit>();
	
	/**
	 * Returns the name of a faction
	 * @return
	 * 			the name of a faction
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Variabele Storing the name
	 */
	private String name;
	
	
	//TODO deel van deel 3
	
	public Scheduler getScheduler(){
		return this.scheduler;
	}
	
	private final Scheduler scheduler = new Scheduler();
	
	/**
	 * Makes a string from a faction
	 */
	@Override
	public String toString(){
		return this.getName();
	}

}
