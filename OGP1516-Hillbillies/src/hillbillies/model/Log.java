package hillbillies.model;

import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.IllegalWorldException;
import hillbillies.helpers.ObjectWorld;

public class Log extends ObjectWorld {
	/**
	 * The constructor of the class Log. Initialize the new log with a weight
	 * and a position.
	 * 
	 * @param pos
	 *            The position of the log.
	 * @throws IllegalPositionException
	 * @throws IllegalWorldException
	 */
	public Log(int[] pos, World world) throws IllegalPositionException, IllegalWorldException {
		super(pos, world);
		world.addLog(this);
	}

	
	/**
	 * Returns string of the log
	 * 
	 * @return "log"
	 */
	@Override
	public String toString() {
		return "Log";
	}
}
