package hillbillies.model;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * A class to make Schedulers
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 */
public class Scheduler{
	
	/**
	 * Constructor of the class Scheduler
	 * The scheduler is basically a list of tasks.
	 */
	public Scheduler(){
		this.tasks = new ArrayList<Task>();
	}
	
	/**
	 * Adds task to the scheduler
	 * @param task
	 * 			The task that is to be added
	 * @pre the task must not already be in the list
	 * 			| this.tasks.contains(task)= false
	 * @post the task is added to the list of tasks
	 * 			| new.tasks.contains(task)==True
	 * @post the scheduler will be added to the given task
	 * 			|task.getSchedulers().contains(this)= true
	 */
	public void schedule(Task task) {
		if (!this.tasks.contains(task)){
			this.tasks.add(task);
			task.addScheduler(this);
			}
		
	}
	
	/**
	 * Removes a task from the scheduler
	 * @param task
	 * 			the task that has to be removed
	 * @pre the task must be in  the list of tasks
	 * 			|  this.tasks.contains(task)= true
	 * @post the task is not in the list of tasks
	 * 			| new.tasks.contains(task)=false
	 * @post the scheduler will be removed from the given task
	 * 			|task.getSchedulers().contains(this)= false
	 */
	public void removeTask(Task task){
		if (this.tasks.contains(task)){
			this.tasks.remove(task);
			task.removeScheduler(this);
			}
	}
	
	/**
	 * Replaces a given task with a given replacement
	 * @param original
	 * 			The task that has to be replaced
	 * @param replacement
	 * 			The replacement of the task
	 * @pre the replacement must not already be in the list
	 * 			| this.tasks.contains(replacement)= false
	 * @pre the original must already be in the list
	 * 			| this.tasks.contains(original)= true
	 * @post the original is not in the list of tasks
	 * 			| new.tasks.contains(original)=false
	 * @post the replacement is added to the list of tasks
	 * 			| new.tasks.contains(replacement)==True
	 * @post the tasks will be reseted
	 * 			| original.reset()
	 * 			| replacement.reset()
	 */
	public void replace(Task original, Task replacement) {
		original.reset();
		replacement.reset();
		this.schedule(replacement);
		this.removeTask(original);
	}
	
	/**
	 * Returns the task with highest priority that is not already assigned to an other unit
	 * @return
	 * 			available task with highest priority
	 */
	public Task getTask(){
		Iterator<Task> iterator = this.iterator();
		while (iterator.hasNext()){
			Task t = iterator.next();
			if (t.getAssignedUnit()==null)
				return t;
		}
		return null;
	}
	
	/**
	 * Returns whether or not the given collection of tasks are part of the list of tasks
	 * @param tasks
	 * 			a collection of tasks that have to be checked
	 * @return
	 * 			Boolean:
	 * 			| true if the tasks occur in the the list of all tasks 
	 * 			| false if not
	 */
	public boolean areTasksPartOf(Collection<Task> tasks) {
		return this.tasks.containsAll(tasks);
	}
	
	/**
	 * Variable storing the list of tasks.
	 */
	private List<Task> tasks ;
	
	/**
	 * Returns an iterator of all the tasks
	 * @return
	 * 			iterator of tasks 
	 */
	public Iterator<Task> iterator() {
		this.sortList();
		Iterator<Task> iterator = new Iterator<Task>() {
			private int index;
			
			@Override
			public boolean hasNext() {
				return index< tasks.size() && tasks.get(index)!= null;
			}
			@Override
			public Task next() {
				return tasks.get(index++);
			}
		};
		
		return iterator;
	}
	
	/**
	 * Sorts the list of tasks so the task with highest priority will be first
	 * (using an anonymous class)
	 */
	private void sortList(){
		this.tasks.sort(Comparator.comparingInt(Task::getPriority).reversed());
	}
}
