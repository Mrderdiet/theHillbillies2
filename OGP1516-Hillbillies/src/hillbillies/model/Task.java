package hillbillies.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hillbillies.exceptions.BreakStatementException;
import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.statements.Statement;
import hillbillies.tasks.type.Type;

/**
 * Class to make and manage tasks
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 */
public class Task {
	
	/**
	 * Constructor of a Task
	 * @param name
	 * 			the name of the task
	 * @param priority
	 * 			the priority of the task
	 * @param activity
	 * 			the activity (statement) of the task
	 * @param select
	 * 			the selected location of the task
	 * @invar the statement must not be null and must be well formed
	 * 			| activity != null
	 * 			| activity.isWellFormed() == true
	 */
	@SuppressWarnings("rawtypes")
	public Task(String name, int  priority, Statement activity, int[] select){
		if (activity == null || !activity.isWellFormed(false)){
			throw new NullPointerException("activity not wellformed");
		}
		this.name = name;
		this.priority = priority;
		this.activity = activity;
		this.globalVariables = new HashMap<String,Expression>();
		if (select != null)
			this.selected = select;
	}
	
	/**
	 * Returns the assigned unit of this task , null if the task isn't assigned yet
	 * @return
	 * 			unit
	 */
	public Unit getAssignedUnit() {
		return this.unit;
	}
	
	/**
	 * Assigns a given unit to this task
	 * @param unit
	 * 			the unit to assign the task to
	 * @post the unit will be the assigned unit
	 * 			|new.getAssingedUnit() = unit  
	 */
	public void assignTo(Unit unit){
		this.unit = unit;
	}
	
	/**
	 * Variable storing the unit
	 */
	private Unit unit;

	/**
	 * returns the selected cube
	 * @return
	 * 			list of integers
	 */
	public int[] getSelectedCube(){
		return this.selected;
	}
	/**
	 * Variable storing the selected location
	 */
	private int[] selected;

	/**
	 * Returns the name of the task
	 * @return
	 * 			String name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Variable storing the name of the task.
	 */
	private final String name;

	/**
	 * Returns the priority of the task
	 * @return
	 * 			integer the priority
	 */
	public int getPriority() {
		return this.priority;
	}
	
	/**
	 * Reduces the priority of the the task by 1
	 * @post the priority will be reduced by 1
	 * 			|new.getPriority()= old.getPriority()-1
	 */
	private void reducePriority(){
		this.priority--;
	}
	
	/**
	 * Variable storing the priority
	 */
	private int priority;

	/**
	 * Returns the activity of the task
	 * @return
	 * 			Statement 
	 */
	private Statement getActivity(){
		return this.activity;
	}
	
	/**
	 * Variable storing the activity of the task
	 */
	private Statement activity;
	
	/**
	 * checks whether or not the task is well formed
	 * @return
	 * 			Boolean
	 * 			|this.getActivity().isWellFormed(false)
	 */
	public boolean isWellFormed() {
		return this.getActivity().isWellFormed(false);
	}
	
	/**
	 * Checks whether or not the task is executed
	 * @return
	 * 			Boolean
	 * 			|this.getActivity().isWellFormed(false)
	 */
	public boolean isExecuted() {
		return this.getActivity().isExecuted();
	}

	/**
	 * tries to execute one statement every 0.001 seconds
	 * @param dt
	 * 			the time that has passed 
	 */
	public void execute(double dt){
		int nbRemainingExpressions = ((int)(dt * 1000));
		try {
			this.getActivity().execute(this, nbRemainingExpressions);
		} catch (NoMoreExpressionException | BreakStatementException |IllegalArgumentExceptions e) {
			
		}
	}
	
	/**
	 * Adds a variable to the list of variables
	 * @param name
	 * 			Name of the variable
	 * @param variable
	 * 			Value of the variable
	 */
	public <T extends Type> void addVariable(String name, Expression<T> variable){
		this.globalVariables.put(name, variable);
	}
	
	/**
	 * Returns the value of the variable
	 * @param name
	 * 			the name of the variable we must have
	 * @return
	 * 			Expression
	 */
	@SuppressWarnings("unchecked")
	public <T extends Type> Expression<T> getVariable(String name){
		return  this.globalVariables.get(name);
	}

	/**
	 * Map storing all the global variables
	 */
	@SuppressWarnings("rawtypes")
	private Map<String,Expression> globalVariables;
	
	/**
	 * Get the set of schedulers of witch this task is part of
	 * @return
	 * 			set of schedulers
	 */
	public Set<Scheduler> getSchedulers() {
		return this.schedulers;
	}
	
	/**
	 * Adds scheduler to the set
	 * @param scheduler
	 * 			the scheduler to add
	 */
	public void addScheduler(Scheduler scheduler){
		if (!this.getSchedulers().contains(scheduler))
			this.schedulers.add(scheduler);
	}
	
	/**
	 * Removes scheduler from set
	 * @param scheduler
	 * 			the scheduler to remove
	 */
	public void removeScheduler(Scheduler scheduler){
		if (this.getSchedulers().contains(scheduler))
			this.schedulers.remove(scheduler);
	}
	/**
	 * Variable storing the schedulers
	 */
	private Set<Scheduler> schedulers = new HashSet<Scheduler>();

	/**
	 * Removes this task from the world 
	 */
	public void remove() {
		this.unit=null;
		for (Scheduler s: this.getSchedulers()){
			s.removeTask(this);
			this.removeScheduler(s);
		}
		
		
	}

	/**
	 * resets this task 
	 */
	public void reset() {
		if (this.getAssignedUnit()!=null){
			this.getAssignedUnit().setTask(null);
			this.reducePriority();
			this.assignTo(null);
			}
		this.getActivity().reset();
	}
}
