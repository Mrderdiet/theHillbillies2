package hillbillies.model;


import hillbillies.exceptions.*;
import hillbillies.helpers.*;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import ogp.framework.util.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class to make new Units
 * 
 * @version 2.0
 * @author Dieter Verbruggen & Paolo Léonard
 *
 *
 * @Invar The unit must have a valid name
 * 			|Name.canHaveAsName(unit.getName())
 * @Invar The unit must have a valid position
 * 			|Position.canHaveAsCordinates(unit.getPosition)
 * @Invar The unit must have a valid attributes (weight, agility, strength, toughness) with values between 0 and 200
 * 			|0<unit.getWeight()<200
 * 			|0<unit.getAgility()<200
 * 			|0<unit.getStrength()<200
 * 		  	|0<unit.getTougness()<200
 * @Invar The unit must have a valid weight with value between (strength+ Agility)/2 and 200
 * 			|(unit.getStrength() + unit.getAgility())/2<unit.getWeight()<200
 * @Invar The unit must have valid hitpoints and valid stamina points (values between 0 and (2 * unit.getWeight() * unit.getToughness() / 100))
 * 			|0<unit.getCurrentHitPoints()<(2*unit.getWeight() * unit.getToughness() / 100)
 * 			|0<unit.getCurrentStaminaPoints()<(2*unit.getWeight() * unit.getToughness() / 100)
 * @Invar The orientation of a unit must always lie between 0 and 2PI
 *        	|0<unit.getOrientation()<2*PI
 * 
 */

public class Unit {
	/**
	 * Constructor for a unit
	 * 
	 * @param name
	 *            The name of the unit.
	 * @param initialPosition
	 *            The initialPosition of the unit.
	 * @param weight
	 *            The weight of the unit.
	 * @param agility
	 *            The agility of the unit.
	 * @param strength
	 *            The strength of the unit.
	 * @param toughness
	 *            The toughness of the unit.
	 * @param enableDefaultBehavior
	 *            Enables the default behaviour of the unit.
	 * @post The unit has the given argument as variables |this.name == name
	 *       |this.position == initialPosition |this.attributes == attributes
	 *       |this.orientation == DEFAULT_ORIENTATION
	 * @throws IllegalArgumentExceptions
	 *             If the created unit has invalid arguments it throws a
	 *             IllegalArgumentExeptions.
	 * @throws IllegalPositionException
	 *             If the created unit has a invalid position it throws a
	 *             IllegalPositionException.
	 */
	@Raw
	public Unit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness,
			boolean enableDefaultBehavior,World world) throws IllegalArgumentExceptions, IllegalPositionException {
		this.setWorld(world);
		this.name = new Name(name);
		try {
			this.position = new Position(initialPosition,world.getWorldBoudaries());
		} catch (IllegalPositionException e) {
			throw new IllegalArgumentExceptions();
		}
		this.attributes = new Attributes(weight, agility, strength, toughness);
		this.setOrientation(DEFAULT_ORIENTATION);
		this.setCurrentStaminaPoints(this.getMaxStaminaPoints());
		this.setCurrentHitPoints(this.getMaxHitPoints());
		this.setDefaultBehaviour(enableDefaultBehavior);
	}
	/**
	 * constructor, no given world
	 * @throws IllegalTerraintypeException 
	 */
	@Raw
	public Unit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness,
			boolean enableDefaultBehavior) throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException {
		this(name,initialPosition,weight,agility,strength,toughness,enableDefaultBehavior,new World(new int [initialPosition[0]+1][initialPosition[1]+1][initialPosition[2]+1], new DefaultTerrainChangeListener()));
	}
	
	
	
	/**
	 * Check whether this unit is terminated.
	 */
	@Basic
	@Raw
	public boolean isAlive() {
		return !this.isTerminated;
	}

	/**
	 * Terminate this Unit.
	 * @throws IllegalWorldException 
	 *
	 * @post   This person is terminated.
	 *       | new.isTerminated()
	 */
	public void terminate() {
		if (this.isCarryingBoulder()|| this.isCarryingLog())
			try {
				this.drop();
			} catch (IllegalPositionException | IllegalWorldException e) {
			}
		this.position = null;
		this.isTerminated = true;		
	}
	
	/**
	 * Variable storing 
	 */
	public boolean isTerminated = false;
	
	/* World */
	
	/**
	 * return the world the unit is living in
	 * @return the world of the unit.
	 */
	@Raw @Basic
	public World getWorld() {
		return world;
	}
	
	/**
	 * Method that sets the world of a unit to a given World
	 * @param world
	 * 			the newWorld for the unit
	 * @post the new world of the unit is the given value
	 * 			|new.getWorld==world
	 */
	@Basic
	public void setWorld(World world) {
		this.world = world;
	}
	
	/**
	 * Variable storing the world of the unit
	 */
	private World world ;
	
	/* Faction */
	
	/**
	 * Method that returns the faction of the unit
	 * @return
	 * 			the faction of the unit
	 */
	@Raw @Basic
	public Faction getFaction() {
		return this.faction;
		}
	
	/**
	 * sets the faction of the unit to a given faction
	 * @param faction
	 * 			the new faction of the unit
	 * @post the new faction of the unit is the given faction
	 * 			|new.getFaction()==faction
	 */
	@Basic
	public void setFaction(Faction faction){
		this.getFaction().fireUnit(this);
		this.faction = faction;
		this.getFaction().recruitUnit(this);
	}
	
	/**
	 * sets the faction of a unit to the unit with corresponding id
	 * @param id
	 * 			the id of the faction the unit's faction has to chance into
	 * @post the id of the new faction is equal to the given value
	 * 			|new.getFaction().getId()== id
	 */
	@Raw @Basic
	public void setFaction(int id){
		for (Faction faction : Faction.values()) {
			 if (id==faction.getId())
				 this.faction = faction;
			}
		
	}
	/**
	 * Variable storing the faction of a unit
	 */
	private Faction faction; 
	
	
	/* Task */
	//TODO documentatie
	@Raw @Basic
	public Task getAssignedTask() {
		return this.task;
	}
	
	public void setTask(Task task){
		this.task = task;
	}
	
	private Task task;
	
	/* Log */
	
	/**
	 * Give if a unit carries a log or not
	 * @return true if carriesLog==true
	 * @return false if carriesLog==false
	 */
	public boolean isCarryingLog(){
		return this.isCarryingLog;
	}
	
	private boolean isCarryingLog = false;
	
	/* Boulder */
	
	/**
	 * Give if a unit carries a boulder or not
	 * @return true if carriesBoulder==true
	 * @return false if carriesBoulder==false
	 */
	public boolean isCarryingBoulder(){
		return this.isCarryingBoulder;
	}
	
	private boolean isCarryingBoulder = false;
	
	/* Position and name (defensive) */

	/**
	 * returns the exact coordinates of the unit
	 * 
	 * @return the exact coordinates
	 */
	@Basic
	@Raw
	public double[] getPosition() {
		return this.position.getPosition();
	}

	/**
	 * returns the rounded coordinates of the unit
	 * 
	 * @return the rounded coordinates
	 */
	@Basic
	@Raw
	public int[] getCubeCoordinate() {
		return this.position.getRoundPosition();
	}
	
	/**
	 * Returns the position of the unit as a position
	 * @return the Position of the unit
	 */
	public Position getExactPosition(){
		return this.position;
	}

	/**
	 * variable storing the position of the unit
	 */
	private Position position;

	/**
	 * returns the name of the unit
	 * 
	 * @return the name of the unit.
	 */
	@Basic
	@Raw
	public String getName() {
		return this.name.getName();
	}

	/**
	 * Sets this.name to the given value newName
	 * 
	 * 
	 * @param newName
	 *            the new Value for this.name
	 * @effect the name will be set to the given String (if possible)
	 *         |new.getName == newName
	 * @throws IllegalNameException
	 */
	@Raw
	public void setName(String newName) throws IllegalNameException {
		this.name.setName(newName);
	}

	/**
	 * variable storing the name of the unit
	 */
	private Name name;

	/* Primary Attributes (total) */

	/**
	 * Method that give if a unit has already got experience from a task.
	 * @return true if hasGotExperience == true
	 * @return false if hasGotExperience == false;
	 */
	public boolean hasGotExperience(){
		return this.hasGotExperience;
	}
	
	private boolean hasGotExperience;
	
	/**
	 * Give the current amount of experience of a unit.
	 * @return this.currentExperience
	 */
	public int getExperience(){
		return this.currentExperience;
	}
	
	/**
	 * Set the experience to a new amount.
	 * @param exp the new amount of experience of the unit
	 * @throws IllegalArgumentException
	 * 		  | if (exp <= 0) then throw IllegalArgumentException;
	 * @post if (exp >= 10) then random(setStrength(getStrenght()+1), 
	 * 									setAgility(getAgility()+1),
	 * 									setToughness(getToughness()+1));
	 */
	public void setExperience(int exp) throws IllegalArgumentException{
		if (exp <= 0){
			throw new IllegalArgumentException("Invalid experience");
		}
		else if (exp >= 10){
			this.currentExperience = exp % 10;
			//0==strength, 1==agility, 2==toughness;
			if (rand.nextInt(3)==0){
				this.setStrength(this.getStrength()+1);
			}
			else if(rand.nextInt(3)==1){
				this.setAgility(this.getAgility()+1);
			}
			else{
				this.setToughness(this.getToughness()+1);
			}
		}
		else{
			this.currentExperience = exp;
		}
	}
	
	/**
	 * The current unit's experience.
	 */
	private int currentExperience;
	
	/**
	 * Return the strength of the unit
	 * 
	 */
	@Basic
	@Raw
	public int getStrength() {
		return this.attributes.getStrength();
	}

	/**
	 * sets this.strength to given strength, it adjust the value if needed
	 * 
	 * @param strength
	 *            the strength of the unit
	 * @effect the strength will be set to the right value |new.getStrength()==
	 *         strength
	 */
	@Raw
	public void setStrength(int strength) {
		this.attributes.setStrength(strength);
	}

	/**
	 * Return the agility of the unit
	 * 
	 */
	@Basic
	@Raw
	public int getAgility() {
		return this.attributes.getAgility();
	}

	/**
	 * sets the agility to given agility, it adjust the value if needed
	 * 
	 * @param agility
	 *            the agility of the unit
	 * @effect the agility will be set to the right value |new.getAgility()==
	 *         agility
	 */
	@Raw
	public void setAgility(int agility) {
		this.attributes.setAgility(agility);
	}

	/**
	 * Return the toughness of the unit
	 * 
	 */
	@Basic
	@Raw
	public int getToughness() {
		return this.attributes.getToughness();
	}

	/**
	 * sets this.toughness to given toughness, it adjust the value if needed
	 * 
	 * @param toughness
	 *            the toughness of the unit
	 * @effect the toughness will be set to the right value
	 *         |new.getToughness()== toughness
	 */
	@Raw
	public void setToughness(int toughness) {
		this.attributes.setToughness(toughness);
	}

	/**
	 * Return the toughness of the unit
	 * 
	 */
	@Basic
	@Raw
	public int getWeight() {
		return this.attributes.getWeight()+ this.weightCarrying;
	}

	/**
	 * sets this.weight to given toughness, it adjust the value if needed
	 * 
	 * @param weight
	 *            the weight of the unit
	 * @effect the weight will be set to the right value |new.getWeight()==
	 *         weight
	 */
	public void setWeight(int weight) {
		this.attributes.setWeight(weight);
	}

	/**
	 * variable storing all the attributes
	 */
	private Attributes attributes;

	/* Secondary Attributes (nominal) */

	/**
	 * Return the MaxHitPoints of the unit
	 * 
	 */
	@Basic
	public int getMaxHitPoints() {
		return this.attributes.getMaxHitPoints();
	}

	/**
	 * Return the current Hit Points of the unit
	 * 
	 */
	@Basic
	public int getCurrentHitPoints() {
		return this.attributes.getCurrentHitPoints();
	}

	/**
	 * Set the contents of this Hitpoints to the given amount.
	 * 
	 * @param hitpoints
	 *            The new hitpoints for the current hitpoints
	 * @pre The given amount of hitpoints must be a valid amount for an unit, in
	 *      view of the MaxHitpoints of this unit. |
	 *      isValidHitPoint(hitpoints,this.getMaxHitPoints())
	 * @post The amount of hitpoints of this unit is equal to the given amount
	 *       of hitpoints. | new.getCurrentHitPoints() == hitpoints
	 */

	public void setCurrentHitPoints(int hitpoints) {
		if (hitpoints <= 0){
			this.isTerminated=true;
		}
		this.attributes.setCurrentHitPoints(hitpoints);
	}

	/**
	 * Return the MaxStaminaPoints of the unit
	 * 
	 */
	@Basic
	public int getMaxStaminaPoints() {
		return this.attributes.getMaxStaminaPoints();
	}

	/**
	 * Return the current stamina Points of the unit
	 * 
	 */
	@Basic
	public int getCurrentStaminaPoints() {
		return this.attributes.getCurrentStaminaPoints();
	}

	/**
	 * Set the contents of this oil tank to the given amount.
	 * 
	 * @param hitpoints
	 *            The new stamina for the current stamina
	 * @pre The given amount of stamina must be a valid amount for an unit, in
	 *      view of the MaxHitpoints of this unit. |
	 *      isValidStaminaPoint(stamina,this.getMaxStaminaPoints())
	 * @post The amount of stamina of this unit is equal to the given amount of
	 *       stamina. | new.getCurrentStaminaPoints() == stamina
	 */

	public void setCurrentStaminaPoints(int stamina) {
		this.attributes.setCurrentStaminaPoints(stamina);
	}

	/* Consuming and regaining of stamina and hitpoints (nominal) */

	/**
	 * Adds a given amount of hitpoints to the current hitpoints
	 * 
	 * @param amount
	 *            The amount to be added to current hitpoints of this unit.
	 * @pre The given amount must be positive and less or equal than the maximum
	 *      amount of hitpoints (to prevent overflowing) . |
	 *      this.getMaxHitPoints()>amount > 0
	 * @effect The current amount of hitpoints of this unit is set to its
	 *         current amount incremented with the given amount of points. |
	 *         setCurrentHitPoints(getCurrentHitPoints() + amount)
	 */
	public void regainHitpoints(int amount) {
		this.attributes.regainHitpoints(amount);
	}

	/**
	 * Adds a given amount of stamina points to the current stamina points
	 * 
	 * @param amount
	 *            The amount to be added to current stamina points of this unit.
	 * @pre The given amount must be positive and less or equal than the maximum
	 *      amount of stamina points (to prevent overflowing) . |
	 *      this.getMaxStaminaPoints()>amount > 0
	 * @effect The current amount of stamina points of this unit is set to its
	 *         current amount incremented with the given amount of points. |
	 *         setCurrentStaminaPoints(getCurrentStaminaPoints() + amount)
	 */
	public void regainStamina(int amount) {
		this.attributes.regainStamina(amount);
	}

	/**
	 * Takes a given amount of staminapoints to the current staminapoints
	 * 
	 * @param amount
	 *            The amount to be token from current stamina points of this
	 *            unit.
	 * @pre The given amount must be positive and less or equal than the current
	 *      amount of stamina points. | this.getCurrentStaminaPoints()>amount >
	 *      0
	 * @effect The current amount of stamina points of this unit is set to its
	 *         current amount decremented with the given amount of points. |
	 *         setCurrentStaminaPoints(getCurrentStaminaPoints() - amount)
	 */
	public void consumeStamina(int amount) {
		this.attributes.consumeStamina(amount);
	}

	/* Orientation (total) */

	private final float DEFAULT_ORIENTATION = (float) (Math.PI / 2);

	/**
	 * Return the orientation of the unit
	 * 
	 */
	@Basic
	@Raw
	public float getOrientation() {
		return this.orientation;
	}

	/**
	 * sets this.orientation to given orientation, it adjust the value if needed
	 * 
	 * @param orientation
	 *            the orientation of the unit
	 * @post The orientation will lay between 0 and 2*PI | new.orientation =
	 *       orientation%(2*PI)
	 */
	@Raw
	public void setOrientation(float orientation) {
		this.orientation = (float) (orientation % (2 * Math.PI));
	}

	/**
	 * variable storing the orientation of the unit
	 */
	private float orientation;

	/**
	 * Changes the orientation of the unit by a given amount
	 * 
	 * @param amount
	 *            The amount to be added to the orientation of this unit.
	 * @effect the orientation is changed by the amount |
	 *         this.setOrientation(this.getOrientation() +amount)
	 * @note the amount can be any value you want, we only look at the modulo
	 *       with 2*PI
	 * @note this function is not yet in use
	 */
	public void rotateUnit(float amount) {
		this.setOrientation(this.getOrientation() + amount);
	}

	/* Advanced time (defensive) */

	

	/**
	 * A method to update the interaction of the Unit with the world.
	 * 
	 * @param dt
	 *            a variable indicating the time that have passed
	 * @throws IllegalActionException
	 *             if one of more of the update functions went wrong, throw an
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	public void advanceTime(double dt) throws IllegalActionException {
		if (isValidTime(dt)&& this.isAlive()) {
			try {
				if(this.getAssignedTask()!=null){
					if(this.getAssignedTask().isExecuted()){
						this.getAssignedTask().remove();
						this.setTask(null);
					}else if(!this.isAttacking()&&!this.isMoving()&&!this.isResting()&&!this.isWorking()){
							this.getAssignedTask().execute(dt);
					}
				}
				this.updateMovingTo(dt);
				this.updateMovement(dt);
				this.updateSprinting(dt);
				this.updateAction(dt);
				this.updateFight(dt);
				this.updateDefaultSpeed();
				this.falling();
			} catch (IllegalArgumentExceptions | IllegalPositionException e) {
				throw new IllegalActionException();
			}
		}
	}


	/**
	 * Checks whether a given time is between certain amounts
	 * 
	 * @post The time is between 0 and 0,2.
	 * @return returns true if the time is between 0 and 0.2, false otherwise
	 *         |if ((dt <= 0.2) & (dt > 0)) |then return true |else return false
	 */
	@Raw
	private boolean isValidTime(double dt) {
		if ((dt <= 0.2) & (dt > 0))
			return true;
		return false;
	}

	/* Basic Movement (defensive) */

	/**
	 * Returns the value of the default speed
	 * 
	 * @return this.defaultSpeed;
	 */
	@Basic
	@Raw
	public double getDefaultSpeed() {
		return this.defaultSpeed;
	}

	/**
	 * variable storing the default speed of the unit
	 */
	private double defaultSpeed = 0;

	/**
	 * Returns the value of the current speed
	 * @pre this.isMoving()==true;
	 * @post old.getDistance() == sqrt((this.target.getPosX() - this.position.getPosX())^2,
	 *								   (this.target.getPosY() - this.position.getPosY())^2,
	 *								   (this.target.getPosZ() - this.position.getPosZ())^2);
	 * @post
	 *		|If unit.isFalling()  && unit.isMoving()
	 * 		|	then unit.getCurrentSpeed()[0] == 0;
	 * 		|	then unit.getCurrentSpeed()[1] == 0;
	 * 		|	then unit.getCurrentSpeed()[2] == -3;
	 * 		|else if unit.isMoving()
	 * 		|	then unit.getCurrentSpeed()[0] == (this.target.getPosX() - this.position.getPosX()) * this.getDefaultSpeed() / this.getDistance();
	 * 		|   then unit.getCurrentSpeed()[1] == (this.target.getPosY() - this.position.getPosY()) * this.getDefaultSpeed() / this.getDistance();
	 *  	|   then unit.getCurrentSpeed()[2] == (this.target.getPosZ() - this.position.getPosZ()) * this.getDefaultSpeed() / this.getDistance();
	 * 		|else
	 * 		|	then unit.getCurrentSpeed()[0] == 0;
	 * 		|	then unit.getCurrentSpeed()[1] == 0;
	 * 		|	then unit.getCurrentSpeed()[2] == 0;
	 * 
	 * @return this.currentSpeed
	 */
	@Basic
	@Raw
	public double[] getCurrentSpeed() {
		this.updateDefaultSpeed();
		this.setDistance((this.target.getPosX() - this.position.getPosX()),
				(this.target.getPosY() - this.position.getPosY()), (this.target.getPosZ() - this.position.getPosZ()));
		if (this.isMoving() && this.isFalling()==true) {
			return  new double[] {
					(-(this.target.getPosX() - this.position.getPosX()) * this.getDefaultSpeed() / this.getDistance()),
					(-(this.target.getPosY() - this.position.getPosY()) * this.getDefaultSpeed() / this.getDistance()),
					(-(this.target.getPosZ() - this.position.getPosZ()) * this.getDefaultSpeed() / this.getDistance()) };
		}
		else if(this.isMoving()){
			return  new double[] {
					((this.target.getPosX() - this.position.getPosX()) * this.getDefaultSpeed() / this.getDistance()),
					((this.target.getPosY() - this.position.getPosY()) * this.getDefaultSpeed() / this.getDistance()),
					((this.target.getPosZ() - this.position.getPosZ()) * this.getDefaultSpeed() / this.getDistance()) };
		}
		else {
			return new double[] { 0, 0, 0 };
		}
	}


	/**
	 * A method to initiate movement to a neighboring cube
	 * 
	 * @param dx
	 *            component of the vector showing the direction of the unit in the x-as.
	 * @param dy
	 *            component of the vector showing the direction of the unit in the y-as.
	 * @param dz
	 *            component of the vector showing the direction of the unit in the z-as.
	 * @post this.isWorking()==false
	 * @post this.isResting()==false
	 * @post this.target == new Position(new double[] { 
						(this.position.getPosX() + dx),
						(this.position.getPosY() + dy), 
						(this.position.getPosZ() + dz) },
						this.world.getWorldBoudaries())}
	 * @post this.isMoving == true;
	 * @throws IllegalArgumentExceptions
	 *             throws a ModelExeption when the unit is already moving. or
	 *             throws a exception if the target position is invalid
	 */
	public void moveToAdjacent(int dx, int dy, int dz) throws IllegalArgumentExceptions {
		this.stopWorking();
		this.stopResting();
		if (this.isMoving()){
			throw new IllegalArgumentExceptions("Already moving");
		}
		else if (this.isPassable((int)this.position.getPosX()+dx,(int)this.position.getPosY()+dy,(int)this.position.getPosZ()+1+dz)){
			this.setOrientation((float) Math.atan2(dy, dx));
			try {
				this.target = new Position(new double[] { 
						(this.position.getPosX() + dx),
						(this.position.getPosY() + dy), 
						(this.position.getPosZ() + dz) },
						this.world.getWorldBoudaries());
				} catch (IllegalPositionException e) {
				throw new IllegalArgumentExceptions("Illegal coordonnates");
			}
			this.isMoving = true;
		}
		else{
			throw new IllegalArgumentException("Solid cube, not passable");
		}
	}

	/**
	 * variable storing the target location
	 */
	public Position target = new Position(new int[] {0,0,0} , new int[] {100,100,100});

	/**
	 * A method to initiate movement between a target named cube and the current
	 * position.
	 * 
	 * @param path
	 *            The distance in any direction from the current position the
	 *            the target(cube)
	 * @param cube
	 *            The cube that is the new target.
	 * @post new.cube==cube
	 * @post new.path == this.cube.getRoundPosition()-this.getCubeCoordinate()
	 * @throws IllegalArgumentExceptions
	 *             throw an exception if cube is not a valid coordinate.
	 */
	public void moveTo(int[] cube) throws IllegalArgumentExceptions {
		try {
			this.isMoving=false;
			this.route.clear();//TODO follow
			this.cube = new Position(cube,this.world.getWorldBoudaries());
			this.isMovingTo = true;
			this.route = this.recoverDijkstra(this.getCubeCoordinate(), this.cube.getRoundPosition());
		} catch (IllegalPositionException e) {
			throw new IllegalArgumentExceptions();

		}
	}
	
	List<int[]> routeTest;
	/**
	 * variables storing the cube and whether or not the unit is
	 * moving to a distance location
	 */
	private Position cube;
	
	private boolean isMovingTo;

	/**
	 * returns the distance between target and the current position of the unit
	 * 
	 * @return this.distance
	 */
	@Basic
	private double getDistance() {
		return this.distance;
	}

	/**
	 * calculates the distance and stores it
	 * 
	 * @param dx
	 *            distance between the two x-values
	 * @param dy
	 *            distance between the two y-values
	 * @param dz
	 *            distance between the two z-values
	 * @post the distance is equal to the distance between the current position
	 *       and the target position |new.distance == Math.sqrt(Math.pow(dx, 2)
	 *       + Math.pow(dy, 2) + Math.pow(dz, 2));
	 */
	private void setDistance(double dx, double dy, double dz) {
		this.distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2));
	}

	/**
	 * variable storing the distance between the position of the unit and the
	 * target
	 */
	private double distance;

	/**
	 * returns whether or not a unit is moving.
	 * 
	 * @return this.isMoving
	 */
	@Basic
	@Raw
	public boolean isMoving() {
		return this.isMoving;
	}

	/**
	 * variable storing whether or not a unit is moving.
	 */
	private boolean isMoving = false;

	/**
	 * Checks whether or not a unit is on his target position.
	 * 
	 * @return
	 */
	private boolean isArrived() {
		return this.position.equals(this.target);
	}
	
	/**
	 * Method that return true if the given cube solid is. False otherwise.
	 * @param x x-component of the cube being analyzed
	 * @param y y-component of the cube being analyzed
	 * @param z z-component of the cube being analyzed
	 * @return  |if this.world.isSolid(x,y,z)==true
	 * 		    | then return true
	 * 			|else
	 * 			| return false	
	 */
	public boolean isPassable(int x, int y, int z){
		if(this.world.isSolid(x, y, z)){
			return false;
		}
		return true;
	}

	/**
	 * Method that return if a given cube has solid neighbor. 
	 * @param x the x-coordinate of the cube being analyzed.
	 * @param y the y-coordinate of the cube being analyzed.
	 * @param z the z-coordinate of the cube being analyzed.
	 * @return  if the coordinates are out of the world boundaries then it return false,
	 * 			otherwise if at least one solid cube exist in the neighbor of the cube return true,
	 * 			otherwise return false
	 * 			|if x+i > this.world.getNbCubesX() or y+j> this.world.getNbCubesY() or z+k>this.world.getNbCubesZ()
	 * 			| then return false
	 * 			|if this.world.isSolid(x+i,y+j,z+k)
	 * 			| then return true
	 * 			|else
	 * 			| return false
	 */	
	public boolean hasSolidNeighbor(int x, int y, int z){
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				for (int k=-1;k<2;k++){
					if (x+i>this.world.getNbCubesX() || y+j> this.world.getNbCubesY() || z+k>this.world.getNbCubesZ()){
						return false;
					}
					if(this.world.isSolid(x+i, y+j, z+k)){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Return whether or not a unit is falling.
	 * @return true if this.isFalling==true;
	 * 			false otherwise
	 */
	public boolean isFalling(){
		return this.isFalling;
	}
	
	/**
	 * Variable that store if a unit is falling or not
	 */
	private boolean isFalling;
	
	/**
	 * Check if a unit must fall or not and initiate the falling if the unit must fall.
	 * @pre zFallen >= 0
	 * @post if a unit has no solid neighbor then it must fall until he reaches a solid ground.
	 * 		|if !(this.hasSolidNeighbor(this.getCubeCoordinate()[0], this.getCubeCoordinate()[1], this.getCubeCoordinate()[2]+1) and this.isFalling==false
	 * 		| then this.isFalling==true
	 * 		| then this.moveToAdjacent(0,0,-1)
	 * 		|else if !(this.hasSolidNeighbor(this.getCubeCoordinate()[0], this.getCubeCoordinate()[1], this.getCubeCoordinate()[2]+1) and this.isFalling==true
	 * 		| then this.moveToAAdjacent(0,0,-1)
	 * @post if the unit reaches a solid ground then it stop falling and lose 10HP for each z-level he fell.
	 * 		|if this.world.isSolid(this.getCubeCoordinate()[0], this.getCubeCoordinate()[1], this.getCubeCoordinate()[2])&& this.isFalling==true
	 * 		| then this.isFalling==false
	 * 		| then new.getCurrentHitPoints() == old.getCurrentHitPoints() - this.zFallen * 10
	 * 		| then new.zFallen == 0
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException
	 */
	public void falling() throws IllegalArgumentExceptions, IllegalPositionException{
		if(!(this.hasSolidNeighbor(this.getCubeCoordinate()[0], this.getCubeCoordinate()[1], this.getCubeCoordinate()[2]+1))
				&& !this.isFalling()){
				this.isFalling=true;
				this.moveToAdjacent(0,0,-1);
			}
		else if((!this.world.isSolid(this.getCubeCoordinate()[0], this.getCubeCoordinate()[1], this.getCubeCoordinate()[2])
				&& this.isFalling())){
			this.moveToAdjacent(0,0,-1);	
		}
		else if((this.world.isSolid(this.getCubeCoordinate()[0], this.getCubeCoordinate()[1], this.getCubeCoordinate()[2])
				&& this.isFalling())){
			this.isFalling=false;
			this.setCurrentHitPoints((int) (this.getCurrentHitPoints()-10*this.zFallen));
			this.zFallen=0;
		}
		
	}
	
	private int zFallen=0;
	/**
	 * returns whether or not a unit is sprinting
	 * 
	 * @return
	 */
	@Basic
	@Raw
	public boolean isSprinting() {
		return this.isSprinting;
	}

	/**
	 * toggle the IsSprinting true
	 * 
	 * @throws IllegalArgumentExceptions
	 *             throws exception if unit is already sprinting or if the unit
	 *             is not moving
	 * @post if the unit is moving and his stamina points are high enough, the
	 *       unit will start to sprint |if (this.isMoving and
	 *       this.getCurrentStaminaPoints() !=0) |then new.isSprinting()== true
	 */
	public void startSprinting() throws IllegalArgumentExceptions {
		if (!this.isMoving() || this.getCurrentStaminaPoints() == 0)
			throw new IllegalArgumentExceptions();
		else
			this.isSprinting = true;
	}

	/**
	 * toggle the IsSprinting false
	 * 
	 * @throws IllegalArgumentExceptions
	 *             throws exception if unit isn't sprinting
	 * @post the unit stops sprinting |new.isSprinting() == false
	 */
	public void stopSprinting() {
		this.isSprinting = false;
	}

	/**
	 * variable storing whether or not a unit is sprinting.
	 */
	private boolean isSprinting = false;

	/* Activities */
	/**
	 * Drop the boulder or the log that are being carried.
	 * @throws IllegalPositionException 
	 * @throws IllegalWorldException 
	 * @post new.getWeight()== old.getWeight() - boulder.getWeight();
	 * @post new.getWeight()== old.getWeight() - log.getWeight();
	 */
	private void drop() throws IllegalPositionException, IllegalWorldException{
		this.weightCarrying = 0;
		double[] pos = null;
		if (this.workAtCube == null){
			 pos = this.getPosition();
		}else
			 pos = this.workAtCube.getPosition();
		if (this.isCarryingBoulder()){
			this.boulder.Drop(pos);
			this.boulder = null;
			this.isCarryingBoulder = false;
		}
		else if (this.isCarryingLog()){
			this.log.Drop(pos);
			this.isCarryingLog = false;
			this.log = null;
		}
		
	}
	
	/**
	 * A method to check whether or not a cube is workable by the unit.
	 * @param x
	 * 			x-coordinate to check
	 * @param y
	 * 			y-coordinate to check
	 * @param z
	 * 			z-coordinate to check
	 * @return
	 * 			a boolean 
	 */
	@Raw @Basic
	private boolean isWorkable(int x,int y,int z){
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				for (int k=-1;k<2;k++){
					if ((this.position.getRoundPosition()[0]==(x+i))&& (this.position.getRoundPosition()[1]==(y+j))&&(this.position.getRoundPosition()[2]==(z+k)))
						return true;		
				}
			}
		}
		return false;		
	}
	
	
	
	
	
	/**
	 * Make the unit work at the given location.
	 * @param x
	 * 			x-coordinate to work at
	 * @param y
	 * 			y-coordinate to work at
	 * @param z
	 * 			z-coordinate to work at
	 * 
	 * @throws IllegalPositionException
	 * 			throw new IllegalPositionException if the given location is not in the world boundaries.
	 * @throws IllegalArgumentException
	 * 			throw new IllegalArgumentException if the given location is not a neighboring cube
	 * @post the orientation is chanched, the unit is looking at the place he is working on (or default if he is working on the cube he is standing on.
	 * 			|new.getOrientation() == Math.atan2(y - this.position.getRoundPosition()[1],x - this.position.getRoundPosition()[0])
	 * 			| if (this.position == position to work at)
	 * 			| new.getOrientation() == this.DEFAULT_ORIENTATION
	 * @post the location to work at is updated to the given location
	 * 			|new.workAtCube.getRoundPosition() == {x,y,z}
	 * @effect the work order has changed (full description see this.setWorkOrder())
	 * 
	 * @effect the unit will start working
	 */
	public void workAt(int x, int y, int z) throws IllegalArgumentException {
		if (!this.isWorkable(x, y, z)){
			throw new IllegalArgumentException("The cube where I must work is not directly adjacent");
		}
		try {
			this.workAtCube = new Position(new int[] {x,y,z},this.getWorld().getWorldBoudaries());
			if (x==this.position.getRoundPosition()[0]&& y ==this.position.getRoundPosition()[1])
				this.setOrientation(DEFAULT_ORIENTATION);
			else
				this.setOrientation((float) Math.atan2(y - this.position.getRoundPosition()[1],x - this.position.getRoundPosition()[0]));
			this.setWorkOrder(x,y,z);	
			this.startWorking();
			} catch (IllegalPositionException e) {
			}
		
	}
	
	/**
	 * Sets the work order of the unit according to the conditions of the cube.
	 * @param x
	 * 			x-coordinate of the cube
	 * @param y
	 * 			x-coordinate of the cube
	 * @param z
	 * 			x-coordinate of the cube
	 * @post the work order is chanced according to the conditions of the cube
	 * 			|if (this.isCarryingBoulder() || this.isCarryingLog())
	 * 			|	new.workOrder ==  WorkOrder.DROP;
	 * 			|else if(this.getWorld().getCubeType(x, y, z)==3 && this.world.getBoulderCube(x, y, z)!= null && this.world.getLogCube(x, y, z)!= null )
	 * 			|	new.workOrder ==  WorkOrder.WORK_AT_WORKSHOP;
	 * 			|else if (this.getWorld().getBoulderCube(x, y, z)!= null && !(this.isCarryingBoulder() || this.isCarryingLog()))
	 * 			|	new.workOrder =  WorkOrder.PICK_UP_BOULDER;
	 * 			|else if (this.getWorld().getLogCube(x, y, z)!= null && !(this.isCarryingBoulder() || this.isCarryingLog()))
	 * 			|	new.workOrder == WorkOrder.PICK_UP_LOG;
	 * 			|else if(this.getWorld().getCubeType(x, y, z)==2)
	 * 			|	new.workOrder ==  WorkOrder.WORK_WOOD;
	 * 			|else if(this.getWorld().getCubeType(x, y, z)==1)
	 * 			|	new.workOrder ==  WorkOrder.WORK_ROCK;
	 * 			|else
	 * 			|	new.workOrder ==  WorkOrder.DO_NOTHING;
	 */
	private void setWorkOrder(int x, int y, int z){
		if (this.isCarryingBoulder() || this.isCarryingLog())
			this.workOrder =  WorkOrder.DROP;
		else if(this.getWorld().getCubeType(x, y, z)==3 && this.world.getBoulderCube(x, y, z)!= null && this.world.getLogCube(x, y, z)!= null )
			this.workOrder =  WorkOrder.WORK_AT_WORKSHOP;
		else if (this.getWorld().getBoulderCube(x, y, z)!= null && !(this.isCarryingBoulder() || this.isCarryingLog()))
			this.workOrder =  WorkOrder.PICK_UP_BOULDER;
		else if (this.getWorld().getLogCube(x, y, z)!= null && !(this.isCarryingBoulder() || this.isCarryingLog()))
			this.workOrder =  WorkOrder.PICK_UP_LOG;
		else if(this.getWorld().getCubeType(x, y, z)==2)
			this.workOrder =  WorkOrder.WORK_WOOD;
		else if(this.getWorld().getCubeType(x, y, z)==1)
			this.workOrder =  WorkOrder.WORK_ROCK;
		else	
			this.workOrder =  WorkOrder.DO_NOTHING;
	}

	/**
	 * Variable storing the work order
	 */
	private WorkOrder workOrder = WorkOrder.DO_NOTHING;
	
	/**
	 * Returns whether or not a unit is working.
	 * 
	 * @return this.isWorking
	 */
	@Basic
	public boolean isWorking() {
		return this.isWorking;
	}

	/**
	 * The Unit starts working
	 * 
	 * @post Toggle is working boolean true
	 * 			|new.isWorking == true;
	 * @post Sets timer for the work
	 * 			|new.workCounter == (float) 500 / this.getStrength();
	 */
	public void startWorking() {
		if (!this.isResting() && !this.isMoving()) {
			this.isWorking = true;
			this.workCounter = (float) 500 / this.getStrength();
		}
	}

	/**
	 * The Unit is interrupted 
	 * @post Toggle is working boolean false
	 * 			|new.isWorking == false;
	 * @post Ends timer for the work
	 * 			|new.workCounter == 0;
	 */
	private void stopWorking() {
		this.isWorking = false;
		this.workCounter = 0;
		}
	
	/**
	 * Finish work 
	 * the unit completed the task.
	 * According to the work order adjust the world
	 * @post if case==drop then the unit drop the object he is carrying. 
	 * 		|this.dropOrder()
	 * @post if case==work_at_workshop then the unit drop the object he is carrying. 
	 * 		|this.workAtWorkshop()
	 * @post if case==pick_up_boulder then the unit drop the object he is carrying. 
	 * 		|this.pick_up_boulder()
	 * @post if case==pick_up_log then the unit drop the object he is carrying. 
	 * 		|this.pick_up_log()
	 * @post if case==work_wood then the unit drop the object he is carrying. 
	 * 		|this.collapseWood()
	 * @post if case==work_rock then the unit drop the object he is carrying. 
	 * 		|this.collapseRock()
	 */
	private void finishWorking() {
		switch(this.workOrder){
		case DROP: 			
			try {
				this.dropOrder();
			} catch (IllegalPositionException | IllegalWorldException e) {}
			break;
		case WORK_AT_WORKSHOP: 	
			this.workAtWorkshop();
			break;
		case PICK_UP_BOULDER:
			try {
				this.pickUpBoulder();
			} catch (IllegalActionException e1) {}
				break;
		case PICK_UP_LOG: try {
				this.pickUpLog();
			} catch (IllegalActionException e) {}
				break;
		case WORK_WOOD:
			this.collapseWood();
			this.getWorld().getListCubePassable().add(new int[] {this.workAtCube.getRoundPosition()[0],
					this.workAtCube.getRoundPosition()[1],
					this.workAtCube.getRoundPosition()[2]-1});
			break;
		case WORK_ROCK:
			this.collapseRock();
			this.getWorld().getListCubePassable().add(new int[] {this.workAtCube.getRoundPosition()[0],
					this.workAtCube.getRoundPosition()[1],
					this.workAtCube.getRoundPosition()[2]-1});
			break;
		default:
			break;
		}
		this.stopWorking();
		}
	
	/**
	 * let a rock collapse
	 * @post add 10 points to the current experience
	 * 			|new.getExperience() == this.getExperience()+10
	 */
	private void collapseRock() {
		this.getWorld().collapse(this.workAtCube.getRoundPosition()[0], this.workAtCube.getRoundPosition()[1],this.workAtCube.getRoundPosition()[2], 1);
		this.setExperience(this.getExperience()+10);
	}
	/**
	 * let a tree collapse
	 * @post add 10 points to the current experience
	 * 			|new.getExperience() == this.getExperience()+10
	 */
	private void collapseWood() {
		this.getWorld().collapse(this.workAtCube.getRoundPosition()[0], this.workAtCube.getRoundPosition()[1],this.workAtCube.getRoundPosition()[2], 2);
		this.setExperience(this.getExperience()+10);
	}
	/**
	 * pick up a log
	 * @post this.log is the log at the position where the work took place.
	 * 			|new.log == this.getWorld().getLogCube(this.workAtCube[0], this.workAtCube[1],this.workAtCube[2]);
	 * @post the weight of the log is the new weight carrying
	 * 			|new.weightCarrying == this.log.getWeight();
	 * @post toggle this.isCarryingLog false
	 * 			|new.isCarryingLog == false
	 * @post add 10 points to the current experience
	 * 			|new.getExperience() == this.getExperience()+10
	 * @throws IllegalActionException
	 */
	private void pickUpLog() throws IllegalActionException {
		this.log = this.getWorld().getLogCube(this.workAtCube.getRoundPosition()[0], 
				this.workAtCube.getRoundPosition()[1],
				this.workAtCube.getRoundPosition()[2]);
		this.log.pickUp();
		this.weightCarrying = this.log.getWeight();
		this.isCarryingLog = true;
		this.setExperience(this.getExperience()+10);
	}
	/**
	 * pick up a boulder
	 * @post this.boulder is the log at the position where the work took place.
	 * 			|new.boulder = this.getWorld().getBoulderCube(this.workAtCube[0], this.workAtCube[1],this.workAtCube[2]);
	 * @post the weight of the boulder is the new weight carrying
	 * 			|new.weightCarrying == this.boulder.getWeight();
	 * @post toggle this.isCarryingBoulder false
	 * 			|new.isCarryingBoulder == false 
	 * @post add 10 points to the experience
	 * 			|new.getExperience() == this.getExperience()+10
	 * @throws IllegalActionException
	 */
	private void pickUpBoulder() throws IllegalActionException {
		this.boulder = this.getWorld().getBoulderCube(this.workAtCube.getRoundPosition()[0],
				this.workAtCube.getRoundPosition()[1],
				this.workAtCube.getRoundPosition()[2]);
		this.boulder.pickUp();
		this.weightCarrying = this.boulder.getWeight();
		this.isCarryingBoulder = true;
		this.setExperience(this.getExperience()+10);
	}
	
	/**
	 * Work at Workshop and increase the unit's stuff.
	 * @post increase weight
	 * 			| new.getWeight()== this.getWeight()+1
	 * @post increase toughness
	 * 			| new.getToughness() == old.getToughness()+1
	 * @post add 10 points to the experience
	 * 			|new.getExperience() == old.getExperience()+10
	 */
	private void workAtWorkshop() {
		this.getWorld().getBoulderCube(this.workAtCube.getRoundPosition()[0], 
				this.workAtCube.getRoundPosition()[1],
				this.workAtCube.getRoundPosition()[2]).consume();
		this.getWorld().getLogCube(this.workAtCube.getRoundPosition()[0], 
				this.workAtCube.getRoundPosition()[1],
				this.workAtCube.getRoundPosition()[2]).consume();
		this.setWeight(this.getWeight()+1);
		this.setToughness(this.getToughness()+1);
		this.setExperience(this.getExperience()+10);
	}
	
	/**
	 * Drop the boulder or the log that is being carried.
	 * @throws IllegalPositionException 
	 * @throws IllegalWorldException 
	 * @post  sets the weightCarrying to 0;
	 * 			|new.weightCarrying== 0;
	 * @post change the state of the boulder or the log to null and toggle isCarryingBoulder or isCarryingLog to false.
	 * 			|if(this.isCarryingBoulder())
	 * 			|	this.boulder == null;
	 * 			|	this.isCarryingBoulder == false 
	 * 			|else if(this.isCarryinglog())
	 * 			|	this.log == null;
	 * 			|	this.isCarryinglog == false 
	 */
	private void dropOrder() throws IllegalPositionException, IllegalWorldException{
		if (this.getWorld().isSolid(this.workAtCube.getRoundPosition()[0], this.workAtCube.getRoundPosition()[1], this.workAtCube.getRoundPosition()[2]))
			this.workAtCube = this.position;
		this.weightCarrying = 0;
		if (this.isCarryingBoulder()){
			this.boulder.Drop(this.workAtCube.getPosition());
			this.boulder = null;
			this.isCarryingBoulder = false;
		}
		else if (this.isCarryingLog()){
			this.log.Drop(this.workAtCube.getPosition());
			this.isCarryingLog = false;
			this.log = null;
		}
		
	}
	/**
	 * boulder the unit is carrying
	 */
	private Boulder boulder;
	/**
	 * log the unit is carrying
	 */
	private Log log;
	/**
	 * the extra weight the unit is carrying
	 */
	private int weightCarrying=0;
	
	/**
	 * Variable storing whether or not a unit is working.
	 */
	private boolean isWorking = false;
	
	/**
	 * stores the position the unit has to work at
	 */
	private Position workAtCube;
	
	/**
	 * storing the unit has to work when arrived
	 */
	private boolean hasToWork = false;

	/**
	 * Returns whether or not a unit is resting.
	 * 
	 * @return true if isResting==true false otherwise
	 */
	@Basic
	public boolean isResting() {
		return this.isResting;
	}

	/**
	 * Sets the variable isResting to true.
	 */
	public void startResting() {
		this.stopWorking();
		this.isResting = true;
		this.stopWorking();
	}

	/**
	 * Set the variable isResting to false.
	 */
	private void stopResting() {
		this.isResting = false;
	}

	/**
	 * Variable storing whether or not a unit is resting.
	 */
	private boolean isResting = false;

	/* Default behavior */
	
	/**
	 * Method that return defaultBehavior.
	 * 
	 * @return true if defaultBehavior==true false otherwise
	 */
	public boolean isDefaultBehaviourEnabled() {
		return this.defaultBehaviour;
	}

	/**
	 * Method that set defaultBehavior to the given boolean value.
	 * 
	 * @param defaultBehaviour
	 */
	public void setDefaultBehaviour(boolean defaultBehaviour) {
		this.defaultBehaviour = defaultBehaviour;
	}

	/**
	 * This method start the default behavior of the unit.
	 * 
	 * @post if this.isMoving == true or this.isMovingTo == true and this.odd <=
	 *       0.5 this.startSprinting
	 * @post if not moving and random number == 0 then the unit move to a random position.
	 * 		|if (this.isMoving == false or this.isMovingTo == false) and this.random(4) == 0 
	 * 		| then this.moveTo(random.dx, random.dy, random.dz)
	 * @post if not moving and random number == 1 then the unit begin to work.
	 * 		|if (this.isMoving == false or this.isMovingTo == false) and this.random(4) == 1 
	 * 		| then this.startWorking
	 * @post if not moving and random number == 2 then the unit begin to rest.
	 * 		|if (this.isMoving == false or this.isMovingTo == false) and this.random(4) == 2 
	 * 		| then this.startResting.
	 * @post if not moving and random number == 3 then the unit begin to attack a unit that belong to another faction en that is in range
	 * 		|if (this.isMoving == false or this.isMovingTo == false) and this.random(4) == 3 and this.hasValidDefender()
	 * 		| then this.startAttacking(defender)
	 * @post this.defaultBehavior==true
	 * 
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentException 
	 */
	public void startDefaultBehavior() throws IllegalArgumentExceptions, IllegalArgumentException, IllegalPositionException {
		this.setDefaultBehaviour(false);
		Task testTask = this.getFaction().getScheduler().getTask();
		if (testTask!=null){
			testTask.assignTo(this);
			this.setTask(testTask);
		}else{
			if (this.isMoving || this.isMovingTo) {
				if (Math.random() <= 0.5) {
					this.startSprinting();
				}	
			}
			else {
				int randomActivityIndex = rand.nextInt(4);
				randomActivityIndex = 0;
				if (randomActivityIndex == 0) {
					this.isMovingTo = true;
				} else if (randomActivityIndex == 1) {
					this.workAt((int)this.getPosition()[0], (int)this.getPosition()[1], (int)this.getPosition()[2]-1);
				} else if (randomActivityIndex == 2) {
					this.startResting();
				}
				else if (randomActivityIndex == 3){
					if (this.hasValidDefender()){
						this.isAttacking=true;
					}
				}
				if (this.isMovingTo) {
					int dx = rand.nextInt(this.getWorld().getNbCubesX());
					int dy = rand.nextInt(this.getWorld().getNbCubesY());
					int dz = rand.nextInt(this.getWorld().getNbCubesZ());
					while(!isPassable(dx, dy, dz+1) || !this.hasSolidNeighbor(dx, dy, dz+1)){
						dx = rand.nextInt(this.getWorld().getNbCubesX());
						dy = rand.nextInt(this.getWorld().getNbCubesY());
						dz = rand.nextInt(this.getWorld().getNbCubesZ());
					}
					this.moveTo(new int[] { dx, dy, dz });
				}
				else if (this.isWorking) {
					int dx = -1;
					int dy = -1;
					while(this.getCubeCoordinate()[0]+dx<this.getWorld().getNbCubesX() && 
							this.getCubeCoordinate()[0]+dx>=0 &&
							this.getCubeCoordinate()[1]+dy<this.getWorld().getNbCubesY() &&
							this.getCubeCoordinate()[1]+dy>=0 &&
							this.getWorld().getCubeType(this.getCubeCoordinate()[0]+dx, this.getCubeCoordinate()[1]+dy, this.getCubeCoordinate()[2]+1)!=1){
						dx = rand.nextInt(2)-1;
						dy = rand.nextInt(2)-1;
					}
					this.workAt(this.getCubeCoordinate()[0]+dx, this.getCubeCoordinate()[1]+dy, this.getCubeCoordinate()[2]+1);
				}}}
	}


	/**
	 * Variable storing the boolean value of defaultBehavior.
	 */
	private boolean defaultBehaviour = false;

	/* Fighting */
	/**
	 * Method that return whether or not a unit is attacking.
	 * 
	 * @return true if isAttacking == true false otherwise
	 */
	@Basic
	public boolean isAttacking() {
		return this.isAttacking;
	}

	/**
	 * Variable storing the boolean value of isAttacking.
	 */
	private boolean isAttacking = false;

	/**
	 * Variable storing the Unit value of the defender.
	 */
	private Unit defender;

	/**
	 * Make the given unit fight with another unit.
	 * 
	 * @param defender
	 *            The unit that gets attacked and should defend itself
	 * @pre defender must be a valid Unit
	 * @post this.isMoving == false
	 * @post this.isWorking == false
	 * @post this.isResting == false
	 * @post this.isSprinting == false
	 * @post this.isAttacking = true;
	 * @post this.canAttack = false;
	 * @post this.isMoving = false;
	 * @post this.getOrientation == atan(defender.getPosition()[1] -
	 *       this.getPosition()[1], defender.getPosition()[0] -
	 *       this.getPosition()[0]))
	 * @post defender.getOrientation == atan( this.getPosition()[1] -
	 *       defender.getPosition()[1], this.getPosition()[0] -
	 *       defender.getPosition()[0])
	 * @post this.attackCounter == 1
	 * @throws IllegalArgumentExceptions
	 *             the argument defender must be a valid Unit.
	 * @throws IllegalPositionException 
	 */
	public void startAttacking(Unit defender) throws IllegalArgumentExceptions, IllegalPositionException {
		this.stopResting();
		this.stopSprinting();
		this.stopWorking();
		this.isMovingTo = false;
		
		if (!this.isValidDefender(defender))
			throw new IllegalArgumentExceptions("Not a valid Defender");
			this.defender = defender;
			this.isAttacking = true;
			this.canAttack = false;
			this.isMoving = false;
			this.setOrientation((float) Math.atan2(defender.getPosition()[1] - this.getPosition()[1],
					defender.getPosition()[0] - this.getPosition()[0]));
			defender.setOrientation((float) Math.atan2(this.getPosition()[1] - defender.getPosition()[1],
					this.getPosition()[0] - defender.getPosition()[0]));
			defender.startDefending(this);
			this.stopResting();
			this.stopSprinting();
			this.stopWorking();
			this.attackCounter = 1;
	}

	/**
	 * Method that determine if a given unit can be attacked by this.
	 * @pre defender is a valid Unit.
	 * @param defender
	 *            defender is a Unit variable.
	 * @return true if a defender is in the range of the attacker and have a different faction of the attacker, false otherwise.
	 * 			|if defender == this
	 * 			| then return false
	 * 			|if defender.getFaction() == this.getFaction
	 * 			| then return false
	 * 			|if (for (i in [0..2]) defender.getPosition()[i] in[this.getPosition()[i]-1..this.getPosition()[i]+1]) 
	 * 			| then return true
	 * 			|else
	 * 			| return false
	 */
	private boolean isValidDefender(Unit defender) {
		if (defender == this){
			return false;}
		if(defender==null){
			return false;
		}
		else if (defender.getFaction()==this.getFaction()){
			return false;
		} 
		for (int i = 0; i < 3; i++) {
			if (!(defender.position.getPosition()[i] >= (this.position.getPosition()[i]) - 1)
					&& (defender.position.getPosition()[i] <= (this.position.getPosition()[i] + 1))) {
				return false;
			}
		}
		return true;
	}

	
	/**
	 * Method that give if a unit has valid defender around him
	 * @invariant i & j are smaller than 2 and increment monotonous way until they found a unit that fulfill the condition isValidDefender.
	 * @post if there is a valid defender around the unit then it return true and the variable defender store the found unit.
	 * 		|if isValidDefender(unit.around)
	 * 		| then this.unit==this.defender
	 * 		| return true
	 * 		|else return false.
	 * 	
	 */
	private boolean hasValidDefender(){
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				if (this.getCubeCoordinate()[0]+i < this.getWorld().getNbCubesX() && 
						this.getCubeCoordinate()[0]+i >= 0 && 
						this.getCubeCoordinate()[1]+j < this.getWorld().getNbCubesY() && 
						this.getCubeCoordinate()[1]+j >= 0 && 
						this.isValidDefender( this.getWorld().getUnitCube(this.getCubeCoordinate()[0]+i,
						this.getCubeCoordinate()[1]+j,
						this.getCubeCoordinate()[2]+1))){
					this.defender=this.getWorld().getUnitCube(this.getCubeCoordinate()[0],
							this.getCubeCoordinate()[1],
							this.getCubeCoordinate()[2]+1);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Method to make the unit stop fighting.
	 * 
	 * @post this.isAttacking==false
	 * @post new.attackCounter==0
	 * @post this.canAttack == true
	 * @throws IllegalArgumentExceptions
	 *             throw an exception if the unit is not attacking.
	 */

	private void stopAttacking() throws IllegalArgumentExceptions {
		if (!this.isAttacking())
			throw new IllegalArgumentExceptions("The unit has already stopped attacking");
		else {
			this.canAttack = true;
			this.isAttacking = false;
			this.attackCounter = 0;
		}
	}

	/**
	 * Variable storing whether or not a unit is able to attack.
	 */
	private boolean canAttack = true;
	/* Defending */

	/**
	 * Method that return whether or not the defender dodge the attacker's
	 * attack
	 * 
	 * @param attacker
	 *            A Unit variable
	 * @return true if (random.number in [0..this.probabilityDodging]) false
	 *         otherwise.
	 */
	public boolean dodge(Unit attacker) {
		this.probabilityDodging = (0.2 * (this.getAgility() / attacker.getAgility()));
		if (Math.random() <= this.probabilityDodging)
			return true;
		return false;
	}

	/**
	 * Variable storing the probability of dodging a unit's attack.
	 */
	private double probabilityDodging;

	/**
	 * Variable storing the probability of blocking a unit's attack.
	 */
	private double probabilityBlocking;

	/**
	 * Variable storing the new position of the defender after evading an
	 * attack.
	 */
	private Position dodgePos;

	/**
	 * Method that return whether or not the defender block the attacker's
	 * attack.
	 * 
	 * @param attacker
	 *            A Unit variable.
	 * @return true if (random.number in [0..this.probabilityBlocking]) false
	 *         otherwise.
	 */
	public boolean block(Unit attacker) {
		this.probabilityBlocking = (0.25
				* ((this.getStrength() + this.getAgility()) / (attacker.getStrength() + attacker.getAgility())));
		if (Math.random() <= this.probabilityBlocking)
			return true;
		return false;
	}

	/**
	 * Method that start the defense process of the unit.
	 * 
	 * @param attacker
	 *            A Unit Variable that execute the attack.
	 * @post this.isMoving == false
	 * @post this.isWorking == false
	 * @post this.isResting == false
	 * @post this.isSprinting == false
	 * @post this.isDefending = true;
	 * @post this.isMoving = false;
	 * @post if the unit dodge the attack then the unit must move to a passable terrain in the range (X_c+-1, Y_c+-1, Z_c)
	 * 		|if (((dx != 0) || (dy != 0)) &&
			|			defender.position()[0] + dx < this.getWorld().getNbCubesX() &&
			|			defender.position()[1] + dy <this.getWorld().getNbCubesY() &&
			|			defender.position()[0] + dx >= 0 &&
			|			defender.position()[1] + dy >= 0)
			| then new.defender.position()== {defender.position()[0]+dx,defender.position()[1]+dy,defender.position()[2]}
	 * @post defender lose attacker.getStrength() / 10 hitpoints or gain 20 experiences.
	 *		 |if ((this.dodge(attacker) == true) and ((RandomInt([-1,0,1]).dx) or
	 *       |(RandomInt([-1,0,1]).dy != 0))
	 *       |new.getPosition() == [
	 *       (this.getPosition()[0] + (RandomInt([-1,0,1]).dx)),
	 *       (this.getPosition()[1] + (RandomInt([-1,0,1]).dy)),
	 *       (this.getPosition()[2] ]
	 * @post if (!this.block(attacker)) new.getHitPoints == old.getHitPoints -
	 *       attacker.getStrength() / 10
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException 
	 */
	public void startDefending(Unit attacker) throws IllegalArgumentExceptions, IllegalPositionException {
		this.stopResting();
		this.stopSprinting();
		this.stopWorking();
		this.isMovingTo = false;
		this.isDefending = true;
		if (this.dodge(attacker)) {
			this.setExperience(this.getExperience()+20);
			double dx = 0;
			double dy = 0;
			while (!this.isValidDodgeLocation) {
				dx = 2*rand.nextDouble()-1;
				dy = 2*rand.nextDouble()-1;
				if (((dx != 0) || (dy != 0)) &&
						this.position.getRoundPosition()[0] + dx <this.getWorld().getNbCubesX() &&
						this.position.getRoundPosition()[1] + dy <this.getWorld().getNbCubesY() &&
						this.position.getRoundPosition()[0] + dx >= 0 &&
						this.position.getRoundPosition()[1] + dy >= 0) {
						this.dodgePos = new Position(new double[] { this.position.getRoundPosition()[0] + dx,
								this.position.getRoundPosition()[1] + dy,
								this.position.getRoundPosition()[2]},this.world.getWorldBoudaries());
						this.isValidDodgeLocation = true;
						this.position = new Position(new double[] { (this.dodgePos.getRoundPosition()[0] + 0.5),
								(this.dodgePos.getRoundPosition()[1] + 0.5),
								(this.dodgePos.getRoundPosition()[2] + 0.5) },this.world.getWorldBoudaries());

				}
			}
		}

		else if (!this.block(attacker)) {
			this.setCurrentHitPoints(this.getCurrentHitPoints()-(int) attacker.getStrength() / 10);
			attacker.setExperience(attacker.getExperience()+20);
		}
		else{
			this.setExperience(this.getExperience()+20);
		}
	}

	/**
	 * A random variable to calculate the different probabilities.
	 */
	private Random rand = new Random();

	/**
	 * A method that return whether or not a unit is defending.
	 * 
	 * @return true if this.isDefending == true false otherwise
	 */
	@Basic
	public boolean isDefending() {
		return this.isDefending;
	}

	/**
	 * A variable storing the boolean value of isDefending.
	 */
	private boolean isDefending = false;

	/**
	 * A variable storing the boolean value of the dodge location.
	 */
	private boolean isValidDodgeLocation = false;

	/* Updates */
	/**
	 * Updates the default speed according to the situation.
	 * 
	 * @pre this.isMoving == true
	 * @post if the unit is climbing up the default speed is 0,5*this.defaultSpeed.
	 * 		|if (this.position.getPosZ() - this.target.getPosZ() <-Util.DEFAULT_EPSILON)
	 * 		| then new.defaultSpeed == 0.5 *this.defaultSpeed
	 * 
	 * @post if the unit is climbing down the default speed is 0,5*this.defaultSpeed.
	 * 		|if (this.position.getPosZ() - this.target.getPosZ() > Util.DEFAULT_EPSILON)
	 * 		| then new.defaultSpeed == 1,2 *this.defaultSpeed
	 * 
	 * @post if the unit is sprinting then the defaultSpeed is dubbeld
	 * 		|if (this.isSprinting())
	 *      | then new.defaultSpeed  == 2 * this.defaultSpeed
	 *      
	 * @post the unit is falling then the default speed is -3.
	 * 		|if this.isFalling == true
	 * 		| then new.defaultSpeed == -3
	 * 
	 * @post if the unit is not moving then the speed is 0
	 * 		|if !this.isMoving
	 * 		| then new.defaultSpeed == 0
	 * 
	 * @post if the unit is not climbing nor sprinting nor falling then the default speed is 1.5 / 2 * (this.getStrength() + this.getAgility()) / (this.getWeight());
	 * 		|else 
	 * 		| then new.defaultSpeed == 1.5 / 2 * (this.getStrength() + this.getAgility()) / (this.getWeight())
	 */

	private void updateDefaultSpeed() {
		if (this.position.getPosZ() - this.target.getPosZ() < -Util.DEFAULT_EPSILON) {
			this.defaultSpeed = 0.5 * 1.5 / 2 * (this.getStrength() + this.getAgility()) / (this.getWeight());
		} else if (this.position.getPosZ() - this.target.getPosZ() > Util.DEFAULT_EPSILON) {
			this.defaultSpeed = 1.2 * 1.5 / 2 * (this.getStrength() + this.getAgility()) / (this.getWeight());
		} else if (!this.isMoving) {
			this.defaultSpeed = 0;
		} else {
			this.defaultSpeed = 1.5 / 2 * (this.getStrength() + this.getAgility()) / (this.getWeight());
		}
		if (this.isSprinting())
			this.defaultSpeed = 2 * this.defaultSpeed;
		if (this.isFalling()==true){
			this.defaultSpeed= -3.0;
		}
		//Speed update in functie van carried log of boulder.
		
	}

	/**
	 * A Method to update the movement
	 * 
	 * @param dt
	 *            the time with which the game advance.
	 * @pre the time must be valid
	 * 		|isValidTime(dt)
	 * @post If the unit is arrived then the position is set to the center of the cube.
	 * 		 If the unit is still falling then zFallen+=1. If the unit hasn't got experience yet then she gain exp.
	 * 		 If the route is not empty we remove the first road of route. If the route is empty this.MovingTo==false
	 * 		|if this.isArrived()
	 * 		| then new.getPosition() == this.target
	 * 		| then this.isMoving = false
	 * 		| then new.getCurrentSpeed == 0
	 * 		|if !this.isMovingTo
	 * 		| then this.isSprinting==false
	 *		|if !hasGotExperience
	 *		| then new.getExperience() == old.getExperience()+1
	 *		|if this.isFalling == true
	 *		| then new.zFallen == old.zFallen + 1
	 *		|if this.route.isEmpty()
	 *		| then this.movingTo==false
	 *		|if !this.route.isEmpty()
	 *		| then this.route.remove(this.route.get(0))
	 *
	 * @post if the unit is move then he moves toward the target.
	 * 		|if this.isMoving == true 
	 * 		| then new.position = new Position(new double[] { (this.position.getPosX() + this.getCurrentSpeed()[0] * dt),
			|		(this.position.getPosY() + this.getCurrentSpeed()[1] * dt),
			|		(this.position.getPosZ() + this.getCurrentSpeed()[2] * dt)
	 * @post if the unit is falling then he move the same way as he doesn't fall.
	 * 		|if this.isFalling == true 
	 * 		| then new.position = new Position(new double[] { (this.position.getPosX() + this.getCurrentSpeed()[0] * dt),
			|		(this.position.getPosY() + this.getCurrentSpeed()[1] * dt),
			|		(this.position.getPosZ() + this.getCurrentSpeed()[2] * dt)
	 * @throws IllegalPositionException
	 */
	private void updateMovement(double dt) throws IllegalPositionException {
		if (this.isArrived()) {
			this.position= new Position(new double[] { (this.target.getRoundPosition()[0] + 0.5),
					(this.target.getRoundPosition()[1] + 0.5), (this.target.getRoundPosition()[2] + 0.5) },this.world.getWorldBoudaries());
			this.isMoving = false;
			if (this.isFalling()){
				this.zFallen+=1;
				}
			if (!this.isMovingTo)
				this.stopSprinting();
			if (!this.hasGotExperience()){
				this.setExperience(this.getExperience()+1);
				this.hasGotExperience=true;
				}
			if (!this.route.isEmpty()){
				this.route.remove(this.route.get(0));
			}
			if (this.route.isEmpty()){
				this.isMovingTo=false;
			}
		}
		else if (this.isFalling()){
			this.hasGotExperience=false;
			this.position= new Position(new double[] { (this.position.getPosX() + this.getCurrentSpeed()[0] * dt),
					(this.position.getPosY() + this.getCurrentSpeed()[1] * dt),
					(this.position.getPosZ() + this.getCurrentSpeed()[2] * dt) },this.world.getWorldBoudaries());
		}
		else if (this.isMoving) {
			this.hasGotExperience=false;
			this.position= new Position(new double[] { (this.position.getPosX() + this.getCurrentSpeed()[0] * dt),
					(this.position.getPosY() + this.getCurrentSpeed()[1] * dt),
					(this.position.getPosZ() + this.getCurrentSpeed()[2] * dt) },this.world.getWorldBoudaries());
		}
	}

	
	/**
	 * Method that give if 2 cubes are neighbor.
	 * @param cube1 : the first cube being analyzed
	 * @param cube2 : the second cube being analyzed.
	 * @invariant i increase monotonically if cube1[0]+i != cube2[0]
	 * @invariant j increase monotonically if cube1[1]+j != cube2[1]
	 * @invariant k increase monotonically if cube1[2]+k != cube2[2]
	 * @return true if the 2 cubes are neighbor, false otherwise.
	 * 		|if cube1[0]+i==cube2[0] &&		
	 *		|				cube1[1]+j==cube2[1] &&
	 *		|				cube1[2]+k==cube2[2]
	 *		| then return true
	 *		|else
	 *		| return false
	 */
	public boolean areNeighbor(int[] cube1,int[] cube2){
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				for (int k=-1;k<2;k++){
					if (cube1[0]+i==cube2[0] &&
							cube1[1]+j==cube2[1] &&
							cube1[2]+k==cube2[2]){
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * A method that update the movement
	 * 
	 * @param dt
	 *            the time with which the game advance.
	 * @pre isValidTime(dt)
	 * @post a moveToAdjacent movement is initiated if the route is not empty, if the unit is doing a moveTo movement
	 *  and not a moveToAdjacent movement, if the unit is not resting, working, attacking or defending.
	 *  	|if (!this.route.isEmpty() && this.isMovingTo && !this.isMoving && !this.isResting() && !this.isWorking() && !this.isAttacking() 
	 *  	|	&& !this.isDefending())
	 *  	| then indexCurrentStart = this.route.get(0)[0] & indexCurrentEnd = this.route.get(0)[1]
	 *  	| then this.moveToAdjacent((this.listCubePassable.get(indexCurrentEnd)[0] - this.listCubePassable.get(indexCurrentStart)[0]),
	 *					(this.listCubePassable.get(indexCurrentEnd)[1] - this.listCubePassable.get(indexCurrentStart)[1]),
	 *					(this.listCubePassable.get(indexCurrentEnd)[2] - this.listCubePassable.get(indexCurrentStart)[2]))
	 * @post /!\ NOT IMPLEMENTED 
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException 
	 */
	private void updateMovingTo(double dt) throws IllegalArgumentExceptions, IllegalPositionException {
		if (this.route.size()>1 && this.isMovingTo && !this.isMoving && !this.isResting() && !this.isWorking() && !this.isAttacking()
				&& !this.isDefending()) {
			this.moveToAdjacent(this.route.get(1)[0]-this.route.get(0)[0],
					this.route.get(1)[1]-this.route.get(0)[1],
					this.route.get(1)[2]-this.route.get(0)[2]);
		}
		// NOT IMPLEMENTED //
		if (this.hasToWork && this.position.equals(new Position(new double[] {this.workAtCube.getPosX(),this.workAtCube.getPosY(),this.workAtCube.getPosZ()},this.getWorld().getWorldBoudaries()))){
			this.startWorking();
			this.hasToWork= false;
		}
	}
	
	/*************************/
	/**DIJKSTRA MOVETO BEGIN**/
	/*************************/
	//TODO Commentary
	/**
	 * Algorithm of Dijkstra to find the shortest path between two cubes. 
	 * First we initialize the distance of all cube to the source to infinity and the previous node to null (==undefined).
	 * The distance from the source to the source is obviously 0. We choose the cube with the smallest distance to the source and that is not
	 * yet visited. We take all neighboring cube of the selected cube that are not already visited. For each neighbor we calculate the distance
	 * between the source cube and them passing trough the selected cube. If the found distance between the source and the neighboring
	 * cube is shorter than the previous distance than we keep this distance and we indicate in listPrev that to reach the neighboring cube,
	 * we must go through the selected cube. The neighboring cube is now visited.
	 * We repeat this operation until the end cube is reached.
	 * @param start
	 * 		The start cube
	 * @param end
	 * 		The end cube
	 * @post the index of the start cube is -1.
	 * @post Each visited cube has a post cube that finally lead to the source.
	 * @post The initialization of listCubePassable is done.
	 * @post u == indexEnd
	 * @invariant s | s has all cube that have been visited by the algo.
	 * @invariant q | q has always the size of listCubePassable.
	 * @return listPrev | A list that keep the previous cube with the shortest path to reach the source.
	 */
	private int[] Dijkstra(int[] start,int[] end){
		
		//Initialization of the listCubePassable.
		/*if (!this.getInitializationState()){
			initialisationCube();
		}*/
		List<int[]> q = new ArrayList<int[]>();
		List<int[]> s = new ArrayList<int[]>();
		int u = -1;
		List<Double> listDist = new ArrayList<Double>();
		
		int[] listPrev = new int[this.getWorld().getListCubePassable().size()];
		
		int indexStart = this.getIndex(start, this.getWorld().getListCubePassable());

		int indexEnd = this.getIndex(end, this.getWorld().getListCubePassable());
		
		for (int i=0;i<this.getWorld().getListCubePassable().size();i++){
			listDist.add(Double.POSITIVE_INFINITY);
			listPrev[i] = -1;
		}
		
		q.addAll(this.getWorld().getListCubePassable());
		listDist.set(indexStart, 0.0);
		
		while (s.size()!=q.size() && u!=indexEnd){
			u = getMinimumEdge(listDist, s);
			s.add(this.getWorld().getListCubePassable().get(u));
			List<int[]> listNeig = neighbour(this.getWorld().getListCubePassable().get(u));
			for (int i=0;i<listNeig.size();i++){
				if (getIndex(listNeig.get(i), s)==-1){
					//The index of the neighbor in the list of distance.
					int indexNeigInListDist = getIndex(listNeig.get(i), this.getWorld().getListCubePassable());
					double alt_route = listDist.get(u) + 
							Math.sqrt(Math.pow((this.getWorld().getListCubePassable().get(u)[0]-listNeig.get(i)[0]), 2)+
							Math.pow((this.getWorld().getListCubePassable().get(u)[1]-listNeig.get(i)[1]), 2)+
							Math.pow((this.getWorld().getListCubePassable().get(u)[2]-listNeig.get(i)[2]), 2));
					if (alt_route<listDist.get(indexNeigInListDist)){
						listDist.set(indexNeigInListDist, alt_route);
						listPrev[indexNeigInListDist]=u;
					}
				}
			}
		}
		return listPrev;
	}

	
	
	/**
	 * Get the the cube with the minimum distance to the source and that is not in in List<int[]> S (The visited's cube list)
	 * @param list | List<Double> the list containing the distance from a cube to the source.
	 * @param S | list of all already visited cube.
	 * @return u 	|u is the index of the cube that is not in S which has the smallest number in list.  
	 * 				|u == Collections.min(changingList) if getIndex(listCubePassable.get(u),S) != -1
	 */
	private int getMinimumEdge(List<Double> list, List<int[]> S){
		List<Double> changingList = list;
		int u= changingList.indexOf(Collections.min(changingList));
		while (getIndex(this.world.getListCubePassable().get(u),S) != -1){
			changingList.set(u, Double.POSITIVE_INFINITY);
			u=changingList.indexOf(Collections.min(changingList));
		}
		return u;
	}
	
	/**
	 * Method that return the shortest route between two cubes. It begin with the end of the route 
	 * @param start | the start cube.
	 * @param end | the end cube
	 * @return route | each cube where the unit must go before reaching the end.
	 */
	private List<int[]> recoverDijkstra(int[] start,int[] end){
		int[] listPrev = Dijkstra(start, end);
		List<int[]> route = new ArrayList<>();
		int indexEnd = getIndex(end,this.world.getListCubePassable());
		while (listPrev[indexEnd] != -1){
			route.add(0,this.world.getListCubePassable().get(indexEnd));
			indexEnd=listPrev[indexEnd];
		}
		route.add(0,this.world.getListCubePassable().get(indexEnd));
		return route;
	}
	
	/**
	 * Return the list of passable neighbor of the given cube.
	 * @param cube | The given cube, int[]
	 * @invariant i
	 * 			| i is in the range of 2.
	 * 			| if no value of i have been handled yet, listNeig is empty.
	 * 			| otherwise listNeig contains all cubes met coordinates {cube[0]+i,cube[1],cube[2]} that are in listCubePassable,
	 * 			| with i != 0.
	 * @invariant j
	 * 			| j is in the range of 2.
	 * 			| if no value of j have been handled yet, listNeig is empty.
	 * 			| otherwise listNeig contains all cubes met coordinates {cube[0]+i,cube[1]+j,cube[2]} that are in listCubePassable,
	 * 			| with i != 0 && j!=0.
	 *@invariant k
	 * 			| k is in the range of 2.
	 * 			| if no value of k have been handled yet, listNeig is empty.
	 * 			| otherwise listNeig contains all cubes met coordinates {cube[0]+i,cube[1]+j,cube[2]+k} that are in listCubePassable,
	 * 			| with i != 0 && j!=0 && k!=0.
	 * @return listNeig | Return a list with all possible neighboring cube of the given cube
	 * 
	 */
	public List<int[]> neighbour(int[] cube){
		List<int[]> listNeig = new ArrayList<int[]>();
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				for (int k=-1;k<2;k++){
					if (getIndex(new int[] {cube[0]+i,cube[1]+j,cube[2]+k}, this.world.getListCubePassable())!=-1 || (i==0 && j==0 && k==0)){
						listNeig.add(new int[] {cube[0]+i,cube[1]+j,cube[2]+k});
					}
				}
			}
		}
		return listNeig;
	}

	
	/**
	 * A list that store all passable cube of a world.
	 *//*
	private List<int[]> listCubePassable = new ArrayList<int[]>();
	
	*//**
	 * Initialization of listCubePasable whether true or not.
	 *//*
	
	private boolean initializationDone = false;
	
	*//**
	 * Set the initialization of listCubePassable to boolean value b;
	 * @param b | boolean value
	 *//*
	private void setInitialization(boolean b){
		this.initializationDone=b;
	}
	
	//TODO remove setInitialization inutil. Add new passable cube if working.
	
	private boolean getInitializationState(){
		return this.initializationDone;
	}
	
	*//**
	 * A method that modify listCubePassable by adding all passable cube of the world.
	 * @pre listCubePassable must exist and be a list
	 * 		|listCubePassable instanceOf List<E>
	 * @post listCubePassable has all the passable terrain
	 * @invariant i
	 * 			| i is in the range of the x-boundaries world.
	 * 			| if no value of i have been handled yet, listCubePassable is empty.
	 * 			| otherwise listCubePassable contains all cubes met coordinates {x,y,z} that are passable & that have solid neighbor.
	 * @invariant j
	 * 			| j is in the range of the y-boundaries world.
	 * 			| if no value of j have been handled yet, listCubePassable is empty.
	 * 			| otherwise listCubePassable contains all cubes met coordinates {i,y,z} that are passable & that have solid neighbor.
	 *@invariant k
	 * 			| k is in the range of the z-boundaries world.
	 * 			| if no value of k have been handled yet, listCubePassable is empty.
	 * 			| otherwise listCubePassable contains all cubes met coordinates {i,j,z} that are passable & that have solid neighbor.
	 *//*
	private void initialisationCube(){
		for (int i=0;i<this.world.getNbCubesX();i++){
			for (int j=0;j<this.world.getNbCubesY();j++){
				for (int k=0;k+1<this.world.getNbCubesZ();k++){
					if(this.isPassable(i, j, k+1) && hasSolidNeighbor(i, j, k+1) && this.getIndex(new int[] {i,j,k},this.listCubePassable)==-1){
						this.listCubePassable.add(new int[] {i,j,k});
					}
				}
			}
		}
		this.setInitialization(true);
	}
	*/
	/***********************/
	/**DIJKSTRA MOVETO END**/
	/***********************/
	
	/**
	 * Get the index of an array int[] in a list of int[]. -1 if no such array is found.
	 * @param cube a array of integer.
	 * @param listCubes a list of array of integer
	 * @return the index of the cube if the cube is in the listCubes, -1 otherwise
	 * @invariant array
	 * 			|if no array have been handled yet then cube has no index.
	 * 			|Otherwise if array == cube then cube has the index of the array.
	 * 			| if array != cube and listCubes.indexOf(array)==listCubes.size()-1 then the index of the cube is -1
	 */
	private int getIndex(int[] cube,List<int[]> listCubes){
		for(int[] array : listCubes){
        	if (equals(cube,array)){
        		return listCubes.indexOf(array);
        	}
        }
		return -1;
    }
	
	/**
	 * A method that return whether 2 arrays of integer have the same value.
	 * @param cube1 first cube that will be compared
	 * @param cube2	second cube that will be compared
	 * @return true if the cubes have the same value, false otherwise.
	 * 		|if cube1.length != cube2.length
	 * 		| then return false
	 * 		|if cube1[i] != cube2[i]
	 * 		| then return false
	 * 		|else
	 * 		| return true
	 * @invariant i
	 * 		|i increment a monotonous way.
	 */
	private boolean equals(int[]cube1,int[] cube2){
		if (cube1.length != cube2.length){
			return false;
		}
		for (int i=0; i<cube1.length;i++){
			if (cube1[i]!= cube2[i]){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * List that store the route that a unit must walk to get to the destination.
	 */
	private List<int[]> route = new ArrayList<int[]>();
	
	/**
	 * Updates the sprinting aspect of the unit.
	 * 
	 * @param dt
	 * @post if (this.isFalling()==false|| this.isResting()==false || this.isWorking()==false || this.isAttacking()==false
	 *       ||this.isDefending()==false) then this.isSprinting == false
	 * @post if (this.isSprinting == true) then this.sprintingCounter += dt if
	 *       (this.sprintingCounter > 0.1) then new.getCurrentStaminaPoints() ==
	 *       old.getCurrentStaminaPoints() - 1 then this.sprintingCounter -= 0.1
	 *       if (this.getCurrentStaminaPoints() == 0) then this.isSprinting ==
	 *       false
	 */
	private void updateSprinting(double dt) {
		if (this.isFalling()){
			this.stopSprinting();
		}
		if (this.isResting() || this.isWorking() || this.isAttacking() || this.isDefending())
			this.stopSprinting();
		if (this.isSprinting()) {
			this.sprintingCounter += dt;
			if (this.sprintingCounter > 0.1) {
				this.consumeStamina(1);
				this.sprintingCounter -= 0.1;
			}
			if (this.getCurrentStaminaPoints() == 0)
				this.stopSprinting();
		}
	}

	/**
	 * variable stores the time a unit is sprinting
	 */
	private double sprintingCounter;

	/**
	 * a method to update all actions
	 * 
	 * @param dt
	 * @post if (this.defaultBehaviour == true) then
	 *       this.startDefaultBehavior(); else if (this.isResting == true) then
	 *       this.restTimer += dt if (this.restTimer > 0.1) then new.restTimer
	 *       == old.restTimer - 0.1 then new.restCounter == old.restCounter +
	 *       this.getToughness() / 400.0; if (this.restCounter > 1) then
	 *       new.restCounter == old.restCounter - 1 if
	 *       (this.getCurrentHitPoints() < this.getMaxHitPoints()) then
	 *       this.regainHitpoints(1) if (this.getCurrentStaminaPoints() <
	 *       this.getMaxStaminaPoints()) then this.regainStamina(1) else
	 *       this.isResting==true
	 * 
	 *       if (this.isWorking == true) then new.workCounter == old.workCounter
	 *       - dt; if (this.workCounter < 0) then this.isWorking == false;
	 *
	 * 
	 *       this.gameRestTimer -= dt; if (this.gameRestTimer < 0) then
	 *       this.isResting == true ; then new.gameRestTimer = 180;
	 *
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentException 
	 */
	private void updateAction(double dt) throws IllegalArgumentExceptions, IllegalArgumentException, IllegalPositionException {
		if (this.isDefaultBehaviourEnabled())
			this.startDefaultBehavior();
		else {
			if (this.isResting) {
				this.restTimer += dt;
				if (this.restTimer > 0.1) {
					this.restTimer -= 0.1;
					this.restCounter += this.getToughness() / 400.0;
					if (this.restCounter > 1) {
						this.restCounter -= 1;
						if (this.getCurrentHitPoints() < this.getMaxHitPoints())
							this.regainHitpoints(1);
						else if (this.getCurrentStaminaPoints() < this.getMaxStaminaPoints())
							this.regainStamina(1);
						else
							this.stopResting();
					}
				}
			}
			if (this.isWorking()) {
				this.workCounter -= (float) dt;
				if (this.workCounter < 0)
					this.finishWorking();

			}
			this.gameRestTimer -= dt;
			if (this.gameRestTimer < 0) {
				this.startResting();
				this.gameRestTimer = 180;
			}
		}

	}

	/**
	 * Updates the fighting and defending aspect of the unit.
	 * 
	 * @param dt
	 * @post if (this.canAttack==false) then new.attackCounter ==
	 *       old.attackCounter - dt if (this.attackCounter < 0) then
	 *       defender.isValidLocation = false then this.isAttacking == false
	 *       then this.getOrientation == atan( defender.getPosition()[1] -
	 *       this.getPosition()[1], defender.getPosition()[0] -
	 *       this.getPosition()[0])) then defender.getOrientation == atan(
	 *       this.getPosition()[1] - defender.getPosition()[1],
	 *       this.getPosition()[0] - defender.getPosition()[0])
	 * @throws IllegalArgumentExceptions
	 */
	private void updateFight(double dt) throws IllegalArgumentExceptions {
		if (!this.canAttack) {
			this.attackCounter -= dt;
			if (this.attackCounter < 0) {
				if(defender.isAlive()){
				defender.isValidDodgeLocation = false;
				this.stopAttacking();
				this.setOrientation((float) Math.atan2(defender.getPosition()[1] - this.getPosition()[1],
						defender.getPosition()[0] - this.getPosition()[0]));
				
					defender.setOrientation((float) Math.atan2(this.getPosition()[1] - defender.getPosition()[1],
						this.getPosition()[0] - defender.getPosition()[0]));

			}}
		}

	}

	/**
	 * variable stores the time a unit is working
	 */
	private float workCounter;
	/**
	 * variable stores the HP or stamina that a unit must recover
	 */
	private float restCounter;
	/**
	 * variable stores the time a unit is resting
	 */
	private float restTimer;
	/**
	 * variable stores the time before a unit must rest
	 */
	private float gameRestTimer = 180;
	/**
	 * variable stores the time a unit is attacking
	 */
	private float attackCounter;

	/**
	 * de te testen cube
	 * 
	 * @param cubePosition
	 * 
	 * returnt de distance dat de unit moet lopen
	 * Indien onbereikbaar -> return Double.POSITIVE_INFINITY;
	 * @return
	 */
	
	public double getLenghtPath(int[] cubePosition) {
		if (this.getIndex(cubePosition, this.world.getListCubePassable())!=-1){
			return Math.sqrt(Math.pow((cubePosition[0]+0.5)-this.getPosition()[0],2)+Math.pow((cubePosition[1]+0.5)-this.getPosition()[1],2)+Math.pow((cubePosition[2]+0.5)-this.getPosition()[2],2));
		}else 
			return Double.POSITIVE_INFINITY;
	}

	

	

	
	@Override
	public String toString(){
		return this.getName();
	}

}