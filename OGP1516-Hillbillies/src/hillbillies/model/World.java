package hillbillies.model;

import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.util.ConnectedToBorder;
import hillbillies.helpers.*;
import java.util.*;
import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.*;

/**
 * Class Of world
 * 
 * @version 1.0
 * @author Dieter Verbruggen & Paolo Léonard
 *
 * @Invar The Type of a cube must be a valid Terraintype
 * 
 */
@Value
public class World {

	public List<int[]> listCubePassable;
	
	/**
	 * Constructor of the class World
	 * 
	 * @param terrainTypes
	 * @throws IllegalPositionException
	 * @throws IllegalTerraintypeException
	 */
	public World(int[][][] terrainTypes, TerrainChangeListener modelListener)
			throws IllegalPositionException, IllegalTerraintypeException {

		this.terrainListener = modelListener;
		this.worldBoundaries = new int[] { terrainTypes.length, terrainTypes[0].length, terrainTypes[0][0].length };
		this.connected = new ConnectedToBorder(getNbCubesX(), getNbCubesY(), getNbCubesZ());
		this.terrain = this.convertWorld(terrainTypes);
		this.refactorWorld();
		this.listCubePassable=this.initialisationCube();
	}

	/**
	 * A method that modify listCubePassable by adding all passable cube of the world.
	 * @pre listCubePassable must exist and be a list
	 * 		|listCubePassable instanceOf List<E>
	 * @post listCubePassable has all the passable terrain
	 * @invariant i
	 * 			| i is in the range of the x-boundaries world.
	 * 			| if no value of i have been handled yet, listCubePassable is empty.
	 * 			| otherwise listCubePassable contains all cubes met coordinates {x,y,z} that are passable & that have solid neighbor.
	 * @invariant j
	 * 			| j is in the range of the y-boundaries world.
	 * 			| if no value of j have been handled yet, listCubePassable is empty.
	 * 			| otherwise listCubePassable contains all cubes met coordinates {i,y,z} that are passable & that have solid neighbor.
	 *@invariant k
	 * 			| k is in the range of the z-boundaries world.
	 * 			| if no value of k have been handled yet, listCubePassable is empty.
	 * 			| otherwise listCubePassable contains all cubes met coordinates {i,j,z} that are passable & that have solid neighbor.
	 */
	private List<int[]> initialisationCube(){
		List<int[]> listCubePassable = new ArrayList<int[]>();
		for (int i=0;i<this.getNbCubesX();i++){
			for (int j=0;j<this.getNbCubesY();j++){
				for (int k=0;k+1<this.getNbCubesZ();k++){
					if(!this.isSolid(i, j, k+1) && this.hasSolidNeighbor(i, j, k+1) && this.getIndex(new int[] {i,j,k},listCubePassable)==-1){
						listCubePassable.add(new int[] {i,j,k});
					}
				}
			}
		}
		return listCubePassable;
	}
	
	public List<int[]> getListCubePassable() {
		return listCubePassable;
	}

	/**
	 * Method that return if a given cube has solid neighbor. 
	 * @param x the x-coordinate of the cube being analyzed.
	 * @param y the y-coordinate of the cube being analyzed.
	 * @param z the z-coordinate of the cube being analyzed.
	 * @return  if the coordinates are out of the world boundaries then it return false,
	 * 			otherwise if at least one solid cube exist in the neighbor of the cube return true,
	 * 			otherwise return false
	 * 			|if x+i > this.world.getNbCubesX() or y+j> this.world.getNbCubesY() or z+k>this.world.getNbCubesZ()
	 * 			| then return false
	 * 			|if this.world.isSolid(x+i,y+j,z+k)
	 * 			| then return true
	 * 			|else
	 * 			| return false
	 */	
	public boolean hasSolidNeighbor(int x, int y, int z){
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				for (int k=-1;k<2;k++){
					if (x+i>this.getNbCubesX() || y+j> this.getNbCubesY() || z+k>this.getNbCubesZ()){
						return false;
					}
					if(this.isSolid(x+i, y+j, z+k)){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Get the index of an array int[] in a list of int[]. -1 if no such array is found.
	 * @param cube a array of integer.
	 * @param listCubes a list of array of integer
	 * @return the index of the cube if the cube is in the listCubes, -1 otherwise
	 * @invariant array
	 * 			|if no array have been handled yet then cube has no index.
	 * 			|Otherwise if array == cube then cube has the index of the array.
	 * 			| if array != cube and listCubes.indexOf(array)==listCubes.size()-1 then the index of the cube is -1
	 */
	private int getIndex(int[] cube,List<int[]> listCubes){
		for(int[] array : listCubes){
        	if (array[0]==cube[0] && array[1]==cube[1] && array[2]==cube[2]){
        		return listCubes.indexOf(array);
        	}
        }
		return -1;
    }

	/**
	 * Returns the length of the world in the X-direction
	 * 
	 * @return the length of the world in the X-direction
	 */
	@Raw
	@Basic
	public final int getNbCubesX() {
		return worldBoundaries[0];
	}

	/**
	 * Returns the length of the world in the Y-direction
	 * 
	 * @return the length of the world in the Y-direction
	 */
	@Raw
	@Basic
	public final int getNbCubesY() {
		return worldBoundaries[1];
	}

	/**
	 * Returns the length of the world in the Z-direction
	 * 
	 * @return the length of the world in the Z-direction
	 */
	@Raw
	@Basic
	public final int getNbCubesZ() {
		return worldBoundaries[2];
	}

	/**
	 * Returns the boundaries of the world
	 * 
	 * @return the boundaries of the world
	 */
	@Raw
	@Basic
	public int[] getWorldBoudaries() {
		return this.worldBoundaries;
	}

	/**
	 * Variable storing the boundaries of the world.
	 */
	private int[] worldBoundaries = { 50, 50, 50 };

	// Manipulating the world //
	/**
	 * Returns the type of terrain from the given coordinates
	 * 
	 * @param x
	 *            x-coordinate of the cube
	 * @param y
	 *            y-coordinate of the cube
	 * @param z
	 *            z-coordinate of the cube
	 * @return the type of terrain from the given coordinates
	 */
	@Raw
	@Basic
	public int getCubeType(int x, int y, int z) {
		return this.terrain[x][y][z].getType();
	}

	/**
	 * Sets a cube to a given terraintype
	 * 
	 * @param x
	 *            x-coordinate of the cube
	 * @param y
	 *            y-coordinate of the cube
	 * @param z
	 *            z-coordinate of the cube
	 * @param value
	 *            The value of the new terrain of the cube
	 * @throws IllegalActionException
	 *             If the given value has no corresponding terrain type, throw
	 *             an IllegalActionException
	 * @post new.getCubeType() == value
	 */
	@Raw
	public void setCubeType(int x, int y, int z, int value) throws IllegalActionException {
		try {
			this.terrain[x][y][z].setType(value);
		} catch (IllegalTerraintypeException e) {
			throw new IllegalActionException();
		}
	}

	// Units //

	/**
	 * Spawns a new unit with random starting Values and tries to add it to the
	 * world
	 * 
	 * @param enableDefaultBehavior
	 *            boolean to enable or disable the default behaviour of the unit
	 * @return A unit with random starting values
	 * @throws IllegalArgumentExceptions
	 *             if the Unit has invalid arguments throws
	 *             IllegalArgumentExceptions
	 * @throws IllegalPositionException
	 *             If the unit has an invalid position throw
	 *             IllegalPositionException
	 * @throws IllegalWorldException
	 */
	public Unit spawnUnit(boolean enableDefaultBehavior)
			throws IllegalArgumentExceptions, IllegalPositionException, IllegalWorldException {
		Random randomGen = new Random();
		int x = -1;
		int y = -1;
		int z = -1;
		boolean foundLocation = false;
		while (!foundLocation) {
			x = randomGen.nextInt(getNbCubesX());
			y = randomGen.nextInt(getNbCubesY());
			z = randomGen.nextInt(getNbCubesZ() - 1);
			if (z == 0 && !this.terrain[x][y][z].isSolid())
				break;
			else if (!this.terrain[x][y][z + 1].isSolid() && this.terrain[x][y][z].isSolid())
				break;
		}

		Unit unit = new Unit("HillBillie", new int[] { x, y, z }, randomGen.nextInt(75) + 25,
				randomGen.nextInt(75) + 25, randomGen.nextInt(75) + 25, randomGen.nextInt(75) + 25,
				enableDefaultBehavior, this);
		try {
			this.addUnit(unit);
			return unit;
		} catch (IllegalUnitExceptions e) {
			unit.terminate();
			return null;
		}
	}

	/**
	 * Adds the unit to a given world and faction
	 * 
	 * @param unit
	 *            the unit that has to be added
	 * @throws IllegalUnitExceptions
	 *             if there are to many units
	 * @post the unit lives in the world and has a valid faction
	 */
	public void addUnit(Unit unit) throws IllegalUnitExceptions {
		if (unit.getFaction() != null){
			this.units.add(unit);
			unit.setWorld(this);
		} else{
		int[] nbUnitsfaction = this.getNbUnitFaction();
		Arrays.sort(nbUnitsfaction);
		if (this.getUnits().size() > 99)
			throw new IllegalUnitExceptions();
		if ((nbUnitsfaction[nbUnitsfaction.length - 1] > 50))
			throw new IllegalUnitExceptions();
		this.addUnitFaction(unit);}
	}

	/**
	 * Returns a set containing all the units of the world
	 * 
	 * @return a set containing all the units of the world
	 */
	@Raw
	@Basic
	public Set<Unit> getUnits() {
		return this.units;
	}

	/**
	 * Returns all active factions of the world
	 * 
	 * @return all active factions of the world
	 */
	@Basic
	@Raw
	public Set<Faction> getActiveFactions() {
		Set<Faction> activeFactions = new HashSet<Faction>();
		for (Unit u : this.getUnits()) {
			if (!activeFactions.contains(u.getFaction()))
				activeFactions.add(u.getFaction());
		}
		return activeFactions;
	}

	/**
	 * Returns all units of a given faction
	 * 
	 * @param faction
	 *            The faction from witch the units are asked
	 * @return all units of a given faction
	 */
	@Raw
	@Basic
	public Set<Unit> getUnitsOfFaction(Faction faction) {
		Set<Unit> unitOfFaction = new HashSet<Unit>();
		for (Unit u : this.getUnits()) {
			if (u.getFaction() == faction)
				unitOfFaction.add(u);
		}
		return unitOfFaction;
	}

	/**
	 * Returns a list containing the number of units in a faction
	 * 
	 * @return a list containing the number of units in a faction
	 */
	@Raw
	@Basic
	private int[] getNbUnitFaction() {
		int[] nbUnitsFaction = new int[Faction.values().length];
		for (Unit u : this.getUnits()) {
			nbUnitsFaction[u.getFaction().getId()] += 1;
		}
		return nbUnitsFaction;

	}

	/**
	 * Adds a given unit to a random faction (the faction with the least units)
	 * 
	 * @param unit
	 *            the unit that need to be added
	 * @post the unit will be part of a faction and live in this world
	 */
	private void addUnitFaction(Unit unit) {
		int key = 0;
		int[] nbUnits = getNbUnitFaction();
		for (int i = 0; i < Faction.values().length; i++) {
			if (nbUnits[i] < nbUnits[key] && nbUnits[i] < 50) {
				key = i;
			}
		}
		if (nbUnits[key] < 50) {
			unit.setFaction(key);
			this.units.add(unit);
			unit.setWorld(this);
			unit.getFaction().recruitUnit(unit);
		}

	}

	/**
	 * Variable storing all the Units of a world
	 */
	private final Set<Unit> units = new HashSet<Unit>();

	// Logs and Boulder//
	/**
	 * Returns a set containing all the logs of the world
	 * 
	 * @return a set containing all the logs of the world
	 */
	@Basic
	@Raw
	public Set<Log> getLogs() {
		return this.logs;
	}

	/**
	 * Returns a set containing all the boulders of the world
	 * 
	 * @return a set containing all the boulders of the world
	 */
	@Basic
	@Raw
	public Set<Boulder> getBoulders() {
		return this.boulders;
	}

	/**
	 * Adds an object boulder to the world
	 * 
	 * @param boulder
	 *            the boulder that need to be added
	 * @throws IllegalWorldException
	 *             If the object is not from this world throw
	 *             IllegalWorldException
	 * @post the boulder is now in this.boulders
	 */
	@Raw
	public void addBoulder(Boulder boulder) throws IllegalWorldException {
		if (!(boulder.getWorld() == this))
			throw new IllegalWorldException();
		this.boulders.add(boulder);
	}

	/**
	 * Adds a log to the world
	 * 
	 * @param log
	 *            the log that need to be added
	 * @throws IllegalWorldException
	 *             If the object is not from this world throw
	 *             IllegalWorldException
	 * @post the log is now in this.logs
	 */
	@Raw
	public void addLog(Log log) throws IllegalWorldException {
		if (!(log.getWorld() == this))
			throw new IllegalWorldException();
		this.logs.add(log);
	}


	/**
	 * Removes a boulder from the world
	 * 
	 * @param boulder
	 *            the boulder to remove
	 * @throws IllegalWorldException
	 *             if the boulder is not from this world
	 * @post the boulder isn't in the world
	 */
	public void removeBoulder(Boulder boulder) throws IllegalWorldException {
		if (!(boulder.getWorld() == this))
			throw new IllegalWorldException();
		this.boulders.remove(boulder);
	}

	/**
	 * Removes a log from the world
	 * 
	 * @param log
	 *            the log to remove
	 * @throws IllegalWorldException
	 *             if the log is not from this world
	 * @post the log isn't in the world
	 */
	public void removeLog(Log log) throws IllegalWorldException {
		if (!(log.getWorld() == this))
			throw new IllegalWorldException();
		this.logs.remove(log);
	}

	
	/**
	 * Spawns a new Boulder in this world.
	 * 
	 * @param pos
	 *            the position of the Boulder
	 * @throws IllegalPositionException
	 *             If the position is not a legal position throw an
	 *             IllegalPositionException
	 * @throws IllegalWorldException
	 *             If the world is not legal throw an IllegalWorldException
	 * @effect a new boulder is added to the set |this.boulders.add(boulder);
	 */
	@Raw
	@Basic
	public void spawnBoulder(int[] pos) throws IllegalPositionException, IllegalWorldException {
		new Boulder(pos, this);
	}

	/**
	 * Spawns a new Log in this world.
	 * 
	 * @param pos
	 *            the position of the Log
	 * @param world
	 *            The world the log lives in
	 * @throws IllegalPositionException
	 *             If the position is not a legal position throw an
	 *             IllegalPositionException
	 * @throws IllegalWorldException
	 *             If the world is not legal throw an IllegalWorldException
	 * @effect a new log is added to the set |this.logs.add(log);
	 */
	@Raw
	@Basic
	public void spawnLog(int[] pos) throws IllegalPositionException, IllegalWorldException {
		new Log(pos, this);
		
	}

	/**
	 * Variable storing all the boulders of a world
	 */
	private final Set<Boulder> boulders = new HashSet<Boulder>();
	
	/**
	 * Variable storing all the boulders of a world that are picked up
	 */
	private final Set<Boulder> pickedUpBoulders = new HashSet<Boulder>();

	/**
	 * Variable storing all the logs of a world
	 */
	private final Set<Log> logs = new HashSet<Log>();
	
	/**
	 * Variable storing all the logs of a world that are picked up
	 */
	private final Set<Log> pickedUpLogs = new HashSet<Log>();

	
	/**
	 * Returns a boulder if one available on the given cube
	  * @param x
	 *            X-Coordinate of the cube
	 * @param y
	 *            Y-Coordinate of the cube
	 * @param z
	 *            Z-Coordinate of the cube
	 * @return
	 * 			boulder if available
	 * @throws IllegalActionException
	 */
	public Boulder getBoulderCube(int x, int y, int z){
		if (!this.getBoulders().isEmpty()){
			for (Boulder boulder: this.getBoulders()){
				if (boulder.getCubePosition()[0] == x && boulder.getCubePosition()[1] == y && boulder.getCubePosition()[2] == z)
					return boulder;
			}
		}
		return null;
	}
	
	/**
	 * Returns a log if one available on the given cube
	  * @param x
	 *            X-Coordinate of the cube
	 * @param y
	 *            Y-Coordinate of the cube
	 * @param z
	 *            Z-Coordinate of the cube
	 * @return
	 * 			log if available
	 * @throws IllegalActionException
	 */
	public Log getLogCube(int x, int y, int z){
		if (!this.getLogs().isEmpty()){
			for (Log log: this.getLogs()){
				if (log.getCubePosition()[0] == x && log.getCubePosition()[1] == y && log.getCubePosition()[2] == z)
					return log;
			}
		}
		return null;
	}
	
	/**
	 * Returns a unit if one available on the given cube
	  * @param x
	 *            X-Coordinate of the cube
	 * @param y
	 *            Y-Coordinate of the cube
	 * @param z
	 *            Z-Coordinate of the cube
	 * @return
	 * 			unit if available
	 * @throws IllegalActionException
	 */
	public Unit getUnitCube(int x, int y, int z){
		if (!this.getUnits().isEmpty()){
			for (Unit unit: this.getUnits()){
				if (unit.getCubeCoordinate()[0] == x && unit.getCubeCoordinate()[1] == y && unit.getCubeCoordinate()[2] == z)
					return unit;
			}
		}
		return null;
	}
	
	// AdvancedTime//
	/**
	 * A methode to update all units in the world
	 * 
	 * @param dt
	 *            a variable indicating the time that have passed
	 * @throws IllegalWorldException 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 * @effect All the boulders, logs and unit of the world will be updated
	 */
	public void advancedTime(double dt) throws IllegalWorldException {
		if (!this.getUnits().isEmpty()) {
			for (Unit u : this.getUnits())
				if (u.isAlive()) {
					try {
						u.advanceTime(dt);
					} catch (IllegalActionException e) {
					}
				} else {
					u.terminate();
					this.units.remove(u);
					u.getFaction().fireUnit(u);
				}
		}
		if (!this.getLogs().isEmpty()) {
			for (Log log : this.getLogs()){
				if (log.isConsumed())
					this.removeLog(log);
				if (log.isPickedUp()){
					this.pickedUpLogs.add(log);
					this.logs.remove(log);
				}
				try {
					log.advanceTime(dt);
				} catch (IllegalPositionException e) {
				}}
		}
		if (!this.pickedUpLogs.isEmpty()){
			for (Log log : this.pickedUpLogs){
				if (!log.isPickedUp()){
					this.pickedUpLogs.remove(log);
					this.logs.add(log);
				}
			}
		}
		if (!this.getBoulders().isEmpty()) {
			
			for (Boulder boulder : this.getBoulders()){
				if (boulder.isConsumed())
					this.removeBoulder(boulder);
				if (boulder.isPickedUp()){
					
					this.pickedUpBoulders.add(boulder);
					this.boulders.remove(boulder);
				}
				try {
					boulder.advanceTime(dt);
				} catch (IllegalPositionException e) {
				}}
		}
		if (!this.pickedUpBoulders.isEmpty()){
			for (Boulder boulder : this.pickedUpBoulders){
				if (!boulder.isPickedUp()){
					this.pickedUpBoulders.remove(boulder);
					this.boulders.add(boulder);
				}
			}
		}
	}

	// Terrain//

	/**
	 * Returns whether or not the cube on the given location is solid or not.
	 * 
	 * @param x
	 *            X-Coordinate
	 * @param y
	 *            Y-Coordinate
	 * @param z
	 *            Z-Coordinate
	 * @return a boolean, true if the cube is solid
	 */
	public boolean isSolid(int x, int y, int z) {
		
		if (x >= this.getNbCubesX() || x < 0 || y >= this.getNbCubesY() || y < 0 || z >= this.getNbCubesZ()) {
			return false;
		}
		if (z==-1)
			return true;
		if (z<0)
			return false;
		return this.terrain[x][y][z].isSolid();
	}

	/**
	 * Transform the given terrain into a terrain with cubes And Updates the
	 * Connected to border
	 * 
	 * @param terrain
	 *            the terrain to transform
	 * @return A new terrain existing of cubes
	 * 
	 * @throws IllegalTerraintypeException
	 *             If the cubetype is not valid
	 */
	private Cube[][][] convertWorld(int[][][] terrain) throws IllegalTerraintypeException {
		Cube[][][] newTerrain = new Cube[getNbCubesX()][getNbCubesY()][getNbCubesZ()];
		for (int x = 0; x < getNbCubesX(); x++) {
			for (int y = 0; y < getNbCubesY(); y++) {
				for (int z = 0; z < getNbCubesZ(); z++) {
					newTerrain[x][y][z] = new Cube(terrain[x][y][z]);
					if (terrain[x][y][z]==3)
						try {
							this.workshops.add(new Position(new double[] {x,y,z}, this.getWorldBoudaries()));
						} catch (IllegalPositionException e) {}
					if (!newTerrain[x][y][z].isSolid()) {
						connected.changeSolidToPassable(x, y, z);

					}
				}
			}
		}
		return newTerrain;
	}

	/**
	 * Checks whether or not there are floating cubes in the world. If they
	 * occur, they will collapse.
	 * 
	 * @effect There will be no more floating cubes
	 */
	private void refactorWorld() {

		for (int x = 0; x < getNbCubesX(); x++) {
			for (int y = 0; y < getNbCubesY(); y++) {
				for (int z = 0; z < getNbCubesZ(); z++) {
					if (this.terrain[x][y][z].isSolid()) {
						if (!this.isSolidConnectedToBorder(x, y, z)) {
							this.collapse(x, y, z, -1);
						}
					}
				}
			}
		}
	}

	/**
	 * When a cube is worked on or floating, the cube will transform into air
	 * and sometimes drop a log or a boulder
	 * 
	 * @param x
	 *            the X-coordinate of the cube to collapse
	 * @param y
	 *            the Y-coordinate of the cube to collapse
	 * @param z
	 *            the Z-coordinate of the cube to collapse
	 * @param type
	 *            The Type of cube: -1 = doesn't mater 1 = Rock -> drop boulder
	 *            2 = Tree -> drop log
	 * @post All cubes that aren't connected to border will collapse and have a
	 *       chance to spawn an according object
	 */
	public void collapse(int x, int y, int z, int type) {
		if (type == -1) {
			double log_boulder = Math.random();
			try {
				this.terrain[x][y][z].setType(0);
			} catch (IllegalTerraintypeException e2) {
				;
			}
			this.notifyTerrainChanged(x, y, z);
			if (log_boulder < 0.125)
				try {
					this.spawnBoulder(new int[] { x, y, z });
				} catch (IllegalWorldException | IllegalPositionException e1) {
				}
			else if (log_boulder < 0.25)
				try {
					this.spawnLog(new int[] { x, y, z });
				} catch (IllegalWorldException | IllegalPositionException e) {
				}
		} else if (type == 1 || type == 2) {
			List<int[]> needToChange = this.connected.changeSolidToPassable(x, y, z);
			try {
				this.terrain[x][y][z].setType(0);
			} catch (IllegalTerraintypeException e2) {
			}
			this.notifyTerrainChanged(x, y, z);
			if (type == 1)
				try {
					this.spawnBoulder(new int[] { x, y, z });
				} catch (IllegalWorldException | IllegalPositionException e1) {
				}
			else if (type == 2)
				try {
					this.spawnLog(new int[] { x, y, z });
				} catch (IllegalWorldException | IllegalPositionException e) {
				}
			for (int[] cor : needToChange) {
				this.collapse(cor[0], cor[1], cor[2], this.getCubeType(cor[0], cor[1], cor[2]));
			}
		}
	}

	/**
	 * Checks whether or not a cube is connected to the border.
	 * 
	 * @param x
	 *            the X-Coordinate of the cube
	 * @param y
	 *            the y-Coordinate of the cube
	 * @param z
	 *            the z-Coordinate of the cube
	 * @return True is the cube is solid connected to the border
	 */
	public boolean isSolidConnectedToBorder(int x, int y, int z) {
		return this.connected.isSolidConnectedToBorder(x, y, z);

	}

	/**
	 * Notifies the terrainListener if the terrain has chanced.
	 * 
	 * @param x
	 *            the X-Coordinate of the cube
	 * @param y
	 *            the y-Coordinate of the cube
	 * @param z
	 *            the z-Coordinate of the cube
	 * @effect the gui will be updated
	 */
	public void notifyTerrainChanged(int x, int y, int z) {
		this.terrainListener.notifyTerrainChanged(x, y, z);
	}

	/**
	 * Variable storing the terrain of the world
	 */
	private Cube[][][] terrain;
	/**
	 * Variable storing the TerrainChangeListener
	 */
	private TerrainChangeListener terrainListener;
	/**
	 * variable to check the connectivity to the border
	 */
	private ConnectedToBorder connected;

	/**
	 * Makes the world a string
	 */
	@Override
	public String toString() {
		return "World:" + getNbCubesX() + "x" + getNbCubesY() + "x" + getNbCubesZ();
	}
	
	private Set<Position> workshops = new HashSet<Position>();
	
	public Set<Position> getWorkshops(){
		return this.workshops;
	}

}
