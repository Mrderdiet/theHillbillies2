package hillbillies.tasks;

import java.util.ArrayList;
import java.util.List;

import hillbillies.model.Task;
import hillbillies.part3.programs.ITaskFactory;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.tasks.expressions.*;
import hillbillies.tasks.statements.HelperStatements;
import hillbillies.tasks.statements.Statement;
import hillbillies.tasks.type.*;
import hillbillies.tasks.type.Boolean;

@SuppressWarnings("rawtypes")
public class TaskFactory implements ITaskFactory<Expression,Statement, Task> {

	@Override
	public List<Task> createTasks(String name, int priority, Statement activity, List<int[]> selectedCubes) {

		List<Task> tasks = new ArrayList<Task>();
		if (selectedCubes.isEmpty()){
			Task task = new Task(name, priority,activity,null);
			tasks.add(task);
		}
		for (int[] cube: selectedCubes){
			Task task = new Task(name, priority,activity,cube);
			tasks.add(task);
		}
		return tasks;
	}

	@Override
	public Statement createAssignment(String variableName, Expression value, SourceLocation sourceLocation) {
		return HelperStatements.createAssignment(variableName, value, sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createWhile(Expression condition, Statement body, SourceLocation sourceLocation) {
		return HelperStatements.createWhile(condition, body, sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createIf(Expression condition, Statement ifBody, Statement elseBody, SourceLocation sourceLocation) {
		return HelperStatements.createIf(condition, ifBody, elseBody, sourceLocation);
	}

	@Override
	public Statement createBreak(SourceLocation sourceLocation) {
		return HelperStatements.createBreak(sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createPrint(Expression value, SourceLocation sourceLocation) {
		return HelperStatements.createPrint(value, sourceLocation);
	}

	@Override
	public Statement createSequence(List<Statement> statements, SourceLocation sourceLocation) {
		return HelperStatements.createSequence(statements, sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createMoveTo(Expression position, SourceLocation sourceLocation) {
		return HelperStatements.createMoveTo(position, sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createWork(Expression position, SourceLocation sourceLocation) {
		return HelperStatements.createWork(position, sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createFollow(Expression unit, SourceLocation sourceLocation) {
		return HelperStatements.createFollow(unit, sourceLocation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Statement createAttack(Expression unitTarget, SourceLocation sourceLocation) {
		return HelperStatements.createAttack(unitTarget, sourceLocation);
	}

	@Override
	public Expression createReadVariable(String variableName, SourceLocation sourceLocation) {
		return HelperExpressions.createReadVariable(variableName, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createIsSolid(Expression position, SourceLocation sourceLocation) {
		return HelperExpressions.createIsSolid(position, sourceLocation);
	}

	@SuppressWarnings({ "unchecked"	})
	@Override
	public Expression<Boolean> createIsPassable(Expression position, SourceLocation sourceLocation) {
		return HelperExpressions.createIsPassable(position, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createIsFriend(Expression unit, SourceLocation sourceLocation) {
		return HelperExpressions.createIsFriend(unit, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createIsEnemy(Expression unit, SourceLocation sourceLocation) {
		return HelperExpressions.createIsEnemy(unit, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createIsAlive(Expression unit, SourceLocation sourceLocation) {
		return HelperExpressions.createIsAlive(unit, sourceLocation);
	}

	@SuppressWarnings({"unchecked" })
	@Override
	public Expression<Boolean> createCarriesItem(Expression unit, SourceLocation sourceLocation) {
		return HelperExpressions.createCarriesItem(unit, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createNot(Expression expression, SourceLocation sourceLocation) {
		return HelperExpressions.createNot(expression, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createAnd(Expression left, Expression right, SourceLocation sourceLocation) {
		return HelperExpressions.createAnd(left, right, sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<Boolean> createOr(Expression left, Expression right, SourceLocation sourceLocation) {
		return HelperExpressions.createOr(left, right, sourceLocation);
	}

	@Override
	public Expression<PositionTask> createHerePosition(SourceLocation sourceLocation) {
		return HelperExpressions.createHerePosition(sourceLocation);
	}

	@Override
	public Expression<PositionTask> createLogPosition(SourceLocation sourceLocation) {
		return HelperExpressions.createLogPosition(sourceLocation);
	}

	@Override
	public Expression<PositionTask> createBoulderPosition(SourceLocation sourceLocation) {
		return HelperExpressions.createBoulderPosition(sourceLocation);
	}

	@Override
	public Expression<PositionTask> createWorkshopPosition(SourceLocation sourceLocation) {
		return HelperExpressions.createWorkshopPosition(sourceLocation);
	}

	@Override
	public Expression<PositionTask> createSelectedPosition(SourceLocation sourceLocation) {
		return HelperExpressions.createSelectedPosition(sourceLocation);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<PositionTask> createNextToPosition(Expression position, SourceLocation sourceLocation) {
		return HelperExpressions.createNextToPosition(position, sourceLocation);
	}

	@Override
	public Expression<PositionTask> createLiteralPosition(int x, int y, int z, SourceLocation sourceLocation) {
		return HelperExpressions.createLiteralPosition(x, y, z, sourceLocation);
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public Expression<PositionTask> createPositionOf(Expression unit, SourceLocation sourceLocation) {
		return HelperExpressions.createPositionOf(unit,sourceLocation );
	}
	@Override
	public Expression<UnitTask> createThis(SourceLocation sourceLocation) {
		return HelperExpressions.createThis(sourceLocation);
	}

	@Override
	public Expression<UnitTask> createFriend(SourceLocation sourceLocation) {
		return HelperExpressions.createFriend(sourceLocation);
	}

	@Override
	public Expression<UnitTask> createEnemy(SourceLocation sourceLocation) {
		return HelperExpressions.createEnemy(sourceLocation);
	}

	@Override
	public Expression<UnitTask> createAny(SourceLocation sourceLocation) {
		return HelperExpressions.createAny(sourceLocation);
	}

	@Override
	public Expression<Boolean> createTrue(SourceLocation sourceLocation) {
		return HelperExpressions.createTrue(sourceLocation);
	}

	@Override
	public Expression<Boolean> createFalse(SourceLocation sourceLocation) {
		return HelperExpressions.createFalse(sourceLocation);
	}

	

	

}
