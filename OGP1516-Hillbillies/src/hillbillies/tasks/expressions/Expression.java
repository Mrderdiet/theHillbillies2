package hillbillies.tasks.expressions;


import hillbillies.model.Task;


public interface Expression<R> {

	public abstract R evaluate(Task self);


}
