package hillbillies.tasks.expressions;

import java.util.function.BiFunction;
import hillbillies.model.Task;
import hillbillies.tasks.type.Type;

public class ExpressionBinary<T extends Type, O extends Type, R extends Type> implements Expression<R>  {

	
	private BiFunction<T, O, R> function;
	private Expression<T> left;
	private Expression<O> right;
	
	public ExpressionBinary(BiFunction<T, O, R> function, Expression<T> left, Expression<O> right) {
		this.function = function;
		this.left = left;
		this.right = right;
	}
	
	public Expression<T> getLeftExpression() {
		return this.left;
	}
	
	public Expression<O> getRightExpression() {
		return this.right;
	}
	public BiFunction<T, O, R> getFunction() {
		return function;
	}
	@Override
	public R evaluate(Task self) {
		return this.getFunction().apply(this.getLeftExpression().evaluate(self), this.getRightExpression().evaluate(self));
				
	}

}
