package hillbillies.tasks.expressions;


import hillbillies.model.Task;
import hillbillies.tasks.type.Type;

public class ExpressionConstant<R extends Type> implements Expression<R> {

	private final R value;

	public ExpressionConstant(R value) {
		this.value = value;
	}
	
	public R getValue() {
		return this.value;
	}


	@Override
	public R evaluate(Task self) {
		return this.getValue();
	}

}
