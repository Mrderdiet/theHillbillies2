package hillbillies.tasks.expressions;

import hillbillies.model.Task;
import hillbillies.tasks.type.Type;

public class ExpressionReadVariable<R extends Type> implements Expression<R> {

	private String variableName;
	
	public ExpressionReadVariable(String variableName) {
		this.variableName = variableName;
	}
	
	private String getName(){
		return this.variableName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public R evaluate(Task self) {
		return (R) self.getVariable(this.getName()).evaluate(self);
	}

}
