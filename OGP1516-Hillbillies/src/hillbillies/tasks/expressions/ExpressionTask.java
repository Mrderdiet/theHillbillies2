package hillbillies.tasks.expressions;

import java.util.function.Function;
import hillbillies.model.Task;
import hillbillies.tasks.type.Type;
import hillbillies.tasks.type.TaskHelper;

public class ExpressionTask<T extends Type,R extends Type> implements Expression<R> {

	public ExpressionTask(Function<T, R> function){
		this.function = function;
	}
	
	public Function<T, R> getFunction() {
		return function;
	}
	
	private Function<T, R> function;
	
	@SuppressWarnings("unchecked")
	@Override
	public R evaluate(Task self) {
		TaskHelper taskhelper = new TaskHelper(self);
		return this.getFunction().apply((T) taskhelper);
	}

}
