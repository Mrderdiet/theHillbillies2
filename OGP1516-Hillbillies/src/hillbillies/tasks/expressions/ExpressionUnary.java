package hillbillies.tasks.expressions;


import java.util.function.Function;
import hillbillies.model.Task;
import hillbillies.tasks.type.Type;


public class ExpressionUnary<T extends Type, R extends Type> implements Expression<R> {

	private Function<T, R> function;
	private Expression<T> expression;

	public ExpressionUnary(Function<T, R> function, Expression<T> expression) {
		this.function = function;
		this.expression = expression;
	}
	
	public Expression<T> getExpression() {
		return expression;
	}
	public Function<T, R> getFunction() {
		return function;
	}
	
	
	@Override
	public R evaluate(Task self) {
		return this.getFunction().apply(this.getExpression().evaluate(self));
	}

}