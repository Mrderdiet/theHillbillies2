package hillbillies.tasks.expressions;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.tasks.type.Boolean;
import hillbillies.tasks.type.PositionTask;
import hillbillies.tasks.type.Type;
import hillbillies.tasks.type.UnitTask;
import hillbillies.tasks.type.TaskHelper;

public class HelperExpressions {
	
	/* Constant Expressions */
	
	public static Expression<Boolean> createTrue(SourceLocation sourceLocation) {
		return new ExpressionConstant<Boolean>(new Boolean(true));
	}

	
	public static Expression<Boolean> createFalse(SourceLocation sourceLocation) {
		return new ExpressionConstant<Boolean>(new Boolean(false));
	}

	/* Unit Expressions */
	public static Expression<PositionTask> createHerePosition(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.getPosition());
	}

	public static Expression<PositionTask> createLogPosition(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.getLogPosition());
	}

	
	public static Expression<PositionTask> createBoulderPosition(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.getBoulderPosition());
	}

	
	public static Expression<PositionTask> createWorkshopPosition(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.getWorkshop());
	}
	

	public static Expression<PositionTask> createSelectedPosition(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.SelectedPosition());
	}
	
	public static Expression<PositionTask> createLiteralPosition(int x, int y, int z, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.LiteralPosition(x, y, z));
	}
	
	public static Expression<PositionTask> createNextToPosition(Expression<PositionTask> position, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.NextToPosition(position));
	}
	
	public static Expression<PositionTask> createPositionOf(Expression<UnitTask> unit, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,PositionTask>(e -> e.PositionOf(unit));
	}

	public static Expression<UnitTask> createThis(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,UnitTask>(e -> e.getThis());
	}

	
	public static Expression<UnitTask> createFriend(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,UnitTask>(e -> e.getFriend());
	}

	
	public static Expression<UnitTask> createEnemy(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,UnitTask>(e -> e.getEnemy());
	}

	public static Expression<UnitTask> createAny(SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,UnitTask>(e -> e.getAny());
	}
	
	public static Expression<Boolean> createIsSolid(Expression<PositionTask> position, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,Boolean>(e -> e.isSolid(position));
	}
	
	public static Expression<Boolean> createIsPassable(Expression<PositionTask> position, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,Boolean>(e -> e.isPassable(position));
	}

	
	public static Expression<Boolean> createIsFriend(Expression<UnitTask> unit, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,Boolean>(e -> e.isFriend(unit));
	}

	
	public static Expression<Boolean> createIsEnemy(Expression<UnitTask> unit, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,Boolean>(e -> e.isEnemy(unit));
	}

	
	public static Expression<Boolean> createIsAlive(Expression<UnitTask> unit, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,Boolean>(e -> e.isAlive(unit));
	}

	
	public static Expression<Boolean> createCarriesItem(Expression<UnitTask> unit, SourceLocation sourceLocation) {
		return new ExpressionTask<TaskHelper,Boolean>(e -> e.isCarring(unit));
	}
	
	
	

	/* Unary Expressions */
	
	

	public static Expression<Boolean> createNot(Expression<Boolean> expression, SourceLocation sourceLocation) {
		return new ExpressionUnary<Boolean, Boolean>(e -> e.not(), expression);	
	}
	
	/* Binary Expressions */
	

	public static Expression<Boolean> createAnd(Expression<Boolean>  left, Expression<Boolean> right, SourceLocation sourceLocation) {
		return new ExpressionBinary<Boolean,Boolean,Boolean>((l,r)-> l.and(r), left, right);
	}

	
	public static Expression<Boolean> createOr(Expression<Boolean> left, Expression<Boolean> right, SourceLocation sourceLocation) {
		return new ExpressionBinary<Boolean,Boolean,Boolean>((l,r)-> l.or(r), left, right);
	}
	
	/* Other */
	
	public static <T extends Type> Expression<T> createReadVariable(String variableName, SourceLocation sourceLocation) {
		return new ExpressionReadVariable<T>(variableName);
	}


}
