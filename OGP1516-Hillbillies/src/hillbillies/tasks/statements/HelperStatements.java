package hillbillies.tasks.statements;

import java.util.List;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.*;
import hillbillies.tasks.type.Boolean;

public class HelperStatements {
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Statement createAssignment(String variableName, Expression value, SourceLocation sourceLocation) {
		return new StatementAssignment(variableName, value);
	}

	
	public static Statement createWhile(Expression<Boolean> condition, Statement body, SourceLocation sourceLocation) {
		return new StatementWhile(condition, body);
	}

	
	public static Statement createIf(Expression<Boolean> condition, Statement ifBody, Statement elseBody, SourceLocation sourceLocation) {
		return new StatementIf(condition,ifBody,elseBody,sourceLocation);
	}

	
	public static Statement createBreak(SourceLocation sourceLocation) {
		return new StatementBreak();
	}

	
	public static Statement createPrint(Expression<? extends Type> value, SourceLocation sourceLocation) {
		return new StatementPrint(value);
	}

	
	public static Statement createSequence(List<Statement> statements, SourceLocation sourceLocation) {
		return new StatementSequence(statements);
	}

	
	public static Statement createMoveTo(Expression<PositionTask> position, SourceLocation sourceLocation) {
		return new StatementMoveTo(position);
	}

	
	public static Statement createWork(Expression<PositionTask> position, SourceLocation sourceLocation) {
		return new StatementWork(position);
	}

	
	public static Statement createFollow(Expression<UnitTask> unit, SourceLocation sourceLocation) {
		return new StatementFollow(unit);
	}

	
	public static Statement createAttack(Expression<UnitTask> unitTarget, SourceLocation sourceLocation) {
		return new StatementAttack(unitTarget);
	}

}
