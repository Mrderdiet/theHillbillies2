package hillbillies.tasks.statements;


import hillbillies.exceptions.BreakStatementException;
import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;

public interface Statement {

	public abstract boolean isExecuted();

	public abstract void reset();

	public abstract boolean isWellFormed(boolean insideWhile);	

	public abstract int execute(Task self, int remaining) throws NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions;


}