package hillbillies.tasks.statements;


import hillbillies.model.Task;


import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.Type;

public class StatementAssignment<R extends Type> implements Statement {

	private String variable;
	private Expression<R> value;
	private boolean executed;
	
	public StatementAssignment(String variable, Expression<R> value) {
		this.variable=variable;
		this.value=value;
	}
	
	@Override
	public boolean isExecuted() {
		return this.executed;
	}

	public void setExecuted(boolean b){
		this.executed=b;
	}
	
	public String getVariableName() {
		return variable;
	}

	public Expression<R> getValue() {
		return value;
	}

	@Override
	public void reset() {
		this.setExecuted(false);
		
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		// Assignments are always well formed
		return true;
	}

	@Override

	public int execute(Task self, int remaining) {
		self.addVariable(this.getVariableName(), this.getValue());
		this.setExecuted(true);
		return remaining;	
	}

}
