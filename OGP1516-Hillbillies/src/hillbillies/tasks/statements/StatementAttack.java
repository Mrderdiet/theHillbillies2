package hillbillies.tasks.statements;

import hillbillies.exceptions.*;
import hillbillies.model.Task;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.UnitTask;

public class StatementAttack implements Statement {


	private final Expression<UnitTask> unitTarget;
	private boolean executed;

	
	public StatementAttack(Expression<UnitTask> unitTarget){
		this.unitTarget=unitTarget;
		this.executed=false;
	}
	
	@Override
	public boolean isExecuted() {
		return executed;
	}
	
	public void setExecuted(boolean b){
		this.executed=b;
	}

	@Override
	public void reset() {
		this.setExecuted(false);
	}

	public Expression<UnitTask> getUnitTarget(){
		return unitTarget;
	}
	
	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return true;
	}

	@Override
	public int execute(Task self,int remaining) throws NoMoreExpressionException {
		if( remaining ==0)
			return remaining;
		try {
			self.getAssignedUnit().startAttacking(this.getUnitTarget().evaluate(self).getValue());
			remaining = 0;
			this.setExecuted(true);

		} catch (IllegalArgumentExceptions | IllegalPositionException e) {
			this.reset();
			}
		return remaining;
	}
}
