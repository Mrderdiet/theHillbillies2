package hillbillies.tasks.statements;


import hillbillies.exceptions.BreakStatementException;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;

public class StatementBreak implements Statement {

	private boolean executed;
	
	public StatementBreak() {
		this.executed=false;
	}
	
	public void setExecuted(boolean b){
		this.executed=b;
	}
	
	@Override
	public boolean isExecuted() {
		return this.executed;
	}

	@Override
	public void reset() {
		this.setExecuted(false);
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return insideWhile;
	}

	//TODO zorg dat die while loop kan breken
	@Override

	public int execute(Task self, int remaining) throws NoMoreExpressionException, BreakStatementException {
		
		this.setExecuted(true);
		throw new BreakStatementException("You are broken");
	}
}
