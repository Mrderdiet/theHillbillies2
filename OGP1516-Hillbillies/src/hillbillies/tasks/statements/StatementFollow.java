package hillbillies.tasks.statements;

import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.helpers.Position;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.UnitTask;

public class StatementFollow implements Statement {

	private final Expression<UnitTask> unitTarget;
	private boolean execute;
	
	public StatementFollow(Expression<UnitTask> unitTarget) {
		this.unitTarget=unitTarget;
		this.execute=false;
	}
	
	public void setExecute(boolean b){
		this.execute=b;
	}
	
	@Override
	public boolean isExecuted() {
		return execute;
	}

	public Expression<UnitTask> getUnitTarget(){
		return unitTarget;
	}
	
	@Override
	public void reset() {
		this.setExecute(false);
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return true;
	}

	//TODO nakijken
	@Override
	public int execute(Task self, int remaining) throws NoMoreExpressionException, IllegalArgumentExceptions {
		if (remaining == 0)
			return remaining;
		
		if (!this.isExecuted()){
			this.setExecute(true);
			remaining--;
		}
		
		Position positionTarget = this.getUnitTarget().evaluate(self).getValue().getExactPosition();
		Unit unitTarget = this.getUnitTarget().evaluate(self).getValue();
		boolean hasTarget = false;
		
		while(self.getAssignedUnit().isAlive() &&
				!self.getAssignedUnit().areNeighbor(self.getAssignedUnit().getCubeCoordinate(), positionTarget.getRoundPosition()) &&
				unitTarget.isAlive()){
			if (!unitTarget.isMoving() && !self.getAssignedUnit().isMoving()){
				self.getAssignedUnit().moveTo(unitTarget.getCubeCoordinate());
				hasTarget=true;
			}
			else if (!unitTarget.isMoving() && self.getAssignedUnit().isMoving() && hasTarget==true){
				continue;
			}
			else if (unitTarget.isMoving() && !self.getAssignedUnit().isMoving() && hasTarget==false){
				self.getAssignedUnit().moveTo(unitTarget.getCubeCoordinate());
				hasTarget=true;
			}
			else if (unitTarget.isMoving() && self.getAssignedUnit().isMoving()){
				self.getAssignedUnit().moveTo(unitTarget.getCubeCoordinate());
				hasTarget=true;
			}
		}
		
		return remaining;
	}

}
