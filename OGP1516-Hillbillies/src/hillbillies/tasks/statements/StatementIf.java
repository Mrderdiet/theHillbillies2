package hillbillies.tasks.statements;

import hillbillies.exceptions.BreakStatementException;
import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.Boolean;

public class StatementIf implements Statement {
	
	private final Statement ifBody;
	private final Statement elseBody;
	private final Expression<Boolean> condition;
	private boolean conditionState;
	private boolean executed;
	
	public StatementIf(Expression<Boolean> condition, Statement ifBody, Statement elseBody, SourceLocation sourceLocation) {
		this.ifBody = ifBody;
		this.elseBody = elseBody;
		this.condition = condition;
	}
	
	public Expression<Boolean> getCondition(){
		return condition;
	}
	
	/**
	 * Set the boolean value of the condition.
	 * @param conditionState | The value attached to the condition
	 */
	public void setConditionState(boolean conditionState){
		this.conditionState = conditionState;
	}

	/**
	 * Return the state of the condition.
	 * @return |True if conditionState==true
	 * 		   |False otherwise;
	 */
	public boolean isConditionExecuted(){
		return this.conditionState;
	}
	
	public Statement getIfBody(){
		return this.ifBody;
	}
	
	public Statement getElseBody(){
		return this.elseBody;
	}
	
	@Override
	public boolean isExecuted() {
		return this.executed;
	}
	
	public void setExecuted(boolean state){
		this.executed=state;
	}
	
	@Override
	public void reset() {
		this.setExecuted(false);
		this.setConditionState(false);
		this.getIfBody().reset();
		if(this.getElseBody()!=null)
			this.getElseBody().reset();
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		if(this.getElseBody()!=null)
			return this.getIfBody().isWellFormed(insideWhile)&& this.getElseBody().isWellFormed(insideWhile);
		else
			return this.getIfBody().isWellFormed(insideWhile);
	}

	@Override
	public int execute(Task self, int remaining) throws NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions {



		//If the condition is false we indicate that the condition already has been checked.
		if (!this.isConditionExecuted()){
			remaining--;
			this.setConditionState(true);
		}
		
		if (this.getCondition().evaluate(self).getValue()) {
			if (!this.getIfBody().isExecuted()) {
				remaining = this.getIfBody().execute(self, remaining);
				
			}
		}
		
		else if (this.getElseBody()!= null &&!this.getElseBody().isExecuted()) {
			remaining = this.getIfBody().execute(self,remaining);
		}

		if(remaining==0){
			return 0;
		}
		this.setExecuted(true);
		return remaining;	

	}
}
