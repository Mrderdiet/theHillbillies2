package hillbillies.tasks.statements;

import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.PositionTask;


public class StatementMoveTo implements Statement {

	private final Expression<PositionTask> position;
	private boolean executed=false;
	
	public StatementMoveTo(Expression<PositionTask> position){
		this.position=position;
	}
	
	private void setExecuted(boolean b) {
		this.executed=b;
	}
	
	@Override
	public boolean isExecuted() {
		return this.executed;
	}
	
	public Expression<PositionTask> getPosition(){
		return position;
	}
	
	@Override
	public void reset() {
		this.setExecuted(false);
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return true;
	}

	@Override
	public int execute(Task self,int remaining) throws NoMoreExpressionException {
		if(remaining == 0)
			return 0;
		try {
			self.getAssignedUnit().moveTo(this.position.evaluate(self).getValue().getRoundPosition());
			remaining=0;
			this.setExecuted(true);
		} catch (IllegalArgumentExceptions e) {
			this.reset();
			e.printStackTrace();
		}
		return remaining;
	}

}
