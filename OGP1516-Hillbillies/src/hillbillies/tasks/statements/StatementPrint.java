package hillbillies.tasks.statements;

import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.Type;

public class StatementPrint implements Statement {

	private final Expression<? extends Type> printExpression;
	private boolean executed;
	
	public StatementPrint(Expression<? extends Type> printExpression) {
		this.printExpression=printExpression;
		this.executed=false;
	}
	
	public Expression<? extends Type> getPrintExpression(){
		return printExpression;
	}
	
	@Override
	public boolean isExecuted() {
		return executed;
	}

	public void setExecuted(boolean b){
		this.executed=b;
	}
	
	@Override
	public void reset() {
		this.setExecuted(false);
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		// print statement false is impossible.
		return true;
	}

	@Override
	public int execute(Task self, int remaining) throws NoMoreExpressionException {
		System.out.println(this.getPrintExpression().evaluate(self));
		return remaining--;
	}

}
