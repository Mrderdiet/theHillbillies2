package hillbillies.tasks.statements;


import java.util.Iterator;
import java.util.List;

import hillbillies.exceptions.BreakStatementException;
import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;


public class StatementSequence implements Statement {

	private boolean executed;
	private final List<Statement> statements;
	private Iterator<Statement> iterator;
	private Statement currentStatement;
	
	public StatementSequence(List<Statement> statements) {
		this.executed=false;
		this.statements=statements;
		this.currentStatement=null;
		this.iterator= statements.iterator();
	}
	
	public Statement getCurrentStatement() {
		return currentStatement;
	}

	public void setCurrentStatement(Statement currentStatement) {
		this.currentStatement = currentStatement;
	}

	public Iterator<Statement> getIterator() {
		return iterator;
	}

	public void setIterator(Iterator<Statement> iterator) {
		this.iterator = iterator;
	}

	public void setExecuted(boolean b){
		this.executed=b;
	}
	
	@Override
	public boolean isExecuted() {
		return this.executed;
	}

	public List<Statement> getStatements(){
		return this.statements;
	}
	
	@Override
	public void reset() {
		this.setExecuted(false);
		this.setCurrentStatement(null);
		this.setIterator(this.getStatements().iterator());
		this.getStatements().stream().forEach(e -> e.reset());
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return this.getStatements().stream().allMatch(s->s.isWellFormed(insideWhile));
	}

	@Override
	public int execute(Task self, int remaining) throws NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions {
		while(this.getIterator().hasNext() || !this.getCurrentStatement().isExecuted()){
			if (this.getCurrentStatement() == null || this.getCurrentStatement().isExecuted()){
				this.setCurrentStatement(this.getIterator().next());
			}
			remaining = this.getCurrentStatement().execute(self,remaining);
			if (remaining == 0){
				return 0;
			}
		}
		this.setExecuted(true);
		return remaining;
	}	

}
