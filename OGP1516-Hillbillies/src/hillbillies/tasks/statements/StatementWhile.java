package hillbillies.tasks.statements;


import hillbillies.exceptions.BreakStatementException;
import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.Boolean;

public class StatementWhile implements Statement{

	private final Statement body;
	private final Expression<Boolean> condition;
	private boolean executed;
	private boolean conditionState;
	private boolean run;
	
	public StatementWhile(Expression<Boolean> condition, Statement body) {
		this.body = body;
		this.condition = condition;
		this.executed=false;
		this.conditionState=false;
		this.run=false;
	}
	
	public Statement getBody(){
		return body;
	}
	
	public void setExecuted(boolean b){
		this.executed=b;
	}
	
	@Override
	public boolean isExecuted() {
		return executed;
	}
	
	public Expression<Boolean> getCondition(){
		return condition;
	}
	
	public boolean isConditionExecuted(){
		return conditionState;
	}
	
	public void setConditonState(boolean b){
		this.conditionState=b;
	}
	
	public boolean isRunning(){
		return this.run;
	}
	
	public void setRunning(boolean b){
		this.run=b;
	}
	
	@Override
	public void reset() {
		this.setExecuted(false);
		this.getBody().reset();
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return this.getBody().isWellFormed(true);
	}


	@Override

	public int execute(Task self, int remaining) throws NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions {

		

		/*
		 * Explanation over the condition in the while
		 * this.getCondition.eval -> must be true to get in the while loop;
		 * this.run==true -> if the while is already running 
		 * this.execute==true -> the while loop cannot be executed if he wants to make an other iteration.
		 */
		while(this.getCondition().evaluate(self).getValue()
				|| this.isRunning() && !this.isExecuted()){

			if (!this.isConditionExecuted()){
				remaining--;

				this.setConditonState(true);
			}
			
			this.setRunning(true);
			remaining=this.getBody().execute(self, remaining);
			if(remaining == 0){
				this.getBody().reset();
				return 0;
				}
			//Wet set the run to false because the while-loop must continue to run after this iteration.
			this.setRunning(false);

			//Once the body is done, we must reset him in order to reuse him in the next iteration.
			if (this.getBody().isExecuted()){
				this.getBody().reset();
			}
			
			//The condition must be set to false because it will be analysed one more time (one more expression).
			this.setConditonState(false);
		}
		this.setExecuted(true);
		//expressionRemaining-1 because the condition is analysed one more time before it is rejected.
		return remaining --;

	}	
	
}