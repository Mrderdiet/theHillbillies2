package hillbillies.tasks.statements;


import hillbillies.exceptions.NoMoreExpressionException;
import hillbillies.model.Task;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.type.PositionTask;

public class StatementWork implements Statement {
	

	private final Expression<PositionTask> position;
	private boolean executed;
	
	public StatementWork(Expression<PositionTask> position) {
		this.position=position;
		this.executed=false;
	}
	
	public Expression<PositionTask> getPosition(){
		return position;
	}
	
	public void setExecuted(boolean b){
		this.executed=b;
	}
	
	@Override
	public boolean isExecuted() {
		return executed;
	}

	@Override
	public void reset() {
		this.setExecuted(false);
	}

	@Override
	public boolean isWellFormed(boolean insideWhile) {
		return true;
	}

	@Override
	public int execute(Task self, int remaining) throws NoMoreExpressionException {
		
		if(remaining == 0)
			return 0;
		if (!this.isExecuted()){
			self.getAssignedUnit().workAt(this.getPosition().evaluate(self).getValue().getRoundPosition()[0], 
					this.getPosition().evaluate(self).getValue().getRoundPosition()[1], 
					this.getPosition().evaluate(self).getValue().getRoundPosition()[2]);
			remaining=0;
			this.setExecuted(true);
		}
		return remaining;
	}

}
