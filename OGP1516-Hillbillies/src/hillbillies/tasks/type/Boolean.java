package hillbillies.tasks.type;

public class Boolean implements Type {

	private final boolean value;
	
	public Boolean() {
		this.value = false;
	}
	public Boolean(boolean flag) {
		this.value = flag;
	}
	
	public boolean getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.getValue());
	}
	
	/* UNARY OPERATIONS */
	public Boolean not() {
		return new Boolean(!this.getValue());
	}
	
	/* BINARY OPERATIONS */
	public Boolean and(Boolean other) {
		return new Boolean(this.getValue() && other.getValue());
	}

	public Boolean or(Boolean other) {
		return new Boolean(this.getValue() || other.getValue());
	}
	
	@Override
	public Boolean equals(Type other) {
		if (!(other instanceof Boolean)) 
			return new Boolean(false);
		return new Boolean(this.getValue() == ((Boolean) other).getValue());
	}

}
