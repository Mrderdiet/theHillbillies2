package hillbillies.tasks.type;

import hillbillies.helpers.Position;

public class PositionTask implements Type {
	
	private Position value;
	
	public PositionTask() {
		this.value = null;
	}
	
	public PositionTask(Position value) {
		this.value = value;
	}
	
	public Position getValue() {
		return this.value;
	}
	
	@Override
	public Boolean equals(Type other) {
		if (other==null)
			throw new NullPointerException();
		if (!(other instanceof PositionTask))
				return new Boolean(false);
		return new Boolean(this.getValue().equals(((PositionTask) other).getValue()));
	}
	
	

}
