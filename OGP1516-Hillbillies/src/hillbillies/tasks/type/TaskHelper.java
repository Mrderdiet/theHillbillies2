package hillbillies.tasks.type;


import java.util.Set;

import hillbillies.exceptions.IllegalPositionException;
import hillbillies.helpers.Position;
import hillbillies.model.*;
import hillbillies.tasks.expressions.Expression;

public class TaskHelper implements Type {

	
	private Task task;
	
	public TaskHelper(Task self) {
		this.task = self;
	}
	
	public Task getTask(){
		return this.task;
	}
	
	public Unit getValue(){
		return this.task.getAssignedUnit();
	}
	
	
	
	
	
	
	/* Positions */
	
	public PositionTask getPosition(){
		return new PositionTask(this.getValue().getExactPosition().copy());
	}
	
	public PositionTask getLogPosition(){
		Set<Log> logs = this.getValue().getWorld().getLogs();
		double distance = Double.POSITIVE_INFINITY;
		PositionTask result = new PositionTask();
		for (Log l:logs){
			double testdist = this.getValue().getLenghtPath(l.getCubePosition());
			if (testdist< distance){
				result = new PositionTask(l.getExactPosition().copy());
				distance = testdist;
				}
		}
		return result;	
	}
	
	public PositionTask getBoulderPosition(){
		Set<Boulder> boulders = this.getValue().getWorld().getBoulders();
		double distance = Double.POSITIVE_INFINITY;
		PositionTask result = new PositionTask();
		for (Boulder b:boulders){
			double testdist = this.getValue().getLenghtPath(b.getCubePosition());
			if (testdist< distance){
				result = new PositionTask(b.getExactPosition().copy());
				distance = testdist;
				}
		}
		return result;	
	}
	public PositionTask getWorkshop(){
		Set<Position> workshops = this.getValue().getWorld().getWorkshops();
		double distance = Double.POSITIVE_INFINITY;
		PositionTask result = new PositionTask();
		for (Position p:workshops){
			double testdist = this.getValue().getLenghtPath(p.getRoundPosition());
			if (testdist < distance){
				result = new PositionTask(p.copy());
				distance = testdist;
				}
		}
		return result;	
	}
	
	public PositionTask SelectedPosition(){
		try {
			return new PositionTask(new Position(this.getTask().getSelectedCube(), this.getValue().getWorld().getWorldBoudaries()));
		} catch (IllegalPositionException e) {
			return new PositionTask();
		}
	}
	
	public PositionTask LiteralPosition(int x, int y , int z){
		try {
			return new PositionTask(new Position(new int[] {x,y,z}, this.getValue().getWorld().getWorldBoudaries()));
		} catch (IllegalPositionException e) {
			return new PositionTask();
		}
	}
	
	// TODO
	public PositionTask NextToPosition(Expression<PositionTask> position){
		int[] cube =  ((PositionTask) position.evaluate(this.getTask())).getValue().getRoundPosition();
		PositionTask result = new PositionTask();
		int[] unitsCor = this.getValue().getExactPosition().getRoundPosition();
		double dist = Double.POSITIVE_INFINITY;
		for (int i=-1;i<2;i++){
			for (int j=-1;j<2;j++){
				for (int k=-1;k<2;k++){
					int x = i+cube[0];
					int y = j+cube[1];
					int z = k+cube[2];
					try {
						Position testPosition = new Position(new int[] {x,y,z}, this.getValue().getWorld().getWorldBoudaries());
						if(this.getValue().hasSolidNeighbor(x, y, z)&& !this.getValue().getWorld().isSolid(x, y, z)){
							double testdist = Math.pow(x-unitsCor[0], 2)+ Math.pow(y-unitsCor[1], 2)+Math.pow(z-unitsCor[2], 2);
							if(testdist<dist){
								result = new PositionTask(testPosition);
								dist = testdist;
							}		
					}
					} catch (IllegalPositionException e) {
					}	
				}
			}
		}
		return result;
	}
	
	public PositionTask PositionOf(Expression<UnitTask> e){
			if (!(((UnitTask) e.evaluate(this.getTask())).getValue() instanceof Unit))
				throw new NullPointerException();
			Unit u  =  ((UnitTask) e.evaluate(this.getTask())).getValue(); 
			return new PositionTask(u.getExactPosition().copy());
		
	}
	
	/* Units */
	
	public UnitTask getThis(){
		return new UnitTask(this.getValue());
	}
	
	public UnitTask getFriend(){
		Set<Unit> units = this.getValue().getFaction().getUnits();
		double distance = Double.POSITIVE_INFINITY;
		UnitTask result = new UnitTask();
		for (Unit u:units){
			double testdist = this.getValue().getLenghtPath(u.getCubeCoordinate());
			if ((u!=this.getValue()) &&(testdist< distance)){
				result = new UnitTask(u);
				distance = testdist;
				}
		}
		return result;
	}

	public UnitTask getEnemy(){
		Set<Unit> units = this.getValue().getWorld().getUnits();
		double distance = Double.POSITIVE_INFINITY;
		UnitTask result = new UnitTask();
		for (Unit u:units){
			double testdist = this.getValue().getLenghtPath(u.getCubeCoordinate());
			if ((u.getFaction()!=this.getValue().getFaction()) &&(testdist< distance)){
				result = new UnitTask(u);
				distance = testdist;
				}
		}
		return result;
	}
	
	public UnitTask getAny(){
		Set<Unit> units = this.getValue().getWorld().getUnits();
		double distance = Double.POSITIVE_INFINITY;
		UnitTask result = new UnitTask();
		for (Unit u:units){
			double testdist = this.getValue().getLenghtPath(u.getCubeCoordinate());
			if ((u!=this.getValue()) &&(testdist< distance)){
				result = new UnitTask(u);
				distance = testdist;
				}
		}
		return result;
	}
	
	
	/* Booleans */
	
	public Boolean isSolid(Expression<PositionTask> position){
		if (!(position.evaluate(this.getTask()).getValue() instanceof Position))
			throw new NullPointerException();
		int[] cube =  position.evaluate(this.getTask()).getValue().getRoundPosition();
		return new Boolean(this.getValue().getWorld().isSolid(cube[0], cube[1], cube[2])) ;	
	}
	
	public Boolean isPassable(Expression<PositionTask> position){
		if (!(position.evaluate(this.getTask()).getValue() instanceof Position))
			throw new NullPointerException();
		int[] cube =  position.evaluate(this.getTask()).getValue().getRoundPosition();
		return new Boolean(!this.getValue().getWorld().isSolid(cube[0], cube[1], cube[2])) ;	
	}
	
	public Boolean isFriend(Expression<UnitTask> e){
		Unit u  = e.evaluate(this.getTask()).getValue(); 
		return new Boolean(this.getValue().getFaction()==u.getFaction()) ;	
	}
	
	public Boolean isEnemy(Expression<UnitTask> e){
		Unit u  = e.evaluate(this.getTask()).getValue();
		return new Boolean(this.getValue().getFaction()!=u.getFaction()) ;	
	}
	
	public Boolean isAlive(Expression<UnitTask> e){
		Unit u  = e.evaluate(this.getTask()).getValue(); 
		return new Boolean(u.isAlive()) ;	
	}
	
	public Boolean isCarring(Expression<UnitTask> e){
		Unit u  = e.evaluate(this.getTask()).getValue(); 
		return new Boolean((u.isCarryingBoulder() || u.isCarryingLog())) ;	
	}
		
	
	@Override
	public Boolean equals(Type other) {
		if (other==null)
			throw new NullPointerException();
		if (!(other instanceof TaskHelper))
				return new Boolean(false);
		return new Boolean(this.getValue().equals(((PositionTask) other).getValue()));
	}

}
