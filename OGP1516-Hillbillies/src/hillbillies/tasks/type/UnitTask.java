package hillbillies.tasks.type;

import hillbillies.model.Unit;

public class UnitTask implements Type {

	private Unit value;
	
	public UnitTask() {
		this.value = null;
	}
	
	public UnitTask(Unit value) {
		this.value = value;
	}
	
	public Unit getValue() {
		return this.value;
	}
	
	
	@Override
	public Boolean equals(Type other) {
		if (!(other instanceof UnitTask))
			return new Boolean();
		if (((UnitTask) other).getValue()==this.getValue()){
			return new Boolean(true);
		}
		return new Boolean();
	}

}
