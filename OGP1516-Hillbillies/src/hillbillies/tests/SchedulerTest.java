package hillbillies.tests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import hillbillies.model.Scheduler;
import hillbillies.model.Task;
import hillbillies.tests.expressions.StatementExpressionsTest;

public class SchedulerTest {
	
	
	private Task makeTask(String name,int priority){
		StatementExpressionsTest statement = new StatementExpressionsTest();
		Task task = new Task(name,priority,statement,null);
		return task;
	}
	
	private Scheduler scheduler = new Scheduler();
	
	/**
	 * test add, remove, replace 
	 */
	@Test
	public final void testAddTask(){
		Set<Task> tasks = new HashSet<Task>();
		Task t = this.makeTask("task1", 50);
		tasks.add(t);
		this.scheduler.schedule(t);
		assertTrue(this.scheduler.areTasksPartOf(tasks));
		Task t2 = this.makeTask("task2", 70);
		this.scheduler.schedule(t2);
		tasks.add(t2);
		assertTrue(this.scheduler.areTasksPartOf(tasks));
		Task t3 = this.makeTask("task3", 51);
		this.scheduler.schedule(t3);
		tasks.add(t3);
		assertTrue(this.scheduler.areTasksPartOf(tasks));
		this.scheduler.removeTask(t2);
		assertFalse(this.scheduler.areTasksPartOf(tasks));
		tasks.remove(t2);
		assertTrue(this.scheduler.areTasksPartOf(tasks));
		tasks.remove(t);
		this.scheduler.replace(t3, t2);
		assertFalse(this.scheduler.areTasksPartOf(tasks));
		this.scheduler.removeTask(t);
		this.scheduler.removeTask(t2);
	}
	
	/**
	 * Test getTask()
	 */
	@Test
	public final void testGetTask(){
		for(int i=1; i<11; i++){
            this.scheduler.schedule(this.makeTask("task"+i, i));
        }
		for (int i=10; i>0; i--){
			Task t = this.scheduler.getTask();
			assertTrue(t.getPriority()==i);
			this.scheduler.removeTask(t);
		}
		assertTrue(this.scheduler.getTask()==null);
	}
	
	/**
	 * Test Iterator()
	 */
	@Test
	public final void testIterator(){
		for(int i=1; i<10000; i++){
            this.scheduler.schedule(this.makeTask("task"+i, i));
        }
		Iterator<Task> iterator = this.scheduler.iterator();
		int priority = 10001;
		while (iterator.hasNext()){
			int taskPriority = iterator.next().getPriority();
			assertTrue(taskPriority<priority);
			priority = taskPriority;
		}
	}
}
	

