package hillbillies.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import hillbillies.exceptions.*;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.tests.expressions.StatementExpressionsTest;

public class TaskTest {
	
	/**
	 * Unit
	 */
	@Test
	public final void testUnit() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException{
		Task t  = new Task("test", 5, new StatementExpressionsTest(), null);
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false);
		assertTrue(t.getAssignedUnit()==null);
		t.assignTo(unit);
		assertTrue(t.getAssignedUnit().equals(unit));
	}
	
	@Test
	public final void testGetSelectedNamePriority(){
		Task t  = new Task("test", 5, new StatementExpressionsTest(),new int[] {1,2,3});
		assertTrue(t.getSelectedCube()[0]==1);
		assertTrue(t.getSelectedCube()[1]==2);
		assertTrue(t.getSelectedCube()[2]==3);
		assertTrue(t.getName()=="test");
		assertTrue(t.getPriority()==5);
	}
}
