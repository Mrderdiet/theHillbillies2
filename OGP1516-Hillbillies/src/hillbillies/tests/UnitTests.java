package hillbillies.tests;

import static hillbillies.tests.util.PositionAsserts.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;

import hillbillies.exceptions.*;
import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import ogp.framework.util.Util;


public class UnitTests {
	
	//private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	
	/* Test survability */
	
	/**
	 * Test the method isAlive()
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testIsAlive() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		assertEquals(true, unit.isAlive());
		unit.setCurrentHitPoints(-1);
		assertEquals(false,unit.isAlive());
	}
	
	/**
	 * Test the method terminate()
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalWorldException 
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testTerminate() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalWorldException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,1}, 50, 50, 100, 50, false);
		int[][][] types = new int[3][3][3];
		types[1][1][1] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		types[0][0][0] = TYPE_ROCK;
		World world = new World(types, new DefaultTerrainChangeListener());
		world.addUnit(unit);
		System.out.println("fdp");
		unit.workAt(1, 1, 1);
		advanceTimeFor(world,10, 0.2);
		System.out.println("fdp");
		assertEquals(0, world.getCubeType(1, 1, 1));
		unit.workAt(1, 1, 0);
		advanceTimeFor(world, 10, 0.2);
		System.out.println("fdp fin");
		assertEquals(true, unit.isCarryingBoulder());
		assertEquals(false, unit.isCarryingLog());
		System.out.println("fdp fin");
		unit.setCurrentHitPoints(-1);
		unit.terminate();
		assertEquals(false, unit.isCarryingBoulder());
		assertEquals(false, unit.isCarryingLog());
		
	}
	
	/* World */
	
	/**
	 * Test the method getWorld() & setWorld(World world)
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalUnitExceptions 
	 */
	@Test
	public void testSetWorld() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,1}, 50, 50, 100, 50, false);
		int[][][] types = new int[3][3][3];
		types[1][1][1] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		types[0][0][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		World world2 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		assertEquals(world1, unit.getWorld());
		unit.setWorld(world2);
		assertEquals(world2, unit.getWorld());
	}
	
	/* Factions */
	
	/**
	 * Test the setter & getter of unit's faction.
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalUnitExceptions 
	 */
	@Test
	public void testFaction() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,1}, 50, 50, 100, 50, false);
		int[][][] types = new int[3][3][3];
		types[1][1][1] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		types[0][0][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		
		world1.addUnit(unit);
		assertEquals("House Baratheon", unit.getFaction().toString());
		unit.setFaction(Faction.House_Lannister);
		assertEquals("House Lannister", unit.getFaction().toString());
		unit.setFaction(0);
		assertEquals("House Baratheon", unit.getFaction().toString());
	}
	/* Test neighbor cube */
	
	/**
	 * Test the method areNeighbor(int[] cube1,int[] cube2)
	 * @param cube1
	 * @param cube2
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testAreNeighbor() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		assertEquals(true, unit.areNeighbor(unit.getCubeCoordinate(), new int[] { 1, 6, 1 }));
	}
	
	/* Test Position */

	/**
	 * Test the getPosition()
	 * @throws IllegalTerraintypeException 
	 */
	@Test
	public void testGetPosition() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		assertDoublePositionEquals("Position must be the center of the cube", 1.5, 6.5, 1.5, unit.getPosition());
	}

	/**
	 * Test the getCubeCoordinate()
	 * @throws IllegalPositionException 
	 */
	@Test
	public void getCubeCoordinate() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		assertIntegerPositionEquals("A valid position should be accepted", 1, 6, 1, unit.getCubeCoordinate());
	}

	/* Test name */
	
	/**
	 * Test getName()
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testGetName() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException{
		Unit unit = new Unit("TestUnit", new int[] { 1, 1, 1 }, 50, 50, 50, 50, false);
		assertEquals("This should be a valid name", "TestUnit", unit.getName());
	}
	/**
	 * Test renaming with valid name
	 */
	@Test
	public void testSetValidName() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException,IllegalNameException {
		Unit unit = new Unit("TestUnit", new int[] { 1, 1, 1 }, 50, 50, 50, 50, false);
		unit.setName("Paolo \"Dieter ' \" O'Hare the first");
		assertEquals("This should be a valid name", "Paolo \"Dieter ' \" O'Hare the first", unit.getName());
	}

	/**
	 * Test renaming with invalid name (without capital)
	 */
	@Test
	public void testSetNameWithoutCapital() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("TestUnit", new int[] { 1, 1, 1 }, 50, 50, 50, 50, false);
		try {
			unit.setName("dieter");
		} catch (IllegalNameException e) {
			
		}
		assertEquals("This name is invalid because it doesn't start with a capital", "TestUnit", unit.getName());
	}

	/**
	 * Test renaming with invalid name (illegal characters)
	 * @throws IllegalNameException 
	 */
	@Test
	public void testSetNameInvalidCharacters() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("TestUnit", new int[] { 1, 1, 1 }, 50, 50, 50, 50, false);
		try {
			unit.setName("dieter123");
		} catch (IllegalNameException e) {
		}
		assertEquals("This name is invalid because it contains illegal charachters ", "TestUnit", unit.getName());
	}

	/**
	 * Test renaming with invalid name (too short)
	 * @throws IllegalNameException 
	 */
	@Test
	public void testSetNameTooShort() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("TestUnit", new int[] { 1, 1, 1 }, 50, 50, 50, 50, false);
		try {
			unit.setName("A");
		} catch (IllegalNameException e) {
		}
		assertEquals("This name is invalid because it is too short", "TestUnit", unit.getName());
	}

	/* Test Primary Attributes */
	/**
	 * Test setting weight,agility,strength and toughness
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testGetAndSetPrimaryAttributes() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.setWeight(100);
		assertEquals(100, unit.getWeight());
		unit.setAgility(100);
		assertEquals(100, unit.getAgility());
		unit.setStrength(100);
		assertEquals(100, unit.getStrength());
		unit.setToughness(100);
		assertEquals(100, unit.getToughness()); 
	}

	/**
	 * Test create unit with invalid values
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testPrimaryAttributesGeneration() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 200, 200, 200, 200, false);
		assertEquals(100, unit.getWeight());
		assertEquals(100, unit.getAgility());
		assertEquals(100, unit.getStrength());
		assertEquals(100, unit.getToughness());
	}

	/**
	 * Test set values too high and too low
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testPrimaryAttributesInvalidValues() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.setWeight(-100);
		assertEquals(50, unit.getWeight());
		unit.setWeight(1000);
		assertEquals(200, unit.getWeight());
		unit.setAgility(-100);
		assertEquals(1, unit.getAgility());
		unit.setAgility(1000);
		assertEquals(200, unit.getAgility());
		unit.setStrength(-100);
		assertEquals(1, unit.getStrength());
		unit.setStrength(1000);
		assertEquals(200, unit.getStrength());
		unit.setToughness(-100);
		assertEquals(1, unit.getToughness());
		unit.setToughness(1000);
		assertEquals(200, unit.getToughness());
		unit.setWeight(-100);
		assertEquals(200, unit.getWeight());

	}

	/* Test Secondary Attributes */

	/**
	 * Test set valid values
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testSetSecundaryAttributesValid() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.setCurrentHitPoints(49);
		assertEquals(49, unit.getCurrentHitPoints());
		unit.setCurrentStaminaPoints(49);
		assertEquals(49, unit.getCurrentStaminaPoints());
	}

	/**
	 * Test set invalid values
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testSetSecundaryAttributesInvalid() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		
		unit.setCurrentStaminaPoints(100);
		assertEquals(50, unit.getCurrentStaminaPoints());
		unit.setCurrentStaminaPoints(-100);
		assertEquals(50, unit.getCurrentStaminaPoints());		
		unit.setCurrentHitPoints(100);
		assertEquals(50, unit.getCurrentHitPoints());
		unit.setCurrentHitPoints(-100);
		assertEquals(50, unit.getCurrentHitPoints());
	}
	
	/**
	 * Test the getMax() from Hitpoints and StaminaPoints
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testGetMaxSecundaryAttributes() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		assertEquals(50, unit.getMaxStaminaPoints());
		assertEquals(50, unit.getMaxHitPoints());
		
	}
	
	
	/**
	 * Test Regaining and consuming of stamina
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	
	@Test
	public void testConsumeRegainStaminapoints() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.consumeStamina(50);
		assertEquals(0, unit.getCurrentStaminaPoints());
		unit.regainStamina(20);
		assertEquals(20, unit.getCurrentStaminaPoints());
		unit.regainStamina(31);
		assertEquals(50, unit.getCurrentStaminaPoints());
		unit.regainStamina(30);
		assertEquals(50, unit.getCurrentStaminaPoints());
		unit.consumeStamina(100);
		assertEquals(50, unit.getCurrentStaminaPoints());
		
	}
	
	/* Orientation */
	
	/**
	 * Test the getOrientation()
	 * @throws IllegalArgumentExceptions 
 
	 */
	@Test
	public void testGetOrientation() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		assertEquals("Same Value", unit.getOrientation(), Math.PI/2, Util.DEFAULT_EPSILON);
	}
	
	/**
	 * Test the setOrientation()
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalPositionException 
	 * @throws IllegalTerraintypeException 
	 */
	@Test
	public void testsetOrientation() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.setOrientation((float) 1);
		Assert.assertEquals("Same Value", unit.getOrientation(), 1, Util.DEFAULT_EPSILON);
		unit.setOrientation((float) Math.PI);
		assertEquals("Same Value", unit.getOrientation(), Math.PI, Util.DEFAULT_EPSILON);
		unit.setOrientation((float) (3*Math.PI));
		Assert.assertEquals("Same Value", unit.getOrientation(), Math.PI, Util.DEFAULT_EPSILON);
	}
	/**
	 * Test the rotate Unit
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testRotateUnit() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.rotateUnit((float)Math.PI);
		Assert.assertEquals("Same Value", unit.getOrientation(), (Math.PI/2+Math.PI), Util.DEFAULT_EPSILON);
	}
	
	/* Basic Movement (defensive) */
	/**
     * Test getDefaultSpeed
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testGetDefaultSpeed() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,0 }, 50, 50, 50, 50, false);
		Unit init = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,0 }, 50, 50, 50, 50, false);
		Unit onit = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,3 }, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[0][0][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		world1.addUnit(init);
		world1.addUnit(onit);
		//Moving
		unit.moveToAdjacent(1, 1, 0);
		advanceTimeFor(world1, 0.2, 0.2);
		Assert.assertEquals("Same Value", unit.getDefaultSpeed(), 1.5 , Util.DEFAULT_EPSILON);
		init.setAgility(100);
		init.setStrength(60);
		init.setWeight(200);
		init.moveToAdjacent(1, 1, 0);
		advanceTimeFor(world1, 0.2, 0.2);
		Assert.assertEquals("Same Value", init.getDefaultSpeed(), 0.6 , Util.DEFAULT_EPSILON);
		//Falling
		Assert.assertEquals(-3.0, onit.getDefaultSpeed(),Util.DEFAULT_EPSILON);
		
	}
	/**
	 * test the function moveToAdjacent()
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testMoveToAdjacent() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException,IllegalActionException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		unit.moveToAdjacent(1, 1, 0);
		advanceTimeFor(unit, 5, 0.2);
		assertDoublePositionEquals(1.5, 1.5, 0.5, unit.getPosition());
		//Invalid argument
		try{
			unit.moveToAdjacent(2, 3, 0);
		}catch(IllegalArgumentExceptions e){
			
		}
		assertDoublePositionEquals(1.5, 1.5, 0.5, unit.getPosition());
		unit.moveToAdjacent(-1, -1, 0);
		advanceTimeFor(unit, 5, 0.2);
		assertDoublePositionEquals(0.5, 0.5, 0.5, unit.getPosition());
		//Cube not passable
		types[1][1][1] = TYPE_ROCK;
		World world2 = new World(types, new DefaultTerrainChangeListener());
		Unit unit2 = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		world2.addUnit(unit2);
		try{
			unit.moveToAdjacent(1, 1, 0);
		}catch (IllegalArgumentExceptions e){
			
		}
		assertDoublePositionEquals(0.5, 0.5, 0.5, unit.getPosition());
	}
	
	/**
	 * Test of the method moveTo()
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testMoveTo() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException,IllegalActionException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][4];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_TREE;
		types[1][1][2] = TYPE_TREE;
		types[1][1][3] = TYPE_TREE;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		world.addUnit(unit);
		unit.moveTo(new int[] {2,2,2});
		advanceTimeFor(world, 10, 0.05);
		assertDoublePositionEquals("Same Value",2.5, 2.5, 2.5, unit.getPosition());
		unit.moveTo(new int[] {0,0,0});
		advanceTimeFor(unit, 10, 0.05);
		assertDoublePositionEquals("Same Value",0.5, 0.5, 0.5, unit.getPosition());
		//Test Illegal Value
		try{
			unit.moveTo(new int[] {-21,49,49});
		}catch (IllegalArgumentExceptions e){
		//OK	
		}
		unit.advanceTime(0.2);
		assertDoublePositionEquals("Same Value",0.5, 0.5, 0.5, unit.getPosition());
	}
	
	
	/**
	 * tests of the method isMoving()
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 * @throws IllegalArgumentExeption
	 */
	@Test
	public void testIsMoving() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException,IllegalActionException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		unit.moveToAdjacent(1, 1, 0);
		advanceTimeFor(unit, 5, 0.2);
		Assert.assertEquals(false, unit.isMoving());
		//Unit is moving.
		unit.moveToAdjacent(1, 1, 1);
		Assert.assertEquals(true, unit.isMoving());
	}
	/**
	 * Tests of the method isSprinting(), same tests as for the method testStartSprinting.
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalUnitExceptions 
	 */
	@Test
	public void testIsSprinting() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException,IllegalActionException, IllegalUnitExceptions{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		unit.moveToAdjacent(1, 1, 0);
		Assert.assertEquals(false, unit.isSprinting());
		unit.startSprinting();
		Assert.assertEquals(true, unit.isSprinting());
		unit.stopSprinting();
		Assert.assertEquals(false, unit.isSprinting());
		//Unit run out of stamina.
		unit.setCurrentStaminaPoints(0);
		try{
			unit.startSprinting();
		}catch (IllegalArgumentExceptions e){
			
		}
		Assert.assertEquals(false, unit.isSprinting());
		
	}
	/**
	 * Tests of the method startSprinting, same test as for the method testIsSprinting.
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalUnitExceptions 
	 */
	@Test
	public void testStartSprinting() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException,IllegalActionException, IllegalUnitExceptions{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		unit.moveToAdjacent(1, 1, 0);
		Assert.assertEquals(false, unit.isSprinting());
		unit.startSprinting();
		Assert.assertEquals(true, unit.isSprinting());
		unit.stopSprinting();
		Assert.assertEquals(false, unit.isSprinting());
		//Unit run out of stamina.
		unit.setCurrentStaminaPoints(0);
		try{
			unit.startSprinting();
		}catch (IllegalArgumentExceptions e){
			
		}
		Assert.assertEquals(false, unit.isSprinting());
	}
	/**
	 * Test the method stopSrpinting().
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testStopSprinting() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException,IllegalActionException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] {0,0,0}, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world1 = new World(types, new DefaultTerrainChangeListener());
		world1.addUnit(unit);
		unit.moveToAdjacent(1, 1, 0);
		advanceTimeFor(world1, 0.1, 0.05);
		unit.startSprinting();
		unit.advanceTime(0.2);
		unit.stopSprinting();
		Assert.assertEquals(false, unit.isSprinting());
	}
	/**
	 * Test of the method isWorking().
	 * @throws IllegalArgumentExceptions
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testIsWorking() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException, NoMoreExpressionException, BreakStatementException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.startWorking();
		Assert.assertEquals(true, unit.isWorking());
		//After 500/unit.getStrengh() seconds the unit should stop working.
		advanceTimeFor(unit, 500/unit.getStrength(), 0.05);
		Assert.assertEquals(false, unit.isWorking());
		unit.setStrength(100);
		unit.startWorking();
		advanceTimeFor(unit, 500/unit.getStrength(), 0.05);
		Assert.assertEquals(false, unit.isWorking());		
	}
	/**
	 * Test the method startWorking, same tests as for the method testIsWorking.
	 * @throws IllegalArgumentExceptions
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testStartWorking() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException, NoMoreExpressionException, BreakStatementException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		unit.startWorking();
		Assert.assertEquals(true, unit.isWorking());
		//After 500/unit.getStrengh() seconds the unit should stop working.
		advanceTimeFor(unit, 500/unit.getStrength(), 0.05);
		Assert.assertEquals(false, unit.isWorking());
		unit.setStrength(100);
		unit.startWorking();
		advanceTimeFor(unit, 500/unit.getStrength(), 0.05);
		Assert.assertEquals(false, unit.isWorking());
	}
	
	/**
	 * Test the method workAt(x,y,z)
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalUnitExceptions 
	 * @throws IllegalWorldException 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testWorkAt() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions, IllegalWorldException, NoMoreExpressionException, BreakStatementException{
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		types[0][1][0] = TYPE_TREE;
		types[1][1][1] = TYPE_ROCK;
		types[0][1][1] = TYPE_TREE;
		types[1][0][1] = TYPE_WORKSHOP;
		World world =new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 1 }, 50, 50, 50, 50, false);
		world.addUnit(unit);
		unit.workAt(1, 1, 0);
		advanceTimeFor(world, 100, 0.2);
		unit.workAt(0, 1, 0);
		advanceTimeFor(world, 100, 0.2);
		assertTrue(world.getBoulderCube(1, 1, 0)!=null);
		assertTrue(world.getLogCube(0, 1, 0)!=null);
		//Carrying a boulder (same test for carrying a log)
		//int boulderWeight = world.getBoulderCube(1,1,0).getWeight();
		unit.setWeight(50);
		unit.workAt(1, 1, 0);
		advanceTimeFor(world, 100, 0.2);
		assertTrue(world.getBoulderCube(1, 1, 0)==null);
		assertEquals(true,unit.isCarryingBoulder());
		//TODO Sometimes the weight change because the unit gain attribute for every completed work. The next test is invalid.
		//assertEquals(50+boulderWeight,unit.getWeight());
		//Dropping a boulder
		unit.workAt(1, 0, 0);
		advanceTimeFor(world, 100, 0.2);
		assertTrue(world.getBoulderCube(1, 0, 0)!=null);
		assertEquals(false,unit.isCarryingBoulder());
		//TODO Idem 
		//assertEquals(50,unit.getWeight());
		//Workshop
		unit.workAt(0, 1, 0);
		advanceTimeFor(world, 100, 0.2);
		unit.workAt(0, 1, 0);
		advanceTimeFor(world, 100, 0.2);
		unit.workAt(1, 0, 0);
		advanceTimeFor(world, 100, 0.2);
		unit.workAt(1, 0, 0);
		advanceTimeFor(world, 100, 0.2);
		assertTrue(unit.getToughness()>50);
		assertTrue(unit.getWeight()>=50);
	}
	/**
	 * Test the method isResting, same tests as for the method testStartResting.
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	@Test
	public void testIsResting() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world =new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0,0 }, 50, 50, 50, 50, false);
		world.addUnit(unit);
		Assert.assertEquals(unit.isResting(), false);
		unit.moveToAdjacent(1, 1, 0);
		unit.startSprinting();
		advanceTimeFor(unit, 100, 0.05);
		unit.startResting();
		Assert.assertEquals(true, unit.isResting());
		Assert.assertNotEquals(unit.getCurrentStaminaPoints(),50);
		advanceTimeFor(unit, 100, 0.05);
		Assert.assertEquals(unit.getCurrentStaminaPoints(), 50);
    }
    /**
     * Test the method isResting, same tests as for the method testIstResting.
     * @throws IllegalArgumentExceptions
     * @throws IllegalPositionException
     * @throws IllegalUnitExceptions 
     * @throws BreakStatementException 
     * @throws NoMoreExpressionException 
     */
	@Test
	public void testStartResting() throws IllegalArgumentExceptions,IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world =new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0,0 }, 50, 50, 50, 50, false);
		world.addUnit(unit);
		Assert.assertEquals(unit.isResting(), false);
		unit.moveToAdjacent(1, 1, 0);
		unit.startSprinting();
		advanceTimeFor(unit, 100, 0.05);
		unit.startResting();
		Assert.assertEquals(true, unit.isResting());
		Assert.assertNotEquals(unit.getCurrentStaminaPoints(),50);
		advanceTimeFor(unit, 100, 0.05);
		Assert.assertEquals(unit.getCurrentStaminaPoints(), 50);
	}
	/**
	 * Test the method isDefaultBehavior, same tests as for the method testSetDefaultBehavior().
	 * @throws IllegalArgumentException
	 * @throws IllegalPositionException
	 * @throws IllegalArgumentExceptions
	 */
	@Test
	public void testIsDefaultBehavior() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException{
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		Assert.assertEquals(false, unit.isDefaultBehaviourEnabled());
		unit.setDefaultBehaviour(true);
		Assert.assertEquals(true, unit.isDefaultBehaviourEnabled());
		}
	
	/**
	 * Test the method setDefaultBehavior, same tests as for the method testIsDefaultBehavior().
	 * @throws IllegalArgumentException
	 * @throws IllegalPositionException
	 * @throws IllegalArgumentExceptions
	 */
	@Test
	public void testSetDefaultBehavior() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException {
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 50, 50, 50, false);
		Assert.assertEquals(false, unit.isDefaultBehaviourEnabled());
		unit.setDefaultBehaviour(true);
		Assert.assertEquals(true, unit.isDefaultBehaviourEnabled());
		}
	
	/**
	 * Test the method startDefaultBehavior.
	 * @throws IllegalArgumentException
	 * @throws IllegalPositionException
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalUnitExceptions 
	 * @throws BreakStatementException 
	 * @throws NoMoreExpressionException 
	 */
	//@Test
	public void testStartDefaultBehavior() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions, NoMoreExpressionException, BreakStatementException{
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world =new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0,0 }, 50, 50, 50, 50, false);
		Unit unit2 = new Unit("Test", new int[] { 0, 0,0 }, 50, 50, 50, 50, false);
		world.addUnit(unit);
		world.addUnit(unit2);
		//Showing that if the unit moves than it can sprint if default behavior is enabled.
		unit.moveToAdjacent(1, 1, 0);
		unit.setCurrentStaminaPoints(100);
		while (unit.isSprinting() != true){
			unit.startDefaultBehavior();
		}
		Assert.assertEquals(true, unit.isSprinting());
		advanceTimeFor(unit, 1000, 0.05);
		Assert.assertEquals(false, unit.isMoving());
		//Showing that unit begin a random activity if the unit is not moving.
		unit.startDefaultBehavior();
		unit.setCurrentStaminaPoints(0);
		advanceTimeFor(unit, 2, 0.05);
		if (unit.isMoving()){
			Assert.assertEquals("The unit is moving",true,unit.isMoving());
		}
		else if (unit.isWorking()){
			Assert.assertEquals("The unit is working",true,unit.isWorking());
		}
		else if (unit.isResting()){
			Assert.assertEquals("The unit is resting",true,unit.isResting());
		}
		else if (unit.isAttacking()){
			Assert.assertEquals("The unit is attacking",true,unit.isResting());
		}
		else{
			Assert.fail("startDefaultBehavior doesn't work");
		}
	}
	
	
	/**
	 * Test the method isAttacking.
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalPositionException
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalUnitExceptions 
	 */
	@Test
	public void testIsAttacking() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions {
		Unit attacker = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,0 }, 50, 50, 50, 50, false);
		Unit defender = new Unit("Paolo \" and \" Dieter", new int[] { 1,0,0 }, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world =new World(types, new DefaultTerrainChangeListener());
		world.addUnit(attacker);
		world.addUnit(defender);
		//the unit is not attacking.
		Assert.assertEquals(false, attacker.isAttacking());
		//Attacker begin an attack.
		attacker.startAttacking(defender);
		Assert.assertEquals(true, attacker.isAttacking());
	}
	/**
	 * Test the method startAttacking.
	 * @param defender
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException 
	 * @throws IllegalUnitExceptions 
	 * @throws IllegalArgumentException 
	 */
	@Test
	public void testStartAttacking() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions {
		Unit attacker = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,0 }, 50, 50, 50, 50, false);
		Unit attacker2 = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,0 }, 50, 50, 50, 50, false);
		Unit defender = new Unit("Paolo \" and \" Dieter", new int[] { 1,0,0 }, 50, 50, 50, 50, false);
		int[][][] types = new int[3][3][3];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		World world =new World(types, new DefaultTerrainChangeListener());
		world.addUnit(attacker);
		world.addUnit(defender);
		world.addUnit(attacker2);
		attacker2.setFaction(0);
		
		attacker.startAttacking(defender);
		assertTrue(attacker.isAttacking());
		assertTrue(defender.isDefending());
		//Same Faction
		try{
			attacker.startAttacking(attacker2);
		}catch (IllegalArgumentExceptions e){
			
		}
	}
	

	
	/**
	 * Test the method dodge.
	 * @throws IllegalArgumentException
	 * @throws IllegalPositionException
	 */
	@Test
	public void testDodge() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException {
		Unit attacker1 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 1, 50, 50, false);
		Unit defender1 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 7, 1 }, 50, 200, 50, 50, false);
		Unit attacker2 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 200, 50, 50, false);
		Unit defender2 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 7, 1 }, 50, 1, 50, 50, false);
		if (defender1.dodge(attacker1)==true){
			Assert.assertTrue("The unit did dodge",true);
		}
		else{
			Assert.assertFalse("The unit didn't dodge",false);
		}
		if (defender2.dodge(attacker2)==true){
			Assert.assertTrue("The unit did dodge",true);
		}
		else{
			Assert.assertFalse("The unit didn't dodge",false);
		}
		
	}
	
	/**
	 * Test the method block.
	 * @throws IllegalPositionException 
	 * @throws IllegalArgumentException 
	 */
	@Test
	public void testBlock() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException {
		Unit attacker1 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 1, 1, 50, false);
		Unit defender1 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 7, 1 }, 50, 200, 200, 50, false);
		Unit attacker2 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 6, 1 }, 50, 200, 200, 50, false);
		Unit defender2 = new Unit("Paolo \" and \" Dieter", new int[] { 1, 7, 1 }, 50, 1, 1, 50, false);
		if (defender1.block(attacker1)==true){
			Assert.assertTrue("The unit did block",true);
		}
		else{
			Assert.assertFalse("The unit didn't block",false);
		}
		if (defender2.block(attacker2)==true){
			Assert.assertTrue("The unit did block",true);
		}
		else{
			Assert.assertFalse("The unit didn't block",false);
		}
	}
	
	/**
	 * Test the method startDefending.
	 * @param attacker
	 * @throws IllegalArgumentExceptions
	 * @throws IllegalPositionException 
	 * @throws IllegalUnitExceptions 
	 * @throws IllegalArgumentException 
	 */
	@Test
	public void testStartDefending() throws IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions {
		Unit attacker = new Unit("Paolo \" and \" Dieter", new int[] { 0,0,0 }, 50, 50, 50, 50, false);
		Unit defender = new Unit("Paolo \" and \" Dieter", new int[] { 1,0,0 }, 50, 50, 50, 50, false);
		int[][][] types = new int[2][2][2];
		types[0][0][0] = TYPE_ROCK;
		types[1][1][0] = TYPE_ROCK;
		types[1][0][0] = TYPE_ROCK;
		types[0][1][0] = TYPE_ROCK;
		World world =new World(types, new DefaultTerrainChangeListener());
		world.addUnit(attacker);
		world.addUnit(defender);
		attacker.startAttacking(defender);
		assertEquals(true, defender.isDefending());
	}
	
	private static void advanceTimeFor(Unit unit, double time, double step) throws IllegalArgumentExceptions, NoMoreExpressionException, BreakStatementException {
		int n = (int) (time / step);
		for (int i = 0; i < n; i++)
			try {
				unit.advanceTime(step);
			} catch (IllegalActionException e) {
			}
		try {
			unit.advanceTime(time - n * step);
		} catch (IllegalActionException e) {
			;
		}
	}
	
	private static void advanceTimeFor(World world, double time, double step) throws IllegalArgumentExceptions, NoMoreExpressionException, BreakStatementException {
		int n = (int) (time / step);
		for (int i = 0; i < n; i++)
			try {
				world.advancedTime(step);
			} catch (IllegalWorldException e) {
			}
		try {
			world.advancedTime(time - n * step);
		} catch (IllegalWorldException e) {
			;
		}
	}
}