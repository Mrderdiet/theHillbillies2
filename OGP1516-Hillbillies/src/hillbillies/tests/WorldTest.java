package hillbillies.tests;

import org.junit.Test;
import static org.junit.Assert.*;

import hillbillies.exceptions.IllegalActionException;
import hillbillies.exceptions.IllegalArgumentExceptions;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.IllegalTerraintypeException;
import hillbillies.exceptions.IllegalWorldException;
import hillbillies.model.*;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
public class WorldTest {
	
	/**
	 * testing the world sizes
	 */
	@Test
	public void testGetWorldBoundaries() throws IllegalPositionException, IllegalTerraintypeException{
		World world = new World(new int[10][20][30], new DefaultTerrainChangeListener());
		assertEquals(10, world.getNbCubesX());
		assertEquals(20, world.getNbCubesY());
		assertEquals(30, world.getNbCubesZ());
		assertEquals(10, world.getWorldBoudaries()[0]);
		assertEquals(20, world.getWorldBoudaries()[1]);
		assertEquals(30, world.getWorldBoudaries()[2]);
	}
	
	// Manipulating the world //
	/**
	 * Test set and get cube type
	 */
	@Test
	public void testGetSetCubeType() throws IllegalPositionException, IllegalTerraintypeException{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		assertEquals(0, world.getCubeType(0, 0, 0));
		assertEquals(1, world.getCubeType(0, 0, 1));
		assertEquals(2, world.getCubeType(0, 0, 2));
		assertEquals(3, world.getCubeType(0, 0, 3));
		try {
			world.setCubeType(0, 0, 0, 1);
		} catch (IllegalActionException e) {}
		assertEquals(1, world.getCubeType(0, 0, 0));
		try {
			world.setCubeType(0, 0, 0, 8);
		} catch (IllegalActionException e) {}
		assertEquals(1, world.getCubeType(0, 0, 0));
	}
	// Units //
	
	
	/**
	 * Test SpawnUnit
	 * @throws IllegalArgumentExceptions 
	 * @throws IllegalWorldException 
	 * 
	 */
	@Test
	public void testSpawnUnit() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalWorldException{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		Unit unit =world.spawnUnit(false);
		assertEquals(true, unit.isAlive());	
	}
	
	/**
	 * Get active factions, get units in faction, get total units
	 * @throws IllegalWorldException 
	 */
	@Test
	public void testgetUnit() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalWorldException{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		for (int i=1;i<6;i++){
			Unit unit = world.spawnUnit(false);
			assertEquals(i, world.getActiveFactions().size());
			assertEquals(i, world.getUnits().size());
			assertEquals(1, world.getUnitsOfFaction(unit.getFaction()).size());
		}
	}
	
	/**
	 * Test addUnit
	 * @throws IllegalArgumentExceptions 
	 */
	@Test
	public void testAddUnit() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Paolo \" and \" Dieter", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false, world);
		assertEquals(unit.getWorld(), world);
	}
	
	/**
	 * Test limit amount of units (maximum 100 units in a world)
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * @throws IllegalWorldException 
	 */
	@Test
	public void testLimitsOfWorld() throws IllegalPositionException, IllegalTerraintypeException, IllegalWorldException{
		int[][][] terrain = new int[10][10][10] ;
		World world = new World(terrain, new DefaultTerrainChangeListener());
		for (int i=1;i<200;i++){
			try {
				world.spawnUnit(false);
			} catch (IllegalArgumentExceptions | IllegalPositionException e) {

			}
			

		}
		assertEquals(100 ,world.getUnits().size()) ;
	}
	
	
	// Boulders and Logs //
	/**
	 * Tests all the functions with boulders
	 * 1) spawn
	 * 2) remove
	 * 3) add
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * 
	 */
	@Test
	public void testSpawnBoulder() throws IllegalPositionException, IllegalTerraintypeException{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		try {
			world.spawnBoulder(new int[] {0,0,0});
			assertEquals(1,world.getBoulders().size());
		} catch (IllegalWorldException e) {
		}
		for (int i=1;i<100;i++){
			try {
				world.spawnBoulder(new int[] {0,0,0});
			} catch (IllegalWorldException e) {
			}
		}
		assertEquals(100, world.getBoulders().size());
		
		Boulder b= world.getBoulders().iterator().next();
		try {
			world.removeBoulder(b);
		} catch (IllegalWorldException e) {
		}
		assertEquals(99, world.getBoulders().size());
		assertFalse( world.getBoulders().contains(b));
		try {
			world.addBoulder(b);
		} catch (IllegalWorldException e) {
		}
		assertEquals(100, world.getBoulders().size());
		assertTrue(world.getBoulders().contains(b));
		
		try {
			Boulder boulder = new Boulder(new int[] {0,0,0}, world);
			assertEquals(101, world.getBoulders().size());
			assertTrue( world.getBoulders().contains(boulder));
		} catch (IllegalWorldException e) {
		}
		
	}
	/**
	 * Tests all the functions with logs
	 * 1) spawn
	 * 2) remove
	 * 3) add
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 * 
	 */
	@Test
	public void testSpawnLog() throws IllegalPositionException, IllegalTerraintypeException{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		try {
			world.spawnLog(new int[] {0,0,0});
			assertEquals(1,world.getLogs().size());
		} catch (IllegalWorldException e) {
		}
		for (int i=1;i<100;i++){
			try {
				world.spawnLog(new int[] {0,0,0});
			} catch (IllegalWorldException e) {
			}
		}
		assertEquals(100, world.getLogs().size());
		
		Log l= world.getLogs().iterator().next();
		try {
			world.removeLog(l);
		} catch (IllegalWorldException e) {
		}
		assertEquals(99, world.getLogs().size());
		assertFalse( world.getLogs().contains(l));
		try {
			world.addLog(l);
		} catch (IllegalWorldException e) {
		}
		assertEquals(100, world.getLogs().size());
		assertTrue( world.getLogs().contains(l));
		
		try {
			Log log = new Log(new int[] {0,0,0}, world);
			assertEquals(101, world.getLogs().size());
			assertTrue(world.getLogs().contains(log));
		} catch (IllegalWorldException e) {
		}
		
	}
	
	
	/**
	 * Test is solid function
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 */
	@Test
	public void testIsSolid() throws IllegalPositionException, IllegalTerraintypeException{
		int[][][] terrain = new int[][][] {{{0,1,2,3}}};
		World world = new World(terrain, new DefaultTerrainChangeListener());
		assertFalse(world.isSolid(0, 0, 0));
		assertTrue(world.isSolid(0, 0, 1));
		assertTrue(world.isSolid(0, 0, 2));
		assertFalse(world.isSolid(0, 0, 3));
	}
	
	/**
	 * Test collapse
	 * If a cube is not connected to the border after an other cube has collapsed, the cube must collapse.
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 */
	@Test 
	public void testCollapse() throws IllegalPositionException, IllegalTerraintypeException{
		int[][][] terrain = new int[7][7][7];
		terrain[2][2][0] = 2;
		terrain[2][2][1]= 1;
		terrain[2][2][2] = 1;
		terrain[1][2][2] = 2;
		terrain[3][2][2] = 2;
		terrain[2][1][2] = 2;
		terrain[2][3][2] = 2;
		World world = new World(terrain, new DefaultTerrainChangeListener());
		assertTrue(world.isSolid(2, 2, 1));
		assertTrue(world.isSolid(2, 2, 2));
		assertTrue(world.isSolid(3, 2, 2));
		assertTrue(world.isSolid(1, 2, 2));
		assertTrue(world.isSolid(2, 3, 2));
		assertTrue(world.isSolid(2, 1, 2));
		world.collapse(2, 2, 1, world.getCubeType(2, 2, 1));
		assertFalse(world.isSolid(2, 2, 1));
		assertFalse(world.isSolid(2, 2, 2));
		assertFalse(world.isSolid(3, 2, 2));
		assertFalse(world.isSolid(1, 2, 2));
		assertFalse(world.isSolid(2, 3, 2));
		assertFalse(world.isSolid(2, 1, 2));
	}
	
	/**
	 * Test Is solid connected
	 * @throws IllegalTerraintypeException 
	 * @throws IllegalPositionException 
	 */
	@Test
	public void testIsSolidConnected() throws IllegalPositionException, IllegalTerraintypeException{
		int[][][] terrain = new int[3][3][3];
		terrain[1][1][2] = 1;
		terrain[0][0][1] = 1;
		terrain[2][2][2] = 2;
		World world = new World(terrain, new DefaultTerrainChangeListener());
		assertTrue(world.isSolidConnectedToBorder(1, 1, 2));
		assertTrue(world.isSolidConnectedToBorder(0, 0, 1));
		assertTrue(world.isSolidConnectedToBorder(2, 2, 2));
	}

}