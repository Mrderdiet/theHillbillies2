package hillbillies.tests.expressions;

import static org.junit.Assert.*;

import org.junit.Test;

import hillbillies.exceptions.*;
import hillbillies.model.*;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import hillbillies.tasks.expressions.*;
import hillbillies.tasks.type.Boolean;
import hillbillies.tasks.type.PositionTask;
import hillbillies.tasks.type.Type;
import hillbillies.tasks.type.UnitTask;


public class HelperExpressionTest {
	
	private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;

	private Task makeTask(Unit u, int[] selected){
		StatementExpressionsTest statement = new StatementExpressionsTest();
		Task task = new Task("Task",1000,statement,selected);
		u.setTask(task);
		task.assignTo(u);
		return task;
	}
	
	@Test
	public final void testCreateFalse() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> booleanExpr = HelperExpressions.createFalse(null);
		assertTrue(booleanExpr.evaluate(this.makeTask(unit, new int[] {0,0,0})).getValue()== false);
	}
	
	@Test
	public final void testCreateTrue() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> booleanExpr = HelperExpressions.createTrue(null);
		assertTrue(booleanExpr.evaluate(this.makeTask(unit, new int[] {0,0,0})).getValue());
	}
	
	
	
	@Test
	public final void testCreateHerePosition() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalUnitExceptions{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_TREE;
		types[0][0][0] = TYPE_AIR;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false);
		world.addUnit(unit);
		Expression<PositionTask> test = HelperExpressions.createHerePosition(null);
		assertTrue(test.evaluate(this.makeTask(unit, new int[] {0,0,0})).getValue().equals(unit.getExactPosition()));
	}
	
	@Test
	public final void testCreateLogPosition() throws IllegalUnitExceptions, IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalWorldException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false);
		Log log = new Log(new int[] {1,1,0}, world);
		world.addUnit(unit);
		world.addLog(log);
		Expression<PositionTask> test = HelperExpressions.createLogPosition(null);
		assertTrue(test.evaluate(this.makeTask(unit, new int[] {0,0,0})).getValue().equals(log.getExactPosition()));
	}
	
	@Test
	public final void testCreateBoulderPosition() throws IllegalUnitExceptions, IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalWorldException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false);
		Boulder boulder = new Boulder(new int[] {1,1,0}, world);
		world.addUnit(unit);
		world.addBoulder(boulder);
		Expression<PositionTask> test = HelperExpressions.createBoulderPosition(null);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().equals(boulder.getExactPosition()));
	}
	
	@Test
	public final void testCreateWorkshopPosition() throws IllegalUnitExceptions, IllegalArgumentExceptions, IllegalPositionException, IllegalTerraintypeException, IllegalWorldException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[0][1][0] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false);
		world.addUnit(unit);
		Expression<PositionTask> test = HelperExpressions.createWorkshopPosition(null);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[0]==0);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[1]==1);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[2]==0);
	}
	
	@Test
	public final void testCreateSelectedPosition() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions{
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false, world);
		Expression<PositionTask> test = HelperExpressions.createSelectedPosition(null);
		assertTrue(test.evaluate(this.makeTask(unit, new int[] {1,1,1})).getValue().getRoundPosition()[0]==1);
		assertTrue(test.evaluate(this.makeTask(unit, new int[] {1,1,1})).getValue().getRoundPosition()[1]==1);
		assertTrue(test.evaluate(this.makeTask(unit, new int[] {1,1,1})).getValue().getRoundPosition()[2]==1);	
	}
	
	@Test
	public final void testCreateLiteralPosition() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false, world);
		Expression<PositionTask> test = HelperExpressions.createLiteralPosition(2, 2, 2, null);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[0]==2);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[1]==2);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[2]==2);
	}
	
	@Test
	public final void testCreateNextToPosition() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalUnitExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 2, 0, 0 }, 50, 50, 50, 50, false, world);
		world.addUnit(unit);
		Expression<PositionTask> position = HelperExpressions.createLiteralPosition(2, 2, 0, null);
		Expression<PositionTask> test = HelperExpressions.createNextToPosition(position, null);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[0]==2);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[1]==1);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[2]==0);
	}
	
	@Test
	public final void testCreatePositionOf() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][0][0] = TYPE_ROCK;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false, world);
		Expression<UnitTask> unitTask = HelperExpressions.createThis(null);
		Expression<PositionTask> test = HelperExpressions.createPositionOf(unitTask, null);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[0]==0);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[1]==0);
		assertTrue(test.evaluate(this.makeTask(unit, null)).getValue().getRoundPosition()[2]==0);
	}

	@Test
	public final void testCreateThis() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][0][0] = TYPE_ROCK;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false, world);
		Expression<UnitTask> unitTask = HelperExpressions.createThis(null);
		assertTrue(unitTask.evaluate(this.makeTask(unit, null)).getValue()==unit);	
	}
	
	@Test
	public final void testCreateFriend() throws IllegalPositionException, IllegalTerraintypeException, IllegalUnitExceptions, IllegalArgumentExceptions{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitEnemy = new Unit("Test", new int[] { 1, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitFriend = new Unit("Test", new int[] { 1, 1, 0 }, 50, 50, 50, 50, false,world);
		world.addUnit(unit);
		world.addUnit(unitEnemy);
		world.addUnit(unitFriend);
		unitFriend.setFaction(unit.getFaction());	
		Expression<UnitTask> testFriend = HelperExpressions.createFriend(null);
		assertTrue(testFriend.evaluate(this.makeTask(unit, null)).getValue().getFaction().equals(unit.getFaction()));
	}
	
	@Test
	public final void testCreateEnemy() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalUnitExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitEnemy = new Unit("Test", new int[] { 1, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitFriend = new Unit("Test", new int[] { 1, 1, 0 }, 50, 50, 50, 50, false,world);
		world.addUnit(unit);
		world.addUnit(unitEnemy);
		world.addUnit(unitFriend);
		Expression<UnitTask> testEnemy = HelperExpressions.createEnemy(null);
		assertTrue(testEnemy.evaluate(this.makeTask(unit, null)).getValue()==unitEnemy);
	}

	@Test
	public final void testCreateAny() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalUnitExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitEnemy = new Unit("Test", new int[] { 1, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitFriend = new Unit("Test", new int[] { 1, 1, 0 }, 50, 50, 50, 50, false,world);
		world.addUnit(unit);
		world.addUnit(unitEnemy);
		world.addUnit(unitFriend);
		Expression<UnitTask> testAny = HelperExpressions.createAny(null);
		assertTrue(testAny.evaluate(this.makeTask(unit, null)).getValue().equals(unitEnemy));
	}
	
	
	@Test
	public final void testCreateIsSolid() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<PositionTask> position = HelperExpressions.createSelectedPosition(null);
		Expression<Boolean> testTrue = HelperExpressions.createIsSolid(position, null);
		assertTrue(testTrue.evaluate(this.makeTask(unit, new int[] {1,1,0})).getValue());
		assertFalse(testTrue.evaluate(this.makeTask(unit, new int[] {1,0,0})).getValue());
	}
	
	@Test
	public final void testCreateIsPassable() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<PositionTask> position = HelperExpressions.createSelectedPosition(null);
		Expression<Boolean> testTrue = HelperExpressions.createIsPassable(position, null);
		assertTrue(testTrue.evaluate(this.makeTask(unit, new int[] {1,0,0})).getValue());
		assertFalse(testTrue.evaluate(this.makeTask(unit, new int[] {1,1,0})).getValue());
	}

	@Test
	public final void testCreateIsFriendEnemy() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalUnitExceptions {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitEnemy = new Unit("Test", new int[] { 1, 0, 0 }, 50, 50, 50, 50, false,world);
		Unit unitFriend = new Unit("Test", new int[] { 1, 1, 0 }, 50, 50, 50, 50, false,world);
		world.addUnit(unit);
		world.addUnit(unitEnemy);
		world.addUnit(unitFriend);
		Expression<UnitTask> testFriend = HelperExpressions.createFriend(null); 
		Expression<UnitTask> testEnemy = HelperExpressions.createEnemy(null);
		Expression<Boolean> testIsFriendTrue= HelperExpressions.createIsFriend(testFriend, null);
		Expression<Boolean> testIsFriendFalse= HelperExpressions.createIsFriend(testEnemy, null);
		Expression<Boolean> testIsEnemyTrue = HelperExpressions.createIsEnemy(testEnemy, null);
		Expression<Boolean> testIsEnemyFalse = HelperExpressions.createIsEnemy(testFriend, null);
		assertTrue(testIsFriendTrue.evaluate(this.makeTask(unit, new int[] {1,0,0})).getValue());
		assertFalse(testIsFriendFalse.evaluate(this.makeTask(unit, new int[] {1,1,0})).getValue());
		assertTrue(testIsEnemyTrue.evaluate(this.makeTask(unit, new int[] {1,0,0})).getValue());
		assertFalse(testIsEnemyFalse.evaluate(this.makeTask(unit, new int[] {1,1,0})).getValue());
	}

	@Test
	public final void testCreateIsAlive() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalWorldException {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> alive = HelperExpressions.createIsAlive(HelperExpressions.createThis(null), null);
		assertTrue(alive.evaluate(this.makeTask(unit,null)).getValue());
		unit.terminate();
		assertFalse(alive.evaluate(this.makeTask(unit,null)).getValue());
	}

	@Test
	public final void testCreateCarriesItem() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions, IllegalWorldException {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][2] = TYPE_WORKSHOP;
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> isCarrying = HelperExpressions.createCarriesItem(HelperExpressions.createThis(null), null);
		assertFalse(isCarrying.evaluate(this.makeTask(unit,null)).getValue());
		}
	
	
	

	/* Unary Expressions */
	
	
	@Test
	public final void testCreateNot() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> booleanExpr = HelperExpressions.createFalse(null);
		Expression<Boolean> not = HelperExpressions.createNot(booleanExpr, null);
		Expression<Boolean> notnot = HelperExpressions.createNot(not, null);
		assertTrue(not.evaluate(this.makeTask(unit,null)).getValue());	
		assertFalse(notnot.evaluate(this.makeTask(unit,null)).getValue());
	}
	
	/* Binary Expressions */
	

	@Test
	public final void testCreateAnd() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> booleanFalse = HelperExpressions.createFalse(null);
		Expression<Boolean> booleanTrue = HelperExpressions.createTrue(null);
		Expression<Boolean> test1 = HelperExpressions.createAnd(booleanTrue, booleanTrue, null);
		Expression<Boolean> test2 = HelperExpressions.createAnd(booleanTrue, booleanFalse, null);
		Expression<Boolean> test3 = HelperExpressions.createAnd(booleanFalse, booleanFalse, null);
		assertTrue(test1.evaluate(this.makeTask(unit,null)).getValue());
		assertFalse(test2.evaluate(this.makeTask(unit,null)).getValue());
		assertFalse(test3.evaluate(this.makeTask(unit,null)).getValue());
	}

	
	@Test
	public final void testCreateOr() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<Boolean> booleanFalse = HelperExpressions.createFalse(null);
		Expression<Boolean> booleanTrue = HelperExpressions.createTrue(null);
		Expression<Boolean> test1 = HelperExpressions.createOr(booleanTrue, booleanTrue, null);
		Expression<Boolean> test2 = HelperExpressions.createOr(booleanTrue, booleanFalse, null);
		Expression<Boolean> test3 = HelperExpressions.createOr(booleanFalse, booleanFalse, null);
		assertTrue(test1.evaluate(this.makeTask(unit,null)).getValue());
		assertTrue(test2.evaluate(this.makeTask(unit,null)).getValue());
		assertFalse(test3.evaluate(this.makeTask(unit,null)).getValue());
	}
	
	/* Other */
	
	@Test
	public final <T extends Type> void testCreateReadVariable() throws IllegalPositionException, IllegalTerraintypeException, IllegalArgumentExceptions {
		int[][][] types = new int[3][3][3];
		World world = new World(types, new DefaultTerrainChangeListener());
		Unit unit = new Unit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, false,world);
		Expression<UnitTask> itsme = HelperExpressions.createThis(null);
		Task task = this.makeTask(unit, null);
		task.addVariable("unit", itsme);
		Expression<T> readVar  = HelperExpressions.createReadVariable("unit", null);
		assertTrue(itsme.evaluate(task).getValue()==((UnitTask) readVar.evaluate(task)).getValue());
	}
	
	
	
	
	
	
}
