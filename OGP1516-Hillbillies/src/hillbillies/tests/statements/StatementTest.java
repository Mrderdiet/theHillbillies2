package hillbillies.tests.statements;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import hillbillies.exceptions.*;
import hillbillies.model.Task;
import hillbillies.part3.programs.ITaskFactory;
import hillbillies.part3.programs.TaskParser;
import hillbillies.tasks.TaskFactory;
import hillbillies.tasks.expressions.Expression;
import hillbillies.tasks.statements.Statement;




public class StatementTest {
	
	
	@Before
	public void Initialize(){
		@SuppressWarnings("rawtypes")
		ITaskFactory<Expression, Statement, Task> factory = new TaskFactory();
		this.parser = TaskParser.create(factory);
	}
	
	@SuppressWarnings("rawtypes")
	ITaskFactory<Expression, Statement, Task> factory = new TaskFactory();
	@SuppressWarnings("unused")
	private TaskParser<?, ?, Task> parser;
	
	@Test
	public void testStatementAssignement() throws IOException, NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions{
		List<Task> parseResult = TaskParser.parseTasksFromString("name: \"assign task\"\npriority: 1\nactivities: w:=true; print w;"
				, factory, Collections.singletonList(new int[] { 1, 1, 1 }));
		parseResult.get(0).execute(0.0001);
	}
	
	@Test
	public void testStatementIfWhileSequence() throws IOException, NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions {
		List<Task> parseResult = TaskParser.parseTasksFromString("name: \"while loop\"\npriority : 1000\nactivities:"
				+ "w:=true;while (w) do if (w) then	w:=false; fi print w; done print w;"
				, factory, Collections.singletonList(new int[] { 1, 1, 1 }));
		parseResult.get(0).execute(0.0001);
	}
	
	@Test
	public void testStatementBreak() throws NoMoreExpressionException, BreakStatementException, IllegalArgumentExceptions {
		List<Task> parseResult = TaskParser.parseTasksFromString("name: \"while loop\"\npriority : 1000\nactivities:"
				+ "w:=true;while (w) do if (w) then	break; fi print false; done print w;"
				, factory, Collections.singletonList(new int[] { 1, 1, 1 }));
		parseResult.get(0).execute(0.0001);
	}
}
